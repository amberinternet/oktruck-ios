//
//  AlertDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class AlertDialogController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var actionButton: RoundButton!
    var message: String? = nil
    var action: String? = nil
    var completion: () -> Void = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if title != nil { titleLabel.text = title }
        messageLabel.text = message
        if action != nil { actionButton.setTitle(action, for: .normal) }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func action(_ sender: Any) {
        self.dismiss(animated: false, completion: completion)
    }

}
