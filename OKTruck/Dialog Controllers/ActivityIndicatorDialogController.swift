//
//  ActivityIndicatorDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ActivityIndicatorDialogController: UIViewController {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    var message: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView.startAnimating()
        if message != nil { loadingLabel.text = message }
    }

}
