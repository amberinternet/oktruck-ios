//
//  WorkStatusLabel.swift
//  OKTrucker
//
//  Created by amberhello on 27/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class WorkStatusLabel: UILabel {
    
    let padding = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    
    var borderColor: UIColor? {
        didSet {
            if let bColor = borderColor {
                self.layer.borderColor = bColor.cgColor
            }
        }
    }

    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: padding))
    }
    
    func initView(work: Work) {
        text = work.getReadableWorkStatus()
        DispatchQueue.main.async {
            self.layer.cornerRadius = 4
            self.layer.borderWidth = 1
            self.textColor = UIColor.white
            switch self.text {
            case WorkStatus.FIND_TRUCK.readableStatus:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = WorkStatus.FIND_TRUCK.StatusColor
                self.backgroundColor = UIColor.clear
            case WorkStatus.BOOK_BACKHAUL.readableStatus:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = WorkStatus.BOOK_BACKHAUL.StatusColor
                self.backgroundColor = UIColor.clear
            case WorkStatus.WAITING_CONFIRM_WORK.readableStatus:
                self.textColor = UIColor.white
                self.borderColor = WorkStatus.WAITING_CONFIRM_WORK.StatusColor
                self.backgroundColor = WorkStatus.WAITING_CONFIRM_WORK.StatusColor
            case WorkStatus.WAITING_RECEIVE_ITEM.readableStatus:
                self.textColor = UIColor.white
                self.borderColor = WorkStatus.WAITING_RECEIVE_ITEM.StatusColor
                self.backgroundColor = WorkStatus.WAITING_RECEIVE_ITEM.StatusColor
            case WorkStatus.ARRIVED_SOURCE.readableStatus:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = WorkStatus.ARRIVED_SOURCE.StatusColor
                self.backgroundColor = WorkStatus.ARRIVED_SOURCE.StatusColor
            case WorkStatus.WAITING_SENT_ITEM.readableStatus:
                self.textColor = UIColor.white
                self.borderColor = WorkStatus.WAITING_SENT_ITEM.StatusColor
                self.backgroundColor = WorkStatus.WAITING_SENT_ITEM.StatusColor
            case WorkStatus.ARRIVED_DESTINATION.readableStatus:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = WorkStatus.ARRIVED_DESTINATION.StatusColor
                self.backgroundColor = WorkStatus.ARRIVED_DESTINATION.StatusColor
            case WorkStatus.COMPLETE.readableStatus:
                if work.driverPaid == 1 {
                    self.setStatusFinish()
                } else {
                    switch work.status {
                    case PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status:
                        self.setStatusFinish()
                    default:
                        self.textColor = UIColor(named: "Text Color")
                        self.borderColor =  WorkStatus.COMPLETE.StatusColor
                        self.backgroundColor = UIColor.clear
                    }
                }
            case WorkStatus.CANCEL.readableStatus:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = UIColor.clear
                self.backgroundColor = UIColor.clear
            default:
                break
            }
        }
    }
    
    private func setStatusFinish() {
        self.textColor = UIColor.white
        self.borderColor = WorkStatus.COMPLETE.StatusColor
        self.backgroundColor = WorkStatus.COMPLETE.StatusColor
    }
    
}
