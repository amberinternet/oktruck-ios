//
//  AddressView.swift
//  OKTruck
//
//  Created by amberhello on 14/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class AddressView: HighlightView {

    @IBOutlet weak var placeholderLabel: UILabel!
    
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
    func setAddress(_ address: Address) {
        addressLabel.text = address.name
        contactLabel.text = address.contactName + " | " + address.contactTelephoneNumber
        
        addressStackView.isHidden = false
        placeholderLabel.isHidden = true
    }
    
    func reset() {
        addressLabel.text = Constants.INITIAL_STRING
        contactLabel.text = Constants.INITIAL_STRING
        
        addressStackView.isHidden = true
        placeholderLabel.isHidden = false
    }

}
