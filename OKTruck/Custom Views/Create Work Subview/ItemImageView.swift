//
//  ItemImageView.swift
//  OKTruck
//
//  Created by amberhello on 21/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class ItemImageView: HighlightView {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    
    func set(itemImage: UIImage) {
        label.isHidden = false
        
        if #available(iOS 13.0, *) { minLabel.textColor = UIColor.placeholderText
        } else { minLabel.textColor = UIColor.placeholder }
        minLabel.isHidden = false
        
        imageView.image = itemImage
        imageViewHeightConstraint.constant = imageView.frame.width
        imageView.isHidden = false
    }

}
