//
//  SummaryFeeStackView.swift
//  OKTruck
//
//  Created by amberhello on 30/3/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class SummaryFeeStackView: UIStackView {

    @IBOutlet weak var fullFeeLabel: UILabel!
    @IBOutlet weak var totalFeeLabel: UILabel!
    
    func initView() {
        self.spacing = 8
        fullFeeLabel.isHidden = true
    }
    
    func showDiscount() {
        self.spacing = 4
        fullFeeLabel.isHidden = false
    }

}
