//
//  PaymentMethodView.swift
//  OKTruck
//
//  Created by amberhello on 23/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class PaymentMethodView: UIView {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!

}
