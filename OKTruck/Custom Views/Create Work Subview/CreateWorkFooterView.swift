//
//  CreateWorkFooterView.swift
//  OKTruck
//
//  Created by amberhello on 20/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class CreateWorkFooterView: UIView {

    @IBOutlet weak var totalFeeLabel: UILabel!
    
    func set(totalFee: String) {
        totalFeeLabel.text = totalFee
    }

}
