//
//  DateView.swift
//  OKTruck
//
//  Created by amberhello on 14/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class DateView: HighlightView {

    @IBOutlet weak var placeholderLabel: UILabel!
    
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var receivedDateLabel: UILabel!
    @IBOutlet weak var deliveredDateLabel: UILabel!
    
    func set(receivedDate: String, deliveredDate: String) {
        receivedDateLabel.text = receivedDate
        deliveredDateLabel.text = deliveredDate
        
        dateStackView.isHidden = false
        placeholderLabel.isHidden = true
    }
    
    func reset() {
        dateStackView.isHidden = true
        placeholderLabel.isHidden = false
    }

}
