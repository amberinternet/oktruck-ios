//
//  PromotionView.swift
//  OKTruck
//
//  Created by amberhello on 30/3/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class PromotionView: UIView {

    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var usableStackView: UIStackView!
    
    func initView() {
        usableStackView.isHidden = true
        usableStackView.subviews[0].isHidden = true
        usableStackView.subviews[1].isHidden = true
    }

}
