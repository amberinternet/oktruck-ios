//
//  TotalFeesView.swift
//  OKTruck
//
//  Created by amberhello on 3/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class TotalFeesView: UIView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var transportationFeeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalFeeLabel: UILabel!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    private(set) var backhaulDiscount: Double!
    private var additionalServices: [AdditionalService]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setViewHeight()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setViewHeight()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTableHeight()
    }
    
    private func setViewHeight() {
        self.frame.size.height = 115
    }
    
    private func setTableHeight() {
        tableViewHeightConstraint.constant = 0
    }
    
    func initView(additionalServices: [AdditionalService], backhaulDiscount: Double? = nil) {
        tableView.delegate = self
        tableView.dataSource = self
        self.additionalServices = additionalServices
        if let backhaulDiscount = backhaulDiscount, backhaulDiscount > 0.0 { self.backhaulDiscount = backhaulDiscount }
        tableView.reloadData()
        self.frame.size.height = 115 + tableView.contentSize.height
        tableViewHeightConstraint.constant = tableView.contentSize.height
    }
    
    // TableView dataSource & delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if backhaulDiscount != nil {
            return additionalServices.count + 1
        }
        
        return additionalServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if backhaulDiscount != nil && indexPath.row == additionalServices.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCell", for: indexPath) as! FeeTableViewCell
            cell.nameLabel.text = NSLocalizedString("Application:BackhaulDiscount", comment: "")
            cell.chargeLabel.text = backhaulDiscount.roundDecimalFormat()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeeCell", for: indexPath) as! FeeTableViewCell
        cell.nameLabel.text = additionalServices[indexPath.item].name
        cell.chargeLabel.text = additionalServices[indexPath.item].getCharge()
        return cell
    }

}
