//
//  BorderView.swift
//  OKTruck
//
//  Created by amberhello on 20/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

@IBDesignable class TruckTypeButtonView: DesignableView {
    
//    @IBOutlet weak var selectTypeLabel: UILabel!
    
    @IBOutlet weak var typeNameLabel: UILabel!
    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var maximumLoadLabel: UILabel!
    
    func selectedAllTypes() {
        typeNameLabel.text = NSLocalizedString("Application:AllTypes", comment: "")
        
        typeNameLabel.isHidden = false
        maximumLoadLabel.isHidden = true
        truckImageView.isHidden = true
    }
    
    func selected(truckType: TruckType) {
        typeNameLabel.text = truckType.name
        maximumLoadLabel.text = truckType.maximumLoad
        truckImageView.image = truckType.image        // TODO: get truck image
        
//        selectTypeLabel.isHidden = true
        typeNameLabel.isHidden = false
        maximumLoadLabel.isHidden = false
        truckImageView.isHidden = false
    }
}
