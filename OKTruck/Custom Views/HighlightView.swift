//
//  HighlightView.swift
//  OKTruck
//
//  Created by amberhello on 10/3/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class HighlightView: UIView {
    
    private let highlightedLayer = CALayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setUp()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }
    
    private func setUp() {
        highlightedLayer.frame = CGRect(x: 0, y: 0, width:  self.frame.size.width, height: self.frame.size.height)
        highlightedLayer.backgroundColor = UIColor.black.withAlphaComponent(0.2).cgColor
        highlightedLayer.opacity = 0
        self.layer.addSublayer(highlightedLayer)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.15, animations: {
            self.highlightedLayer.opacity = 1
        })
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2, animations: {
            self.highlightedLayer.opacity = 0
        })
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2, animations: {
            self.highlightedLayer.opacity = 0
        })
    }

}
