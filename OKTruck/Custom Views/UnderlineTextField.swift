//
//  UnderlineTextField.swift
//  OKTruck
//
//  Created by amberhello on 13/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class UnderlineTextField: UITextField {
    
    // Underline
    
    let underline = CALayer()
    
    @IBInspectable var lineColor : UIColor = UIColor.black {
        didSet{
            underline.borderColor = lineColor.cgColor
        }
    }
    
    @IBInspectable var lineAlpha : CGFloat = CGFloat(1.0) {
        didSet{
            underline.borderColor = lineColor.withAlphaComponent(lineAlpha).cgColor
        }
    }
    
    
    @IBInspectable var lineHeight : CGFloat = CGFloat(1.0) {
        didSet{
            underline.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        underline.borderColor = lineColor.cgColor
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        underline.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
        underline.borderWidth = lineHeight
        self.layer.addSublayer(underline)
        self.layer.masksToBounds = true
        
        updateView()
    }
    
    override func draw(_ rect: CGRect) {
        underline.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        underline.frame = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  self.frame.size.width, height: self.frame.size.height)
    }
    
    // Right Icon
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = rightImage {
            
            let imageSize = CGFloat(16)
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: imageSize, height: imageSize))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            imageView.tintColor = tintColor
            
            rightViewMode = UITextField.ViewMode.always
            rightView = imageView
        } else {
            rightViewMode = UITextField.ViewMode.never
            rightView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: tintColor!])
    }
    
    private var maxLength: Int?
    
    func set(maximumCharacters length: Int) {
        maxLength = length
        self.delegate = self
    }
    
}

extension UnderlineTextField: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= maxLength!
    }
    
}
