//
//  IndicatorImageView.swift
//  OKTruck
//
//  Created by amberhello on 14/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class IndicatorImageView: TintImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        let loading1 = UIImage(named: "Loading 1")?.tintColor(color: self.tintColor)
        let loading2 = UIImage(named: "Loading 2")?.tintColor(color: self.tintColor)
        let loading3 = UIImage(named: "Loading 3")?.tintColor(color: self.tintColor)
        let loading4 = UIImage(named: "Loading 4")?.tintColor(color: self.tintColor)
        let loading5 = UIImage(named: "Loading 5")?.tintColor(color: self.tintColor)
        let loading6 = UIImage(named: "Loading 6")?.tintColor(color: self.tintColor)
        let loading7 = UIImage(named: "Loading 7")?.tintColor(color: self.tintColor)
        let loading8 = UIImage(named: "Loading 8")?.tintColor(color: self.tintColor)
        
        animationImages = [loading1, loading2, loading3, loading4, loading5, loading6, loading7, loading8] as? [UIImage]
        animationDuration = 0.75
    }
    
    override func startAnimating() {
        super.startAnimating()
        self.isHidden = false
    }
    
    override func stopAnimating() {
        super.stopAnimating()
        self.isHidden = true
    }
    
}
