//
//  ModalHeaderView.swift
//  OKTruck
//
//  Created by amberhello on 13/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class ModalHeaderView: UIView {
    
    @IBInspectable var topCornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = topCornerRadius
            self.layer.masksToBounds = true
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    
}
