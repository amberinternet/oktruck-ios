//
//  DesignableTextField.swift
//  OKTrucker
//
//  Created by amberhello on 13/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class DesignableTextField: UITextField {
    
    private let backgroundLayer = CALayer()
    
    @IBInspectable var bgCornerRadius: CGFloat = 0 {
        didSet {
            backgroundLayer.cornerRadius = bgCornerRadius
        }
    }
    
    @IBInspectable var bgColor : UIColor = UIColor.clear {
        didSet{
            backgroundLayer.backgroundColor = bgColor.cgColor
        }
    }
    
    @IBInspectable var bgBorderColor : UIColor = UIColor.clear {
        didSet{
            backgroundLayer.borderColor = bgBorderColor.cgColor
        }
    }
    
    @IBInspectable var lineAlpha : CGFloat = CGFloat(1.0) {
        didSet{
            backgroundLayer.borderColor = bgBorderColor.withAlphaComponent(lineAlpha).cgColor
        }
    }
    
    
    @IBInspectable var bgBorderWidth : CGFloat = CGFloat(1.0) {
        didSet{
            backgroundLayer.borderWidth = bgBorderWidth
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        backgroundLayer.borderColor = bgBorderColor.cgColor
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        backgroundLayer.frame = CGRect(x: 0, y: 0, width:  self.frame.size.width, height: self.frame.size.height)
        backgroundLayer.borderWidth = bgBorderWidth
        self.layer.addSublayer(backgroundLayer)
        self.layer.masksToBounds = true
        
        updateView()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        backgroundLayer.frame = CGRect(x: 0, y: 0, width:  self.frame.size.width, height: self.frame.size.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundLayer.frame = CGRect(x: 0, y: 0, width:  self.frame.size.width, height: self.frame.size.height)
    }
    
    // Right Icon
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var padding: CGFloat = 16 {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let image = rightImage {
            
            let imageSize = CGFloat(16)
            
            let imageView = UIImageView(frame: CGRect(x: 10, y: (self.frame.size.height-imageSize)/2, width: imageSize, height: imageSize))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            imageView.tintColor = tintColor
            
            //imageView Padding
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: (10+padding)+imageSize, height: self.frame.size.height))
//            view.backgroundColor = .green
            view.addSubview(imageView)
            rightViewMode = UITextField.ViewMode.always
            rightView = view
        } else {
            rightViewMode = UITextField.ViewMode.never
            rightView = nil
        }
        
        // Left Padding
        let view = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: self.frame.size.height))
//        view.backgroundColor = .green
        leftViewMode = UITextField.ViewMode.always
        leftView = view
        
        // Placeholder text color
        if #available(iOS 13.0, *) {
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: UIColor.placeholderText])
        } else {
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: UIColor.placeholder])
        }
    }
    
    private var maxLength: Int?
    
    func set(maximumCharacters length: Int) {
        maxLength = length
        self.delegate = self
    }
    
    let countdownLebel = UILabel()
    
    func setTextCountdown(maximumCharacters length: Int) {
        maxLength = length
        self.delegate = self
        
        let lebelSize = CGFloat(16)
        
        countdownLebel.frame = CGRect(x: 10, y: (self.frame.size.height-lebelSize)/2, width: lebelSize, height: lebelSize)
        countdownLebel.font = UIFont.systemFont(ofSize: 10)
        countdownLebel.textAlignment = .center
        if #available(iOS 13.0, *) {
            countdownLebel.textColor = UIColor.placeholderText
        } else {
            countdownLebel.textColor = UIColor.placeholder
        }
        
        //textCountLebel Padding
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: (10+padding)+lebelSize, height: self.frame.size.height))
        view.addSubview(countdownLebel)
        rightViewMode = UITextField.ViewMode.always
        rightView = view
        
    }
    
}

extension DesignableTextField: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        let countdown = (maxLength ?? 0) - count
        if (countdown >= 0) { countdownLebel.text = String(countdown) }
        return count <= maxLength!
    }
    
}
