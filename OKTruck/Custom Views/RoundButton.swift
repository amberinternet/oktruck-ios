//
//  RoundButton
//  OKTruck
//
//  Created by amberhello on 4/1/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class RoundButton: UIButton {
    
    private let highlightedLayer = CALayer()
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            if let bColor = borderColor {
                self.layer.borderColor = bColor.cgColor
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setUp()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }
    
    private func setUp() {
        highlightedLayer.frame = CGRect(x: 0, y: 0, width:  self.frame.size.width, height: self.frame.size.height)
        highlightedLayer.backgroundColor = UIColor.black.withAlphaComponent(0.2).cgColor
        highlightedLayer.opacity = 0
        self.layer.addSublayer(highlightedLayer)
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                UIView.animate(withDuration: 0.15, animations: {
                    self.highlightedLayer.opacity = 1
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.highlightedLayer.opacity = 0
                })
            }
        }
    }
}
