//
//  MyCompleteWorkViewController.swift
//  OKTruck
//
//  Created by amberhello on 19/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift

class MyCompleteWorkViewController: UICollectionViewController {
    
    private var viewModel: MyCompleteWorkViewModel!
    let refreshControl = UIRefreshControl()
    private var disposable: Disposable? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshWorkList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
    private func initVariable() {
        viewModel = MyCompleteWorkViewModel(self)
    }
    
    private func initView() {
        // Fit cell to screen width
        let collectionViewLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth = UIScreen.main.bounds.width - (collectionViewLayout.minimumInteritemSpacing * 2)
        collectionViewLayout.itemSize = CGSize(width: cellWidth, height: collectionViewLayout.itemSize.height)
        
        // Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWorkList), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc private func refreshWorkList() {
        viewModel.isRefresh = true
        disposable = viewModel.getMyCompleteWorkList()
        print("CollectionView is refresh.")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is WorkDetailViewController
        {
            let selectedCell = sender as! WorkCollectionViewCell
            let indexPath = collectionView.indexPath(for: selectedCell)!
            let work = viewModel.workList[indexPath.item]
            
            let viewController = segue.destination as! WorkDetailViewController
            viewController.temporaryWork = work
        }
    }
    
    // collectionView dataSource & delegate
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.workList.isEmpty {
            return 1
        }
        
        return viewModel.workList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewModel.workList.isEmpty {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCell", for: indexPath)
            cell.frame.size.height = 45
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkCell", for: indexPath) as! WorkCollectionViewCell
        cell.initView(work: viewModel.workList[indexPath.item])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.workPagination == nil {
            return
        }
        
        if indexPath.item == (viewModel.workList.count - 1) {
            disposable = viewModel.getMyCompleteWorkList(page: viewModel.workPagination.currentPage + 1)
        }
    }
    
}
