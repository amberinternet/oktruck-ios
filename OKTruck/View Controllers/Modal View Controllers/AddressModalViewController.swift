//
//  AddressModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 2/3/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import GooglePlaces

class AddressModalViewController: BaseModalViewController {
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var currentLocationButton: UIView!
    @IBOutlet weak var loadingIndicator: IndicatorImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var currentLocationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    private var viewModel: AddressModalViewModel!
    private var dialogManager: DialogManager!
    let refreshControl = UIRefreshControl()
    private var locationManager: CLLocationManager!
    private var tableMode = 0       //history mode = 0, search place mode = 1
    private var mapViewController: MapAddressModalViewController!
    private var places = [GMSAutocompletePrediction]()
    
    // google places filter
    private lazy var filter: GMSAutocompleteFilter = {
        let filter = GMSAutocompleteFilter()
        filter.country = "TH"
        return filter
    }()
    
    private var currentLocation: CLLocationCoordinate2D!
    var selectedAddress: Address?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshAddressHistoryList()
        initView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initSizeOfView()
    }
    
    private func initVariable() {
        viewModel = AddressModalViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    private func initRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshAddressHistoryList), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc private func refreshAddressHistoryList() {
        viewModel.isRefresh = true
        if title == NSLocalizedString("Application:SourceAddress", comment: "") {
            disposable = viewModel.getSourceHistoryList()
        } else {
            disposable = viewModel.getDestinationHistoryList()
        }
        print("TableView is refresh.")
    }
    
    private func initView() {
        searchField.placeholder = title
        if selectedAddress == nil {
            searchField.becomeFirstResponder()
        }
        tableView.keyboardDismissMode = .onDrag
        
        if let address = selectedAddress {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.mapViewController.moveMapCamera(to: CLLocationCoordinate2D(latitude: address.latitude, longitude: address.longitude))
            }
            self.mapViewController.placeNameTextField.text = address.name
            self.mapViewController.landmarkTextField.text = address.landmark
            self.mapViewController.contactNameTextField.text = address.contactName
            self.mapViewController.contactTelTextField.text = address.contactTelephoneNumber
            self.mapView.isHidden = false
        }
    }
    
    private func initSizeOfView() {
        let allContentViewHeight = headerHeightConstraint.constant
            + currentLocationHeightConstraint.constant
            + tableHeightConstraint.constant
            + 10
    
        // Set table height
        if allContentViewHeight > view.safeAreaLayoutGuide.layoutFrame.height {
            tableHeightConstraint.constant = view.safeAreaLayoutGuide.layoutFrame.height - (allContentViewHeight - tableHeightConstraint.constant)
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        if searchField.isNilOrEmpty {
            selectedAddress = nil
        }
        
        performSegue(withIdentifier: "AddressUnwindSegue", sender: nil)
    }
    
    @IBAction func getCurrentLocation(_ sender: Any) {
        searchField.resignFirstResponder()
        
        if let gmsMapMyLocation = mapViewController.mapView.myLocation {
            currentLocation = gmsMapMyLocation.coordinate
            
            mapViewController.moveMapCamera(to: currentLocation)
            mapView.isHidden = false
            
            print("current location = (\(currentLocation.latitude), \(currentLocation.longitude))")
        } else {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? MapAddressModalViewController {
            mapViewController = viewController
            mapViewController.delegate = self
            mapViewController.location = currentLocation
        }
        
    }
    
}

extension AddressModalViewController: MapViewDelegate {
    
    func mapView(idleAt address: String) {
        searchField.text = address
    }
    
}

extension AddressModalViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[0].coordinate
        
        mapViewController.moveMapCamera(to: currentLocation)
        loadingIndicator.stopAnimating()
        currentLocationButton.isUserInteractionEnabled = true
        
        print("current location = (\(currentLocation.latitude), \(currentLocation.longitude))")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
            
        case .authorizedWhenInUse:
            currentLocationButton.isUserInteractionEnabled = false
            loadingIndicator.startAnimating()
            manager.requestLocation()
            mapView.isHidden = false
            
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            
        default:
            if !CLLocationManager.locationServicesEnabled() {
                let title = NSLocalizedString("Application:LocationServicesDisabled:title", comment: "")
                let message = NSLocalizedString("Application:LocationServicesDisabled:message", comment: "")
                dialogManager.showAlert(title: title, message: message) {
                    UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
                }
            } else {
                let title = NSLocalizedString("Application:LocationAccessDenied:title", comment: "")
                let message = NSLocalizedString("Application:LocationAccessDenied:message", comment: "")
                let settingsUrl = UIApplication.openSettingsURLString
                dialogManager.showRequestAccessDialog(title: title, message: message, settingsUrl: settingsUrl)
            }
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}

extension AddressModalViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableMode {
        case 1:
            return places.count
        default:
            return viewModel.addressHistoryList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableMode {
            
        case 1:
            let place = places[indexPath.item]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath) as! PlaceTableViewCell
            cell.placeLabel.attributedText = place.attributedFullText
            
            return cell
            
        default:
            let addressHistory = viewModel.addressHistoryList[indexPath.item]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryTableViewCell
            
            cell.addressLabel.text = addressHistory.name
            cell.contactNameLabel.text = addressHistory.contactName
            cell.contactTelLabel.text = addressHistory.contactTelephoneNumber
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.addressHistoryPagination == nil {
            return
        }
        
        if indexPath.item == (viewModel.addressHistoryList.count - 1) {
            if title == NSLocalizedString("Application:SourceAddress", comment: "") {
                disposable = viewModel.getSourceHistoryList(page: viewModel.addressHistoryPagination.currentPage + 1)
            } else {
                disposable = viewModel.getDestinationHistoryList(page: viewModel.addressHistoryPagination.currentPage + 1)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableMode {
            
        case 1:
            let place = places[indexPath.item]
            
            searchField.attributedText = place.attributedFullText
            mapViewController.moveMapCamera(to: place.placeID)
            
            mapView.isHidden = false
            
        default:
            let addressHistory = viewModel.addressHistoryList[indexPath.item]
            
            selectedAddress = Address()
            selectedAddress!.name = addressHistory.name
            selectedAddress!.landmark = addressHistory.landmark
            selectedAddress!.contactName = addressHistory.contactName
            selectedAddress!.contactTelephoneNumber = addressHistory.contactTelephoneNumber
            selectedAddress!.fullAddress = addressHistory.fullAddress
            selectedAddress!.subdistrict = addressHistory.subdistrict
            selectedAddress!.district = addressHistory.district
            selectedAddress!.province = addressHistory.province
            selectedAddress!.latitude = addressHistory.latitude
            selectedAddress!.longitude = addressHistory.longitude
            
            performSegue(withIdentifier: "AddressUnwindSegue", sender: nil)
        }
    }
    
}

extension AddressModalViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchStr = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if searchStr.isEmpty {
            self.places = [GMSAutocompletePrediction]()
            
            tableMode = 0
            tableView.reloadData()
        } else {
            GMSPlacesClient.shared().autocompleteQuery(searchStr, bounds: nil, filter: filter, callback: { (result: [GMSAutocompletePrediction]?, error: Error?) in
                if let places = result {
                    self.places = places
                }
                if let error = error {
                    self.dialogManager.showError(error: error.localizedDescription)
//                    print("An error occurred: \(error.localizedDescription)")
                    return
                }
                
                self.tableMode = 1
                self.tableView.reloadData()
            })
        }
        
        mapView.isHidden = true
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        mapView.isHidden = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        tableView.reloadData()
        return true
    }
    
}
