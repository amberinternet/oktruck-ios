//
//  MapAddressModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 2/3/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol MapViewDelegate {
    func mapView(idleAt address: String)
}

class MapAddressModalViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var placeNameTextField: UITextField!
    @IBOutlet weak var landmarkTextField: UITextField!
    @IBOutlet weak var contactNameTextField: UITextField!
    @IBOutlet weak var contactTelTextField: UITextField!
    private var dialogManager: DialogManager!
    var delegate: MapViewDelegate?
    var location: CLLocationCoordinate2D!
    var isFirstBeingDisplayed = true
    var place: GMSPlace?
    var selectedAddress: Address!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        mapView.isMyLocationEnabled = true
        placeNameTextField.delegate = self
        landmarkTextField.delegate = self
        contactNameTextField.delegate = self
        contactTelTextField.delegate = self
    }
    
    func moveMapCamera(to target: CLLocationCoordinate2D, zoom: Float = 16) {
        mapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: zoom)
        
    }
    
    func moveMapCamera(to placeID: String, zoom: Float = 16) {
        // Get a place by ID
        GMSPlacesClient.shared().fetchPlace(fromPlaceID: placeID, placeFields:  GMSPlaceField.all, sessionToken: nil, callback: {
            (place: GMSPlace?, error: Error?) in
            if let place = place {
                self.mapView.camera = GMSCameraPosition.camera(withTarget: place.coordinate, zoom: zoom)
                
                self.place = place
            }
            if let error = error {
                self.dialogManager.showError(error: error.localizedDescription)
//                print("An error occurred: \(error.localizedDescription)")
                return
            }
        })
    }
    
    @IBAction func selectAddress(_ sender: Any) {
        if validate() {
            selectedAddress.name = placeNameTextField.text!
            selectedAddress.landmark = landmarkTextField.text!
            selectedAddress.contactName = contactNameTextField.text!
            selectedAddress.contactTelephoneNumber = contactTelTextField.text!
            
            let parentViewController = parent as! AddressModalViewController
            parentViewController.selectedAddress = selectedAddress
            parentViewController.performSegue(withIdentifier: "AddressUnwindSegue", sender: sender)
            
            print(selectedAddress)
        }
    }
    
    private func validate() -> Bool {
        if placeNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:PlaceName", comment: ""))
            return false
        }
        if contactNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:ContactName", comment: ""))
            return false
        }
        if contactTelTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:ContactTelephoneNumber", comment: ""))
            return false
        }
        if !contactTelTextField.isValidTelephone {
            dialogManager.showAlert(message: NSLocalizedString("Validate:TelephoneNumberInvalid", comment: ""))
            return false
        }
        return true
    }
    
}

extension MapAddressModalViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        location = position.target
        
        if isFirstBeingDisplayed {
            isFirstBeingDisplayed = false
        } else {
            UserDefaults.standard.set(["th"], forKey: "AppleLanguages")
            GMSGeocoder().reverseGeocodeCoordinate(location, completionHandler: {       //TODO: set result language
                (response, error) in
                if let address = response?.firstResult() {
                    let fullAddress = address.lines!.joined(separator: " ")
                    self.delegate?.mapView(idleAt: fullAddress)
                    
                    self.selectedAddress = Address()
                    if (address.administrativeArea == "กรุงเทพมหานคร" || address.administrativeArea == "Krung Thep Maha Nakhon"), let province = address.administrativeArea, let district = address.subLocality {
                        self.selectedAddress.province = province
                        self.selectedAddress.district = district
                        
                        let subdistrict: String
                        if let thoroughfare = address.thoroughfare {
                            subdistrict = fullAddress.components(separatedBy: district).first!.components(separatedBy: thoroughfare).last!.trimmingCharacters(in: .whitespacesAndNewlines)
                        } else {
                            subdistrict = fullAddress.components(separatedBy: district).first!
                        }
                        self.selectedAddress.subdistrict = subdistrict
                        
                    } else if let province = address.administrativeArea, let subdistrict = address.locality {
                        self.selectedAddress.province = province
                        self.selectedAddress.subdistrict = subdistrict
                        
                        let district = fullAddress.components(separatedBy: province).first!.components(separatedBy: subdistrict).last!.trimmingCharacters(in: .whitespacesAndNewlines)
                        self.selectedAddress.district = district
                    }
                    
                    if let fullAddress = self.place?.formattedAddress {
                        self.selectedAddress.fullAddress = fullAddress
                        self.place = nil
                    } else {
                        self.selectedAddress.fullAddress = fullAddress/*.components(separatedBy: self.selectedAddress!.subdistrict).first!*/
                    }
                    self.selectedAddress.latitude = address.coordinate.latitude
                    self.selectedAddress.longitude = address.coordinate.longitude
                    
                    print(address)
                    print(self.selectedAddress.fullAddress)
                }
                if let error = error {
                    self.dialogManager.showError(error: error.localizedDescription)
//                        print("An error occurred: \(error.localizedDescription)")
                }
                UserDefaults.standard.removeObject(forKey: "AppleLanguages")
            })
        }
        
        print("Map location: (\(location.latitude), \(location.longitude))")
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        view.endEditing(true)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        view.endEditing(true)
    }
    
}

extension MapAddressModalViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == contactTelTextField {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 10
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case placeNameTextField:
            landmarkTextField.becomeFirstResponder()
        case landmarkTextField:
            contactNameTextField.becomeFirstResponder()
        case contactNameTextField:
            contactTelTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
}
