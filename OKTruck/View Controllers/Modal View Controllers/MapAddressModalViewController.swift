//
//  MapAddressModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 2/3/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import ContactsUI
import AnimatedTextInput
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON

protocol MapViewDelegate {
    func mapView(idleAt address: String)
}

class MapAddressModalViewController: UIViewController, CNContactPickerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var placeNameTextField: AnimatedTextInput!
    @IBOutlet weak var landmarkTextField: AnimatedTextInput!
    @IBOutlet weak var contactNameTextField: AnimatedTextInput!
    @IBOutlet weak var contactTelTextField: AnimatedTextInput!
    private var dialogManager: DialogManager!
    private let contactPickerViewController = CNContactPickerViewController()
    var delegate: MapViewDelegate?
    var location: CLLocationCoordinate2D!
    var isFirstBeingDisplayed = true
    var place: GMSPlace?
    var selectedAddress: Address!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        mapView.clear()
        mapView.delegate = nil
        mapView.removeFromSuperview()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
        selectedAddress = Address()
    }
    
    private func initView() {
        let thailandLocation = CLLocationCoordinate2D(latitude: 13.7245601, longitude: 100.4930287)
        mapView.camera = GMSCameraPosition.camera(withTarget: thailandLocation, zoom: 8)
        mapView.settings.tiltGestures = false
        mapView.isMyLocationEnabled = true
        
        placeNameTextField.delegate = self
        placeNameTextField.style = CustomAnimatedTextInputStyle()
        placeNameTextField.type = .standard
        placeNameTextField.placeholder = NSLocalizedString("Application:PlaceName", comment: "")
        
        landmarkTextField.delegate = self
        landmarkTextField.style = CustomAnimatedTextInputStyle()
        landmarkTextField.type = .standard
        landmarkTextField.placeholder = NSLocalizedString("Application:Landmark", comment: "")
        
        contactNameTextField.delegate = self
        contactNameTextField.style = CustomAnimatedTextInputStyle()
        contactNameTextField.type = .standard
        contactNameTextField.placeholder = NSLocalizedString("Application:Contact", comment: "")
        
        contactTelTextField.delegate = self
        contactTelTextField.style = CustomAnimatedTextInputStyle()
        contactTelTextField.type = .phone
        contactTelTextField.placeholder = NSLocalizedString("Application:TelephoneNumber", comment: "")
    }
    
    func moveMapCamera(to target: CLLocationCoordinate2D, zoom: Float = 16) {
        mapView.animate(with: GMSCameraUpdate.setTarget(target, zoom: zoom))
    }
    
    func moveMapCamera(to placeID: String, zoom: Float = 16) {
        // Get a place by ID
        GMSPlacesClient.shared().fetchPlace(fromPlaceID: placeID, placeFields:  GMSPlaceField.all, sessionToken: nil, callback: {
            (place: GMSPlace?, error: Error?) in
            if let place = place {
                self.mapView.animate(with: GMSCameraUpdate.setTarget(place.coordinate, zoom: zoom))
                
                self.place = place
            }
            if let error = error {
                self.dialogManager.showError(error: error.localizedDescription)
//                print("An error occurred: \(error.localizedDescription)")
                return
            }
        })
    }
    
    private func showRequestContactsAccessDialog() {
        let title = NSLocalizedString("Application:ContactsAccessDenied:title", comment: "")
        let message = NSLocalizedString("Application:ContactsAccessDenied:message", comment: "")
        let settingsUrl = UIApplication.openSettingsURLString
        dialogManager.showRequestAccessDialog(title: title, message: message, settingsUrl: settingsUrl)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // contact name
        contactNameTextField.text = "\(contact.givenName) \(contact.familyName)"

        // contact phone number
        let phoneNumberCount = contact.phoneNumbers.count

        guard phoneNumberCount > 0 else {
            dismiss(animated: true)
            dialogManager.showAlert(message: NSLocalizedString("Alert:ContactPicker:NoTelephoneNumber:message", comment: ""))
            return
        }

        if phoneNumberCount == 1 {
            let phoneNumbers = contact.phoneNumbers[0].value.stringValue.replacingOccurrences(of: "-", with: "")
            contactTelTextField.text = phoneNumbers

        } else {
            var title = NSLocalizedString("Alert:ContactPicker:SelectTelephoneNumber:title", comment: "")
            let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)

            for i in 0..<phoneNumberCount {
                let phoneNumbers = contact.phoneNumbers[i].value.stringValue.replacingOccurrences(of: "-", with: "")
                alertController.addAction(UIAlertAction(title: phoneNumbers, style: .default, handler: { _ in
                    self.contactTelTextField.text = phoneNumbers
                }))
            }
            
            title = NSLocalizedString("Application:Dismiss", comment: "")
            alertController.addAction(UIAlertAction(title: title, style: .cancel, handler: nil))

            dismiss(animated: true)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func openContactPicker(_ sender: Any) {
        contactPickerViewController.delegate = self
        contactPickerViewController.displayedPropertyKeys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            self.present(contactPickerViewController, animated: true, completion: nil)
        case .notDetermined:
            CNContactStore().requestAccess(for: .contacts) { authorized, error in
                if authorized {
                    self.present(self.contactPickerViewController, animated: true, completion: nil)
                }
            }
        default:
            self.showRequestContactsAccessDialog()
        }
    }
    
    @IBAction func selectAddress(_ sender: Any) {
        if validate() {
            selectedAddress.name = placeNameTextField.text!
            selectedAddress.landmark = landmarkTextField.text!
            selectedAddress.contactName = contactNameTextField.text!
            selectedAddress.contactTelephoneNumber = contactTelTextField.text!
            
            let parentViewController = parent as! AddressModalViewController
            parentViewController.selectedAddress = selectedAddress
            parentViewController.performSegue(withIdentifier: "AddressUnwindSegue", sender: sender)
            
            print(selectedAddress!)
        }
    }
    
    private func validate() -> Bool {
        if !self.selectedAddress.fullAddress.contains("ประเทศไทย") {
            dialogManager.showAlert(message: NSLocalizedString("Validate:Location", comment: ""))
            return false
        }
        if placeNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:PlaceName", comment: ""))
            return false
        }
        if contactNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:ContactName", comment: ""))
            return false
        }
        if contactTelTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:ContactTelephoneNumber", comment: ""))
            return false
        }
        return true
    }
    
}

extension MapAddressModalViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        location = position.target
        
        if isFirstBeingDisplayed {
            isFirstBeingDisplayed = false
        } else {
            let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.latitude),\(location.longitude)&language=th&key=\(Constants.GOOGLE_API_KEY)")!
            Alamofire.request(url).responseJSON{(response) -> Void in
                switch response.result {
                case .success(let data):
                    let json = JSON(data)
                    let result = json["results"][1]

                    let fullAddress = result["formatted_address"].string
                    var subdistrict, district, province: String?

                    if let addressComponents = result["address_components"].array {
                        for addressComponent in addressComponents {
                            if let types = addressComponent["types"].array {

                                if types.contains("sublocality_level_2") || types.contains("locality") {
                                    subdistrict = addressComponent["long_name"].string
                                }

                                if types.contains("sublocality_level_1") || types.contains("administrative_area_level_2") {
                                    district = addressComponent["long_name"].string
                                }

                                if types.contains("administrative_area_level_1") {
                                    province = addressComponent["long_name"].string
                                }
                            }
                        }
                    }

                    self.delegate?.mapView(idleAt: fullAddress ?? "")

                    self.selectedAddress.fullAddress = fullAddress ?? "-"
                    self.selectedAddress.subdistrict = subdistrict ?? "-"
                    self.selectedAddress.district = district ?? "-"
                    self.selectedAddress.province = province ?? "-"

                    print("Full Address: \(self.selectedAddress.fullAddress)")
                    print("Subdistrict: \(self.selectedAddress.subdistrict)")
                    print("District: \(self.selectedAddress.district)")
                    print("Province: \(self.selectedAddress.province)")

                case .failure(let error):
                    self.dialogManager.showError(error: error.localizedDescription)

                }
            }
            
            self.selectedAddress.latitude = location.latitude
            self.selectedAddress.longitude = location.longitude
            
            print("Map location: (\(location.latitude), \(location.longitude))")
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        view.endEditing(true)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        view.endEditing(true)
    }
    
}

extension MapAddressModalViewController : AnimatedTextInputDelegate {
    
    func animatedTextInput(animatedTextInput: AnimatedTextInput, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if animatedTextInput == contactTelTextField {
            guard let textFieldText = animatedTextInput.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 10
        }
        return true
    }
    
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        switch animatedTextInput {
        case placeNameTextField:
            landmarkTextField.becomeFirstResponder()
        case landmarkTextField:
            contactNameTextField.becomeFirstResponder()
        case contactNameTextField:
            contactTelTextField.becomeFirstResponder()
        default:
            animatedTextInput.resignFirstResponder()
        }
        return true
    }
    
}
