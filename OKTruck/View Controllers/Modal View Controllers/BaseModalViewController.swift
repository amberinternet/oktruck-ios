//
//  BaseModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 25/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import KeychainSwift
import RxSwift

class BaseModalViewController: BaseViewController {
    
    @IBOutlet weak var contentView: UIView!
    private var contentViewSize: CGSize { return contentView.frame.size }
    private var contentViewX: CGFloat { return contentView.frame.origin.x }
    private var contentViewY: CGFloat { return contentView.frame.origin.y }
    private var contentViewHeight: CGFloat { return contentView.frame.height }
    private let animateDuration: TimeInterval = 0.3
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presentAnimate()
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        if !(presentedViewController is UIAlertController) {
            // for ModalViewController
            dismissAnimate {
                super.dismiss(animated: flag, completion: completion)
            }
        } else {
            // for UIAlertController
            super.dismiss(animated: flag, completion: completion)
        }
    }
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        dismissAnimate {
            super.performSegue(withIdentifier: identifier, sender: sender)
        }
    }
    
    private func presentAnimate() {
        self.contentView.frame = CGRect(origin: CGPoint(x: contentViewX,
                                                        y: contentViewY + contentViewHeight),
                                        size: contentViewSize)
        UIView.animate(withDuration: animateDuration, animations: {
            self.contentView.frame = CGRect(origin: CGPoint(x: self.contentViewX,
                                                            y: self.contentViewY - self.contentViewHeight),
                                            size: self.contentViewSize)
        })
    }
    
    private func dismissAnimate(completion: @escaping () -> Void) {
        UIView.animate(withDuration: animateDuration, animations: {
            self.contentView.frame = CGRect(origin: CGPoint(x: self.contentViewX,
                                                            y: self.contentViewY + self.contentViewHeight),
                                            size: self.contentViewSize)
        }) { (_) in
            completion()
        }
    }
    
}
