//
//  ImageModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 27/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ImageModalViewController: BaseModalViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var dialogManager: DialogManager!
    private let imagePickerController = UIImagePickerController()
    
    private var allPhotos: PHFetchResult<PHAsset>? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    var selectedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.mediaTypes = ["public.image"]
        
        // Load Photos
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized, .notDetermined:
                let fetchOptions = PHFetchOptions()
                self.allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            default:
                let title = NSLocalizedString("Application:PhotoLibraryAccessDenied:title", comment: "")
                let message = NSLocalizedString("Application:PhotoLibraryAccessDenied:message", comment: "")
                self.showRequestAccessDialog(title: title, message: message)
            }
        }
    }
    
    private func showRequestAccessDialog(title: String, message: String) {
        let settingsUrl = UIApplication.openSettingsURLString
        dialogManager.showRequestAccessDialog(title: title, message: message, settingsUrl: settingsUrl)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        performSegue(withIdentifier: "ImageUnwindSegue", sender: nil)
    }

    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func openPhotoLibrary(_ sender: Any) {
        imagePickerController.sourceType = .photoLibrary
        present(self.imagePickerController, animated: true, completion: nil)
    }

}

extension ImageModalViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (allPhotos?.count ?? 1) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraCell", for: indexPath)
            return cell
        }
        
        if allPhotos == nil {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IndicatorCell", for: indexPath) as! IndicatorCollectionViewCell
            cell.indicatorView.startAnimating()
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCollectionViewCell
        let reversedIndex = (allPhotos!.count - 1) - (indexPath.item - 1)
        let asset = allPhotos!.object(at: reversedIndex)
        cell.imageView.fetchImage(asset: asset, contentMode: .aspectFit, targetSize: cell.imageView.frame.size)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized, .notDetermined:
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            default:
                let title = NSLocalizedString("Application:CameraAccessDenied:title", comment: "")
                let message = NSLocalizedString("Application:CameraAccessDenied:message", comment: "")
                self.showRequestAccessDialog(title: title, message: message)
            }
        } else {
            let reversedIndex = (allPhotos!.count - 1) - (indexPath.item - 1)
            let asset = allPhotos!.object(at: reversedIndex)
            let options = PHImageRequestOptions()
            options.version = .original
            options.isSynchronous = true
            options.deliveryMode = .highQualityFormat
            PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: 1600, height: 1600), contentMode: .aspectFit, options: options) { image, _ in
                guard let image = image else { return }
                self.selectedImage = image
                self.performSegue(withIdentifier: "ImageUnwindSegue", sender: nil)
            }
        }
    }
    
}

extension UIImageView {
    
    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, _ in
            guard let image = image else { return }
            switch contentMode {
            case .aspectFill:
                self.contentMode = .scaleAspectFill
            default:
                self.contentMode = .scaleAspectFit
            }
            self.image = image
        }
    }
    
}
