//
//  DepositModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 13/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class DepositModalViewController: BaseModalViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var borderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    var priceList : [Double] = []
    var selectedPrice: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initView()
    }
    
    private func initVariable() {
        priceList = Arrays.deposit_amount_list
    }
    
    private func initView() {
        selectButton.isEnabled = false
        
        let allContentViewHeight = contentView.frame.height
        
        // Set table height
        if allContentViewHeight < view.safeAreaLayoutGuide.layoutFrame.height {
            tableHeightConstraint.constant = tableView.contentSize.height
        } else {
            tableHeightConstraint.constant = view.safeAreaLayoutGuide.layoutFrame.height - (headerHeightConstraint.constant + borderHeightConstraint.constant)
        }
        
        // Enable scrolling based on content height
        tableView.isScrollEnabled = tableView.contentSize.height > tableHeightConstraint.constant
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func topUp(_ sender: Any) {
        performSegue(withIdentifier: "DepositUnwindSegue", sender: nil)
    }
    
}

extension DepositModalViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return priceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let topUp = priceList[indexPath.item]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DepositCell", for: indexPath) as! DepositTableViewCell
        cell.priceLabel.text = "฿\(topUp.roundDecimalFormat())"
//        cell.bonusCreditLabel.text = topUp[1].roundDecimalFormat()
//        cell.totalCreditLabel.text = "฿\(topUp[2].roundDecimalFormat())"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPrice = priceList[indexPath.item]
        selectButton.isEnabled = true
    }
    
}
