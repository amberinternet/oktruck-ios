//
//  AdditionalServiceModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 30/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class AdditionalServiceModalViewController: BaseModalViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var borderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    var additionalServices: [AdditionalService]!
    private var isSelectedAdditionalServices: [Bool]!
    var selectedAdditionalServices: [AdditionalService] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initView()
    }
    
    private func initVariable() {
        isSelectedAdditionalServices = [Bool](repeating: false, count: additionalServices.count)
    }
    
    private func initView() {
        let allContentViewHeight = contentView.frame.height
        
        // Set table height
        if allContentViewHeight < view.safeAreaLayoutGuide.layoutFrame.height {
            tableHeightConstraint.constant = tableView.contentSize.height
        } else {
            tableHeightConstraint.constant = view.safeAreaLayoutGuide.layoutFrame.height - (headerHeightConstraint.constant + borderHeightConstraint.constant)
        }
        
        // Enable scrolling based on content height
        tableView.isScrollEnabled = tableView.contentSize.height > tableHeightConstraint.constant
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func selectAdditionalServices(_ sender: Any) {
        selectedAdditionalServices.removeAll()
        if isSelectedAdditionalServices.contains(true) {
            for i in 0..<additionalServices.count {
                if isSelectedAdditionalServices[i] {
                    selectedAdditionalServices.append(additionalServices[i])
                }
            }
        }
       
        performSegue(withIdentifier: "AdditionalServiceUnwindSegue", sender: sender)
    }
    
}

extension AdditionalServiceModalViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return additionalServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let additionalService = additionalServices[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalServiceCell", for: indexPath) as! AdditionalServiceWithChargeLabelTableViewCell
        cell.nameLabel.text = additionalService.name
        cell.chargeLabel.text = additionalService.getCharge()
        
        // set pre-selected
        if !selectedAdditionalServices.isEmpty && selectedAdditionalServices.contains(additionalService) {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            self.tableView(tableView, didSelectRowAt: indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isSelectedAdditionalServices[indexPath.row] = true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        isSelectedAdditionalServices[indexPath.row] = false
    }
    
}
