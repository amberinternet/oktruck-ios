//
//  PaymentMethodModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 23/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class PaymentMethodModalViewController: BaseModalViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    private var dialogManager: DialogManager!
    private var user: User!
    var work: Work!
    
    var paymentMethodStatus: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initView()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
        user = User.shared
    }
    
    private func initView() {
        let allContentViewHeight = contentView.frame.height
        
        // Set table height
        if allContentViewHeight < view.safeAreaLayoutGuide.layoutFrame.height {
            tableHeightConstraint.constant = tableView.contentSize.height
        } else {
            tableHeightConstraint.constant = view.safeAreaLayoutGuide.layoutFrame.height - headerHeightConstraint.constant
        }
        
        // Enable scrolling based on content height
        tableView.isScrollEnabled = tableView.contentSize.height > tableHeightConstraint.constant
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func selectPaymentMethod(_ sender: Any) {
        if validate() {
            performSegue(withIdentifier: "PaymentMethodUnwindSegue", sender: sender)
        }
    }
    
    private func validate() -> Bool {
        if paymentMethodStatus == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:PaymentMethod", comment: ""))
            return false
        }
        return true
    }
    
}

extension PaymentMethodModalViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodCell", for: indexPath) as! PaymentMethodTableViewCell
        
        switch indexPath.item {
        case 0:
            cell.icon.image = UIImage(named: "Wallet Icon")
            cell.nameLabel.text = "Wallet"
            cell.detailLabel.text = NSLocalizedString("PaymentMethodModalViewController:creditLabel:message", comment: "") + user.getCredit()
        case 1:
            cell.icon.image = UIImage(named: "Cash Source Icon")
            cell.nameLabel.text = PaymentMethod.CASH_SOURCE.pay
            cell.detailLabel.text = work.source.name
        default:
            cell.icon.image = UIImage(named: "Cash Destination Icon")
            cell.nameLabel.text = PaymentMethod.CASH_DESTINATION.pay
            cell.detailLabel.text = work.destination.name
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            paymentMethodStatus = PaymentMethod.WALLET.status
        case 1:
            paymentMethodStatus = PaymentMethod.CASH_SOURCE.status
        default:
            paymentMethodStatus = PaymentMethod.CASH_DESTINATION.status
        }
    }
    
}
