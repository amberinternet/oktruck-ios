//
//  TruckTypeModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 22/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class TruckTypeModalViewController: BaseModalViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var borderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    var truckTypes : [TruckType]!
    var selectedTruckType: TruckType?  
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initView()
    }
    
    private func initView() {
//        if selectedTruckType == nil {
//            selectButton.isEnabled = false
//        }
        
        let allContentViewHeight = contentView.frame.height
        
        // Set table height
        if allContentViewHeight < view.safeAreaLayoutGuide.layoutFrame.height {
            tableHeightConstraint.constant = tableView.contentSize.height
        } else {
            tableHeightConstraint.constant = view.safeAreaLayoutGuide.layoutFrame.height - (headerHeightConstraint.constant + borderHeightConstraint.constant)
        }
        
        // Enable scrolling based on content height
        tableView.isScrollEnabled = tableView.contentSize.height > tableHeightConstraint.constant
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func selectTruckType(_ sender: Any) {
        performSegue(withIdentifier: "TruckTypeUnwindSegue", sender: sender)
    }
    
}

extension TruckTypeModalViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return truckTypes.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllTruckTypesCell", for: indexPath)
            let selectedView = UIView()
            selectedView.backgroundColor = UIColor(named: "Background Tint Color")
            selectedView.layer.borderColor = UIColor(named: "Tint Color")!.cgColor
            selectedView.layer.borderWidth = 1
            cell.selectedBackgroundView = selectedView
            
            // set pre-selected
            if selectedTruckType == nil {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                
            }
            
            return cell
        }
        
        let truckType = truckTypes[indexPath.item - 1]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TruckTypeCell", for: indexPath) as! TruckTypeTableViewCell
        cell.typeNameLabel.text = truckType.name
        cell.maximumLoadLabel.text = truckType.maximumLoad
        cell.truckImageView.image = truckType.image       // TODO: get truck image
        
        // set pre-selected
        if selectedTruckType?.id == truckType.id {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            selectedTruckType = nil
        } else {
            selectedTruckType = truckTypes[indexPath.item - 1]
        }
        
//        selectButton.isEnabled = true
    }
    
}
