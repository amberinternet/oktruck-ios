//
//  RegisterViewController.swift
//  OKTruck
//
//  Created by amberhello on 13/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AnimatedTextInput

class RegisterViewController: BaseViewController {
    
    @IBOutlet weak var firstNameTextField: AnimatedTextInput!
    @IBOutlet weak var lastNameTextField: AnimatedTextInput!
    @IBOutlet weak var emailTextField: AnimatedTextInput!
    @IBOutlet weak var telephoneTextField: AnimatedTextInput!
    @IBOutlet weak var companyTextField: AnimatedTextInput!
    @IBOutlet weak var passwordTextField: AnimatedTextInput!
    @IBOutlet weak var confirmPasswordTextField: AnimatedTextInput!
    private var viewModel: RegisterViewModel!
    private var dialogManager: DialogManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = RegisterViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        firstNameTextField.delegate = self
        firstNameTextField.style = RegisterAnimatedTextInputStyle()
        firstNameTextField.type = .standard
        firstNameTextField.placeholder = NSLocalizedString("RegisterViewModel:FirstName", comment: "")
        
        lastNameTextField.delegate = self
        lastNameTextField.style = RegisterAnimatedTextInputStyle()
        lastNameTextField.type = .standard
        lastNameTextField.placeholder = NSLocalizedString("RegisterViewModel:LastName", comment: "")
        
        emailTextField.delegate = self
        emailTextField.style = RegisterAnimatedTextInputStyle()
        emailTextField.type = .email
        emailTextField.placeholder = NSLocalizedString("RegisterViewModel:Email", comment: "")
        
        telephoneTextField.delegate = self
        telephoneTextField.style = RegisterAnimatedTextInputStyle()
        telephoneTextField.type = .phone
        telephoneTextField.placeholder = NSLocalizedString("RegisterViewModel:TelephoneNumber", comment: "")
        
        companyTextField.delegate = self
        companyTextField.style = RegisterAnimatedTextInputStyle()
        companyTextField.type = .standard
        companyTextField.placeholder = NSLocalizedString("RegisterViewModel:Company", comment: "")
        
        passwordTextField.delegate = self
        passwordTextField.style = RegisterAnimatedTextInputStyle()
        passwordTextField.type = .password(toggleable: false)
        passwordTextField.placeholder = NSLocalizedString("RegisterViewModel:Password", comment: "")
        
        confirmPasswordTextField.delegate = self
        confirmPasswordTextField.style = RegisterAnimatedTextInputStyle()
        confirmPasswordTextField.type = .password(toggleable: false)
        confirmPasswordTextField.placeholder = NSLocalizedString("RegisterViewModel:PasswordConfirmation", comment: "")
    }
    
    @IBAction func register(_ sender: Any) {
        if validate() {
            User.shared.firstName = firstNameTextField.text!
            User.shared.lastName = lastNameTextField.text!
            User.shared.email = emailTextField.text!
            User.shared.telephoneNumber = telephoneTextField.text!
            User.shared.company.name = companyTextField.text!
            User.shared.password = passwordTextField.text!
            User.shared.confirmPassword = confirmPasswordTextField.text!
            disposable = viewModel.signUp()
        }
    }
    
    private func validate() -> Bool {
        if firstNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:FirstName", comment: ""))
            return false
        }
        if lastNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:LastName", comment: ""))
            return false
        }
        if emailTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:Email", comment: ""))
            return false
        }
        if !emailTextField.isValidEmail {
            dialogManager.showAlert(message: NSLocalizedString("Validate:EmailInvalid", comment: ""))
            return false
        }
        if telephoneTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:TelephoneNumber", comment: ""))
            return false
        }
        if !telephoneTextField.isValidTelephone {
            dialogManager.showAlert(message: NSLocalizedString("Validate:TelephoneNumberInvalid", comment: ""))
            return false
        }
        if passwordTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:Password", comment: ""))
            return false
        }
        if confirmPasswordTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:PasswordConfirmation", comment: ""))
            return false
        }
        if confirmPasswordTextField.text != passwordTextField.text {
            dialogManager.showAlert(message: NSLocalizedString("Validate:PasswordNotMatch", comment: ""))
            return false
        }
        return true
    }
    
}

extension RegisterViewController : AnimatedTextInputDelegate {
    
    func animatedTextInput(animatedTextInput: AnimatedTextInput, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if animatedTextInput == telephoneTextField {
            guard let textFieldText = animatedTextInput.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 10
        }
        return true
    }
    
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        switch animatedTextInput {
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            telephoneTextField.becomeFirstResponder()
        case telephoneTextField:
            companyTextField.becomeFirstResponder()
        case companyTextField:
            passwordTextField.becomeFirstResponder()
        default:
            confirmPasswordTextField.resignFirstResponder()
        }
        return true
    }
    
}
