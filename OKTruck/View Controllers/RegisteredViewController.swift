//
//  RegisteredViewController.swift
//  OKTruck
//
//  Created by amberhello on 13/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class RegisteredViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
    }
    
    @IBAction func backToLogin(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
