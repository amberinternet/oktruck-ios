//
//  WorkDetailViewController.swift
//  OKTrucker
//
//  Created by amberhello on 12/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class WorkDetailViewController: BaseViewController {
    
    @IBOutlet weak var cancelWorkBarButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userTelephoneNumberLabel: UILabel!
    @IBOutlet weak var statusLabel: WorkStatusLabel!
    @IBOutlet weak var sourceNameLabel: UILabel!
    @IBOutlet weak var sourceContactLabel: UILabel!
    @IBOutlet weak var sourceDateLabel: UILabel!
    @IBOutlet weak var sourceTimeLabel: UILabel!
    @IBOutlet weak var sourceSignatureImageView: UIImageView!
    @IBOutlet weak var destinationNameLabel: UILabel!
    @IBOutlet weak var destinationContactLabel: UILabel!
    @IBOutlet weak var destinationDateLabel: UILabel!
    @IBOutlet weak var destinationTimeLabel: UILabel!
    @IBOutlet weak var destinationSignatureImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemQuantityLabel: UILabel!
    @IBOutlet weak var itemWeightLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var truckTypeLabel: UILabel!
    @IBOutlet weak var licensePlateLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var contactDriverButton: UIStackView!
//    @IBOutlet weak var additionalServicesLabel: UILabel!
    @IBOutlet weak var totalFeeLabel: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    private var viewModel: WorkDetailViewModel!
    private var dialogManager: DialogManager!
    let refreshControl = UIRefreshControl()
    var temporaryWork: Work!
    var isAcceptWork: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initRefreshControl()
        initView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
    private func initVariable() {
        viewModel = WorkDetailViewModel(self)
        viewModel.work = temporaryWork
        dialogManager = DialogManager.default()
    }
    
    private func initRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshWork), for: .valueChanged)
        scrollView.refreshControl = refreshControl
    }
    
    @objc private func refreshWork() {
        viewModel.isRefresh = true
        disposable = viewModel.getWorkDetail()
        print("ScrollView is refresh.")
    }
    
    func initView() {
        title = "\(NSLocalizedString("Application:Work", comment: "")) \(viewModel.work.getReadableWorkId())"
        switch viewModel.work.status {
        case WorkStatus.FIND_TRUCK.status:
            cancelWorkBarButton.isEnabled = true
            cancelWorkBarButton.tintColor = .white
        default:
            cancelWorkBarButton.isEnabled = false
            cancelWorkBarButton.tintColor = .clear
        }
        
        userNameLabel.text = viewModel.work.userName
        userTelephoneNumberLabel.text = viewModel.work.userTelephoneNumber
        statusLabel.initView(work: viewModel.work)
        sourceNameLabel.text = viewModel.work.source.name
        sourceContactLabel.text = viewModel.work.source.getReadableContact()
        sourceDateLabel.text = viewModel.work.source.getArrivalShotDate()
        sourceTimeLabel.text = viewModel.work.source.getReadableTime()
        if !viewModel.work.source.signatureFileName.isEmpty {
            sourceSignatureImageView.af_setImage(withURLRequest: ImageAPIRouter.getItemImage(imagePath: viewModel.work.source.getSignatureImagePath()))
            sourceSignatureImageView.isHidden = false
        } else {
            sourceSignatureImageView.isHidden = true
        }
        destinationNameLabel.text = viewModel.work.destination.name
        destinationContactLabel.text = viewModel.work.destination.getReadableContact()
        destinationDateLabel.text = viewModel.work.destination.getArrivalShotDate()
        destinationTimeLabel.text = viewModel.work.destination.getReadableTime()
        if !viewModel.work.destination.signatureFileName.isEmpty {
            destinationSignatureImageView.af_setImage(withURLRequest: ImageAPIRouter.getItemImage(imagePath: viewModel.work.destination.getSignatureImagePath()))
            destinationSignatureImageView.isHidden = false
        } else {
            destinationSignatureImageView.isHidden = true
        }
        itemNameLabel.text = viewModel.work.item.name
        itemQuantityLabel.text = viewModel.work.item.quantity
        itemWeightLabel.text = viewModel.work.item.weight
        itemImageView.af_setImage(withURLRequest: ImageAPIRouter.getItemImage(imagePath: viewModel.work.item.getImagePath()))
        if !viewModel.work.note.isEmpty { noteLabel.text = viewModel.work.note }
        else { noteLabel.text = "-" }
        truckTypeLabel.text = viewModel.work.truckType.name
        if !viewModel.work.licensePlate.isEmpty {
            licensePlateLabel.text = "\(viewModel.work.licensePlate) \(viewModel.work.truckProvince)"
            driverNameLabel.text = viewModel.work.driverName
        } else {
            licensePlateLabel.isHidden = true
            driverNameLabel.isHidden = true
        }
        switch viewModel.work.status {
        case WorkStatus.COMPLETE.status, WorkStatus.CANCEL.status, WorkStatus.PAYMENT_FAIL.status, WorkStatus.FIND_TRUCK.status:
            contactDriverButton.isHidden = true
        default:
            contactDriverButton.isHidden = false
        }
//        if viewModel.work.additionalServices.count > 0 {
//            additionalServicesLabel.text = viewModel.work.additionalServices.first?.name
//            if viewModel.work.additionalServices.count > 1 {
//                for additionalService in viewModel.work.additionalServices[1...] {
//                    additionalServicesLabel.text = additionalServicesLabel.text! + " / \(additionalService.name!)"
//                }
//            }
//        } else {
//            additionalServicesLabel.isHidden = true
//        }
        totalFeeLabel.text = Double(viewModel.work.summaryFee)!.roundDecimalFormat()
        paymentMethodLabel.text = viewModel.work.getReadablePaymentMethod()
        distanceLabel.text = viewModel.work.getReadableDistance()
    }
    
    @IBAction func cancelWork(_ sender: Any) {
        dialogManager.showConfirm(title: NSLocalizedString("Alert:CancelWork:title", comment: ""), message: NSLocalizedString("Alert:CancelWork:message", comment: "")) {
            self.disposable = self.viewModel.cancelWork()
        }
    }
    
    @IBAction func contactDriver(_ sender: Any) {
        call(telephoneNumber: viewModel.work.driverTelephoneNumber)
    }
    
    @IBAction func recall(_ sender: Any) {
        self.performSegue(withIdentifier: "CreateWorkUnwindSegue", sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func call(telephoneNumber: String) {
        guard let number = URL(string: "tel://" + telephoneNumber) else {
            print("Failed to convert \"\(telephoneNumber)\" to URL.")
            return
        }
        UIApplication.shared.open(number)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is FullScreenImageViewController
        {
            let viewController = segue.destination as! FullScreenImageViewController
            viewController.imagePath = viewModel.work.item.getImagePath()
            viewController.title = viewModel.work.item.name
        }
        
    }
    
}
