//
//  MapViewController.swift
//  OKTruck
//
//  Created by amberhello on 7/2/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    private var dialogManager: DialogManager!
    var work: Work?
    var post: Post?

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
            
    private func initVariable() {
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        let thailandLocation = CLLocationCoordinate2D(latitude: 13.7245601, longitude: 100.4930287)
        mapView.camera = GMSCameraPosition.camera(withTarget: thailandLocation, zoom: 8)
        mapView.settings.tiltGestures = false

        var gmsBounds = GMSCoordinateBounds()
        var sourceLocation, destinationLocation: CLLocationCoordinate2D!
        
        // google places filter
        let filter: GMSAutocompleteFilter = {
            let filter = GMSAutocompleteFilter()
            filter.country = "TH"
            return filter
        }()
        
        // add source & destination province marker & draw path
        if let work = work {
            // add markers
            if work.source.latitude != Constants.INITIAL_DOUBLE && work.source.longitude != Constants.INITIAL_DOUBLE {
                let sourceLocation = CLLocationCoordinate2D(latitude: work.source.latitude, longitude: work.source.longitude)
                let sourceMarker = GMSMarker(position: sourceLocation)
                sourceMarker.title = NSLocalizedString("Application:SourceAddress", comment: "")
                sourceMarker.icon = UIImage(named: "Placeholder Source Address")
                sourceMarker.map = mapView
                
                gmsBounds = gmsBounds.includingCoordinate(sourceLocation)
                // fit map view to show marker
                mapView.animate(with: GMSCameraUpdate.setTarget(sourceLocation, zoom: 16.0))
            }
            if work.destination.latitude != Constants.INITIAL_DOUBLE && work.destination.longitude != Constants.INITIAL_DOUBLE {
                let destinationLocation = CLLocationCoordinate2D(latitude: work.destination.latitude, longitude: work.destination.longitude)
                let destinationMarker = GMSMarker(position: destinationLocation)
                destinationMarker.title = NSLocalizedString("Application:DestinationAddress", comment: "")
                destinationMarker.icon = UIImage(named: "Placeholder Destination Address")
                destinationMarker.map = mapView
                
                gmsBounds = gmsBounds.includingCoordinate(destinationLocation)
                // fit map view to show marker
                mapView.animate(with: GMSCameraUpdate.setTarget(destinationLocation, zoom: 16.0))
            }
            
            // draw path between markers (polylines)
            if work.source.latitude != Constants.INITIAL_DOUBLE && work.source.longitude != Constants.INITIAL_DOUBLE,
                work.destination.latitude != Constants.INITIAL_DOUBLE && work.destination.longitude != Constants.INITIAL_DOUBLE {
                
                let sourceLocation = CLLocationCoordinate2D(latitude: work.source.latitude, longitude: work.source.longitude)
                let destinationLocation = CLLocationCoordinate2D(latitude: work.destination.latitude, longitude: work.destination.longitude)
                let source = "\(sourceLocation.latitude),\(sourceLocation.longitude)"
                let destination = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
                let travelMode = "driving"
                
                let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&mode=\(travelMode)&key=\(Constants.GOOGLE_API_KEY)")!
                Alamofire.request(url).responseJSON{(response) -> Void in
                    switch response.result {
                    case .success(let data):
                        let json = JSON(data)
                        if let routesJson = json["routes"].arrayObject {
                            let routes = routesJson as! [[String: AnyObject]]
                            if routes.count > 0 {
                                for route in routes {
                                    /* get the point */
                                    let overviewPolyline = route["overview_polyline"]?["points"] as! String
                                    let path = GMSMutablePath(fromEncodedPath: overviewPolyline)
                                    /* set up polyline */
                                    let polyline = GMSPolyline.init(path: path)
                                    polyline.strokeWidth = 4
                                    polyline.strokeColor = UIColor(named: "GMS Polyline Color")!
                                    polyline.zIndex = 10;
                                    polyline.map = self.mapView
                                    let borderPolyline = GMSPolyline.init(path: path)
                                    borderPolyline.strokeWidth = polyline.strokeWidth * 1.4
                                    borderPolyline.strokeColor = UIColor(named: "GMS Polyline Border Color")!
                                    borderPolyline.zIndex = polyline.zIndex - 1;
                                    borderPolyline.map = self.mapView
                                }
                                
                                // fit map view to show all markers
                                self.mapView.animate(with: GMSCameraUpdate.fit(gmsBounds, withPadding: 50.0))
                            }
                        }

                    case .failure(let error):
                        self.dialogManager.showError(error: error.localizedDescription)

                    }
                }
            }
            
        } else if let post = post {
            // Search source places
            GMSPlacesClient.shared().autocompleteQuery(post.sourceProvince, bounds: nil, filter: filter, callback: { (result: [GMSAutocompletePrediction]?, error: Error?) in
                if let place = result?.first {
                    // Get a place detail by ID
                    GMSPlacesClient.shared().fetchPlace(fromPlaceID: place.placeID, placeFields:  GMSPlaceField.all, sessionToken: nil, callback: {
                        (place: GMSPlace?, error: Error?) in
                        if let place = place {
                            sourceLocation = place.coordinate
                            // add markers
                            let sourceMarker = GMSMarker(position: sourceLocation)
                            sourceMarker.title = NSLocalizedString("Application:SourceAddress", comment: "")
                            sourceMarker.map = self.mapView
                            
                            // Search destination places
                            GMSPlacesClient.shared().autocompleteQuery(post.destinationProvince, bounds: nil, filter: filter, callback: { (result: [GMSAutocompletePrediction]?, error: Error?) in
                                if let place = result?.first {
                                    // Get a place detail by ID
                                    GMSPlacesClient.shared().fetchPlace(fromPlaceID: place.placeID, placeFields:  GMSPlaceField.all, sessionToken: nil, callback: {
                                        (place: GMSPlace?, error: Error?) in
                                        if let place = place {
                                            destinationLocation = place.coordinate
                                            // add markers
                                            let destinationMarker = GMSMarker(position: destinationLocation)
                                            destinationMarker.title = NSLocalizedString("Application:DestinationAddress", comment: "")
                                            destinationMarker.map = self.mapView
                                            
                                            // draw path between markers
                                            let source = "\(sourceLocation.latitude),\(sourceLocation.longitude)"
                                            let destination = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
                                            let travelMode = "driving"
                                            
                                            let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&mode=\(travelMode)&key=\(Constants.GOOGLE_API_KEY)")!
                                            Alamofire.request(url).responseJSON{(response) -> Void in
                                                switch response.result {
                                                case .success(let data):
                                                    let json = JSON(data)
                                                    if let routesJson = json["routes"].arrayObject {
                                                        let routes = routesJson as! [[String: AnyObject]]
                                                        if routes.count > 0 {
                                                            for route in routes {
                                                                /* get the point */
                                                                let overviewPolyline = route["overview_polyline"]?["points"] as! String
                                                                let path = GMSMutablePath(fromEncodedPath: overviewPolyline)
                                                                /* set up polyline */
                                                                let polyline = GMSPolyline.init(path: path)
                                                                polyline.strokeWidth = 4
                                                                polyline.strokeColor = UIColor(named: "GMS Polyline Color")!
                                                                polyline.zIndex = 10;
                                                                polyline.map = self.mapView
                                                                let borderPolyline = GMSPolyline.init(path: path)
                                                                borderPolyline.strokeWidth = polyline.strokeWidth * 1.4
                                                                borderPolyline.strokeColor = UIColor(named: "GMS Polyline Border Color")!
                                                                borderPolyline.zIndex = polyline.zIndex - 1;
                                                                borderPolyline.map = self.mapView
                                                            }
                                                        }
                                                    }

                                                case .failure(let error):
                                                    self.dialogManager.showError(error: error.localizedDescription)

                                                }
                                            }
                                            
                                            // fit map view to show all markers
                                            gmsBounds = gmsBounds.includingCoordinate(sourceLocation)
                                            gmsBounds = gmsBounds.includingCoordinate(destinationLocation)
                                            self.mapView.animate(with: GMSCameraUpdate.fit(gmsBounds, withPadding: 50.0))
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
            
            // add passed provinces marker
            for province in post.passedProvinces {
                // Search source places
                GMSPlacesClient.shared().autocompleteQuery(province, bounds: nil, filter: filter, callback: { (result: [GMSAutocompletePrediction]?, error: Error?) in
                    if let place = result?.first {
                        // Get a place detail by ID
                        GMSPlacesClient.shared().fetchPlace(fromPlaceID: place.placeID, placeFields:  GMSPlaceField.all, sessionToken: nil, callback: {
                            (place: GMSPlace?, error: Error?) in
                            if let place = place {
                                let location = place.coordinate
                                // add markers
                                let marker = GMSMarker(position: location)
                                marker.title = NSLocalizedString("Application:SourceAddress", comment: "")
                                marker.map = self.mapView

                                // fit map view to show all markers
                                gmsBounds = gmsBounds.includingCoordinate(location)
                                self.mapView.animate(with: GMSCameraUpdate.fit(gmsBounds, withPadding: 50.0))
                            }
                        })
                    }
                })
            }
        }
    }

}
