//
//  SplashViewController.swift
//  OKTruck
//
//  Created by amberhello on 18/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift

class SplashViewController: BaseViewController {
    
    @IBOutlet weak var loadingIndicator: IndicatorImageView!
    private var dialogManager: DialogManager!
    private var userManager: UserManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initAccount()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
        userManager = UserManager.default
    }
    
    private func initAccount() {
        if (userManager.hasToken()) {
            loadingIndicator.startAnimating()
            refreshTokenWithGoToMainStoryBoard()
        } else {
            loadingIndicator.stopAnimating()
            goToLogInViewController()
        }
    }
    
    private func refreshTokenWithGoToMainStoryBoard() {
        let userService = UserService.default
        let observable: Observable<UserResponse> = userService.refreshToken().flatMap({ value -> Observable<UserResponse> in
            UserManager.default.storeAccessToken(value.accessToken)
            return userService.getProfile()
        })
        
        disposable = RxNetwork<UserResponse>().request(observable, onSuccess: { (response) in
            User.shared.set(user: response.user)
            if (User.shared.isVerify == 1) {
                self.goToMainStoryBoard()
            } else {
                self.dialogManager.showAlert(message: NSLocalizedString("Alert:UserNotVerify", comment: ""), completion: {
                    self.goToLogInViewController()
                })
            }
        }, onFailure: {_ in
            self.goToLogInViewController()
        })
    }
    
    private func goToMainStoryBoard() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateInitialViewController()!
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    private func goToLogInViewController() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "LogInSegue", sender: self)
        }
    }
    
}
