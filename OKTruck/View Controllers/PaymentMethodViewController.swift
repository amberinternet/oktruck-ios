//
//  CreateWorkPaymentMethodViewController.swift
//  OKTruck
//
//  Created by amberhello on 5/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class CreateWorkPaymentMethodViewController: BaseViewController {
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userTelephoneNumberTextField: UITextField!
    @IBOutlet weak var noteTextField: UITextField!
    @IBOutlet weak var walletMethodView: WalletMethodView!
    @IBOutlet weak var totalFeesView: TotalFeesView!
    private var viewModel: PaymantMethodViewModel!
    private var dialogManager: DialogManager!
    var work: Work!

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = PaymantMethodViewModel(self)
        dialogManager = DialogManager.default()
        disposable = viewModel.getProfile()
    }
    
    private func initView() {
        userNameTextField.delegate = self
        userTelephoneNumberTextField.delegate = self
        noteTextField.delegate = self
        walletMethodView.creditLabel.text = User.shared.getCredit()
        totalFeesView.initView(additionalServices: work.additionalServices)
        totalFeesView.transportationFeeLabel.text = work.transportationFee
        totalFeesView.totalFeeLabel.text = work.totalFee
        
        userNameTextField.text = work.userName
        userTelephoneNumberTextField.text = work.userTelephoneNumber
        noteTextField.text = work.note
    }
    
    @IBAction func pay(_ sender: Any) {
        if validate() {
            if walletMethodView.isSelected {
                work.userName = userNameTextField.text!
                work.userTelephoneNumber = userTelephoneNumberTextField.text!
                work.note = noteTextField.text!
                disposable = viewModel.createWorkWithWallet(work)
            } else {
                dialogManager.showAlert(message: NSLocalizedString("Validate:PaymentMethod", comment: ""))
            }
        }
    }
    
    private func validate() -> Bool {
        if userNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:UserName", comment: ""))
            return false
        }
        if userTelephoneNumberTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:UserTelephoneNumber", comment: ""))
            return false
        }
        if !userTelephoneNumberTextField.isValidTelephone {
            dialogManager.showAlert(message: NSLocalizedString("Validate:TelephoneNumberInvalid", comment: ""))
            return false
        }
        return true
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if identifier == "CreditCardMethodSegue" && !validate() {
            return false
        }
        
        if identifier == "CashMethodSegue" && !validate() {
            return false
        }
        
        return true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        work.userName = userNameTextField.text!
        work.userTelephoneNumber = userTelephoneNumberTextField.text!
        work.note = noteTextField.text!
        
        if let viewController = segue.destination as? CreditCardMethodViewController {
            viewController.work = work
        }
        
        if let viewController = segue.destination as? CashMethodViewController {
            viewController.work = work
        }
        
    }

}

extension CreateWorkPaymentMethodViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == userTelephoneNumberTextField {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 10
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case userNameTextField:
            userTelephoneNumberTextField.becomeFirstResponder()
        case userTelephoneNumberTextField:
            noteTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
}

