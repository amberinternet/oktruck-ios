//
//  CreateWorkItemViewController.swift
//  OKTruck
//
//  Created by amberhello on 21/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import ContactsUI
import AnimatedTextInput

class CreateWorkItemViewController: BaseViewController, UIImagePickerControllerDelegate, CNContactPickerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var itemNameTextField: AnimatedTextInput!
    @IBOutlet weak var itemQuantityTextField: AnimatedTextInput!
    @IBOutlet weak var itemWeightTextField: AnimatedTextInput!
    @IBOutlet weak var itemImageView: ItemImageView!
    @IBOutlet weak var noteTextField: AnimatedTextInput!
    @IBOutlet weak var userNameTextField: AnimatedTextInput!
    @IBOutlet weak var userTelephoneNumberTextField: AnimatedTextInput!
    @IBOutlet weak var footerView: CreateWorkFooterView!
    private var dialogManager: DialogManager!
    private let contactPickerViewController = CNContactPickerViewController()
    private var user: User!
    private let imagePickerController = UIImagePickerController()
    private var imagePickerDialog: UIAlertController!
    var previousViewController: CreateWorkViewController?
    var work: Work!
    
    var itemImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        self.navigationController?.delegate = self
        dialogManager = DialogManager.default()
        user = User.shared
    }
    
    private func initView() {
        itemNameTextField.delegate = self
        itemNameTextField.style = CustomAnimatedTextInputStyle()
        itemNameTextField.type = .standard
        itemNameTextField.placeholder = NSLocalizedString("Application:Cargo", comment: "")
        itemNameTextField.text = work.item.name
        
        itemQuantityTextField.delegate = self
        itemQuantityTextField.style = CustomAnimatedTextInputStyle()
        itemQuantityTextField.type = .standard
        itemQuantityTextField.placeholder = NSLocalizedString("Application:CargoQuantity", comment: "")
        itemQuantityTextField.text = work.item.quantity
        
        itemWeightTextField.delegate = self
        itemWeightTextField.style = CustomAnimatedTextInputStyle()
        itemWeightTextField.type = .standard
        itemWeightTextField.placeholder = NSLocalizedString("Application:CargoWeight", comment: "")
        itemWeightTextField.text = work.item.weight
        
        if let itemImage = work.item.image {
            self.itemImage = itemImage
            itemImageView.set(itemImage: itemImage)
        }
        
        noteTextField.delegate = self
        noteTextField.style = CustomAnimatedTextInputStyle()
        noteTextField.type = .standard
        noteTextField.placeholder = NSLocalizedString("Application:Note", comment: "")
        noteTextField.text = work.note
        
        userNameTextField.delegate = self
        userNameTextField.style = CustomAnimatedTextInputStyle()
        userNameTextField.type = .standard
        userNameTextField.placeholder = NSLocalizedString("Application:Name", comment: "")
        if work.userName.isEmpty {
            userNameTextField.text = user.getFullName()
        } else {
            userNameTextField.text = work.userName
        }
        
        userTelephoneNumberTextField.delegate = self
        userTelephoneNumberTextField.style = CustomAnimatedTextInputStyle()
        userTelephoneNumberTextField.type = .phone
        userTelephoneNumberTextField.placeholder = NSLocalizedString("Application:TelephoneNumber", comment: "")
        if work.userTelephoneNumber.isEmpty {
            userTelephoneNumberTextField.text = user.telephoneNumber
        } else {
            userTelephoneNumberTextField.text = work.userTelephoneNumber
        }
        
        footerView.totalFeeLabel.text = work.getReadablePrice()
        initImagePickerDialog()
    }
    
    private func initImagePickerDialog() {
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.mediaTypes = ["public.image"]
        
        imagePickerDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        imagePickerDialog.view.tintColor = UIColor(named: "Primary Color")
        
        var message = NSLocalizedString("Application:TakePhoto", comment: "")
        imagePickerDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ _ in
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized, .notDetermined:
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            default:
                self.showRequestCameraAccessDialog()
            }
        }))
        
        message = NSLocalizedString("Application:PhotoLibrary", comment: "")
        imagePickerDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ _ in
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }))
        
        message = NSLocalizedString("Application:Dismiss", comment: "")
        imagePickerDialog.addAction(UIAlertAction(title: message, style: .cancel))
    }
    
    private func showRequestCameraAccessDialog() {
        let title = NSLocalizedString("Application:CameraAccessDenied:title", comment: "")
        let message = NSLocalizedString("Application:CameraAccessDenied:message", comment: "")
        let settingsUrl = UIApplication.openSettingsURLString
        dialogManager.showRequestAccessDialog(title: title, message: message, settingsUrl: settingsUrl)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        itemImageView.set(itemImage: selectedImage!.resize(to: 500))
        itemImage = selectedImage!.resize(to: 1600)
    }
    
    private func showRequestContactsAccessDialog() {
        let title = NSLocalizedString("Application:ContactsAccessDenied:title", comment: "")
        let message = NSLocalizedString("Application:ContactsAccessDenied:message", comment: "")
        let settingsUrl = UIApplication.openSettingsURLString
        dialogManager.showRequestAccessDialog(title: title, message: message, settingsUrl: settingsUrl)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // contact name
        userNameTextField.text = "\(contact.givenName) \(contact.familyName)"

        // contact phone number
        let phoneNumberCount = contact.phoneNumbers.count

        guard phoneNumberCount > 0 else {
            dismiss(animated: true)
            dialogManager.showAlert(message: NSLocalizedString("Alert:ContactPicker:NoTelephoneNumber:message", comment: ""))
            return
        }

        if phoneNumberCount == 1 {
            let phoneNumbers = contact.phoneNumbers[0].value.stringValue.replacingOccurrences(of: "-", with: "")
            userTelephoneNumberTextField.text = phoneNumbers

        } else {
            var title = NSLocalizedString("Alert:ContactPicker:SelectTelephoneNumber:title", comment: "")
            let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)

            for i in 0..<phoneNumberCount {
                let phoneNumbers = contact.phoneNumbers[i].value.stringValue.replacingOccurrences(of: "-", with: "")
                alertController.addAction(UIAlertAction(title: phoneNumbers, style: .default, handler: { _ in
                    self.userTelephoneNumberTextField.text = phoneNumbers
                }))
            }
            
            title = NSLocalizedString("Application:Dismiss", comment: "")
            alertController.addAction(UIAlertAction(title: title, style: .cancel, handler: nil))

            dismiss(animated: true)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func chooseItemImage(_ sender: Any) {
        self.present(imagePickerDialog, animated: true, completion: nil)
    }
    
    @IBAction func openContactPicker(_ sender: Any) {
        contactPickerViewController.delegate = self
        contactPickerViewController.displayedPropertyKeys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            self.present(contactPickerViewController, animated: true, completion: nil)
        case .notDetermined:
            CNContactStore().requestAccess(for: .contacts) { authorized, error in
                if authorized {
                    self.present(self.contactPickerViewController, animated: true, completion: nil)
                }
            }
        default:
            self.showRequestContactsAccessDialog()
        }
    }
    
    @IBAction func pay(_ sender: Any) {
        if validate() {
            work.item.name = itemNameTextField.text!
            work.item.quantity = itemQuantityTextField.text!
            work.item.weight = itemWeightTextField.text!
            work.item.image = itemImage!
            work.note = noteTextField.text!
            work.userName = userNameTextField.text!
            work.userTelephoneNumber = userTelephoneNumberTextField.text!

            performSegue(withIdentifier: "CreateWorkSummarySegue", sender: sender)
        }
    }
    
    private func validate() -> Bool {
        if itemNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:CargoName", comment: ""))
            return false
        }
        if itemQuantityTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:Quantity", comment: ""))
            return false
        }
        if itemWeightTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:Weight", comment: ""))
            return false
        }
        if itemImage == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:CargoImage", comment: ""))
            return false
        }
        if userNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:UserName", comment: ""))
            return false
        }
        if userTelephoneNumberTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:UserTelephoneNumber", comment: ""))
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? CreateWorkSummaryViewController {
            viewController.work = work
        }
            
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if viewController == previousViewController {
            work.item.name = itemNameTextField.text!
            work.item.quantity = itemQuantityTextField.text!
            work.item.weight = itemWeightTextField.text!
            if let itemImage = itemImage { work.item.image = itemImage }
            work.note = noteTextField.text!
            work.userName = userNameTextField.text!
            work.userTelephoneNumber = userTelephoneNumberTextField.text!
            
            previousViewController?.work = work
            
            print(555555)
        }
        
    }
    
}

extension CreateWorkItemViewController : AnimatedTextInputDelegate {
    
    func animatedTextInput(animatedTextInput: AnimatedTextInput, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if animatedTextInput == userTelephoneNumberTextField {
            guard let textFieldText = animatedTextInput.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 10
        }
        return true
    }
    
    func animatedTextInputShouldReturn(animatedTextInput: AnimatedTextInput) -> Bool {
        switch animatedTextInput {
        case itemNameTextField:
            itemQuantityTextField.becomeFirstResponder()
        case itemQuantityTextField:
            itemWeightTextField.becomeFirstResponder()
        case itemWeightTextField:
            noteTextField.becomeFirstResponder()
        case noteTextField:
            userNameTextField.becomeFirstResponder()
        case userNameTextField:
            userTelephoneNumberTextField.becomeFirstResponder()
        default:
            animatedTextInput.resignFirstResponder()
        }
        return true
    }
    
}
