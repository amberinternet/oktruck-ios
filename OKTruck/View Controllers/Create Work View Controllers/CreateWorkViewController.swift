//
//  CreateWorkViewController.swift
//  OKTruck
//
//  Created by amberhello on 9/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift
import GoogleMaps
import Alamofire
import SwiftyJSON

class CreateWorkViewController: BaseViewController {
    
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var truckTypeView: TruckTypeView!
    @IBOutlet weak var sourceAddressView: AddressView!
    @IBOutlet weak var destinationAddressView: AddressView!
    @IBOutlet weak var dateView: DateView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var footerView: CreateWorkFooterView!
    @IBOutlet weak var contentStackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewBottomConstraint: NSLayoutConstraint!
    private var viewModel: CreateWorkViewModel!
    private var dialogManager: DialogManager!
    var post: Post?
    var temporaryWork: Work?
    
    var work: Work!
    private var truckType: TruckType?
    private var source: Address?
    private var destination: Address?
    private var additionalServices: [AdditionalService] = []
    private var pickUpDate: String?
    private var pickUpTime: String?
    private var dropOffDate: String?
    private var dropOffTime: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if post == nil {
            navigationController?.navigationBar.barStyle = UIBarStyle.default
            navigationController?.navigationBar.barTintColor = UIColor(named: "Tab Bar Color")
            let logo = UIImage(named: "OKTruck Logo 2")
            let imageView = UIImageView(image:logo)
            navigationItem.titleView = imageView
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if post == nil {
            navigationController?.navigationBar.barStyle = UIBarStyle.black
            navigationController?.navigationBar.barTintColor = UIColor(named: "Primary Color")
        }
    }
    
    private func initVariable() {
        viewModel = CreateWorkViewModel(self)
        dialogManager = DialogManager.default()
        disposable = viewModel.getTruckType()

        if temporaryWork == nil { work = Work() }
        else { work = temporaryWork }
    }
    
    private func initView() {
        if post == nil {
            tabBarView.isHidden = false
            headerView.isHidden = true
            truckTypeView.isHidden = false
        } else {
            title = NSLocalizedString("Application:CreateBackhaulWork", comment: "")
            
            tabBarView.isHidden = true
            headerView.isHidden = false
            truckTypeView.isHidden = true
            
            if let temporaryWork = temporaryWork {
                if !temporaryWork.source.fullAddress.isEmpty {
                    source = temporaryWork.source
                    sourceAddressView.setAddress(source!)
                }
                if !temporaryWork.destination.fullAddress.isEmpty {
                    destination = temporaryWork.destination
                    destinationAddressView.setAddress(destination!)
                }
//                additionalServices = temporaryWork.additionalServices
                
                calculateTotalFees()
            }
        }
        
        let thailandLocation = CLLocationCoordinate2D(latitude: 13.7245601, longitude: 100.4930287)
        mapView.camera = GMSCameraPosition.camera(withTarget: thailandLocation, zoom: 8)
        mapView.settings.setAllGesturesEnabled(false)

        contentStackViewBottomConstraint.constant = 0
        footerViewBottomConstraint.constant = -footerView.frame.height
    }

    @IBAction func next(_ sender: Any) {
        if validate() {
            work.truckType = truckType!
//            work.additionalServices = additionalServices
            work.source = source!
            work.source.date = pickUpDate!
            work.source.time = pickUpTime!
            work.destination = destination!
            work.destination.date = dropOffDate!
            work.destination.time = dropOffTime!
            if let post = post { work.postId = post.id }
            else { work.postId = nil }
            work.summaryFee = String(viewModel.totalFee)

            performSegue(withIdentifier: "CreateWorkItemSegue", sender: sender)
        }
    }
    
    private func calculateTotalFees() {
        if let source = source, let destination = destination {
            if let post = post {
                truckType = post.getTruckType()
                disposable = viewModel.calculateFee(truckTypeId: truckType!.id,
                                                    sourceLatitude: source.latitude, sourceLongitude: source.longitude,
                                                    destinationLatitude: destination.latitude, destinationLongitude: destination.longitude,
                                                    additionalServices: additionalServices,
                                                    postId: post.id)
            } else {
                truckType = viewModel.truckTypes[(truckTypeView.collectionView.indexPathsForSelectedItems?.first?.item)!]
                disposable = viewModel.calculateFee(truckTypeId: truckType!.id,
                                                    sourceLatitude: source.latitude, sourceLongitude: source.longitude,
                                                    destinationLatitude: destination.latitude, destinationLongitude: destination.longitude,
                                                    additionalServices: additionalServices)
            }
        }
        addMarkersAndDrawPathToMapView()
    }
    
    private func addMarkersAndDrawPathToMapView() {
        // clear mapView before add new markers & draw path (polylines)
        mapView.clear()
        var gmsBounds = GMSCoordinateBounds()
        
        // add markers
        if let source = source {
            let sourceLocation = CLLocationCoordinate2D(latitude: source.latitude, longitude: source.longitude)
            let sourceMarker = GMSMarker(position: sourceLocation)
            sourceMarker.title = NSLocalizedString("Application:SourceAddress", comment: "")
            sourceMarker.icon = UIImage(named: "Placeholder Source Address")
            sourceMarker.map = mapView
            
            gmsBounds = gmsBounds.includingCoordinate(sourceLocation)
            // fit map view to show marker
            mapView.animate(with: GMSCameraUpdate.setTarget(sourceLocation, zoom: 16.0))
        }
        if let destination = destination {
            let destinationLocation = CLLocationCoordinate2D(latitude: destination.latitude, longitude: destination.longitude)
            let destinationMarker = GMSMarker(position: destinationLocation)
            destinationMarker.title = NSLocalizedString("Application:DestinationAddress", comment: "")
            destinationMarker.icon = UIImage(named: "Placeholder Destination Address")
            destinationMarker.map = mapView
            
            gmsBounds = gmsBounds.includingCoordinate(destinationLocation)
            // fit map view to show marker
            mapView.animate(with: GMSCameraUpdate.setTarget(destinationLocation, zoom: 16.0))
        }
        
        // draw path between markers (polylines)
        if let source = source, let destination = destination {
            let sourceLocation = CLLocationCoordinate2D(latitude: source.latitude, longitude: source.longitude)
            let destinationLocation = CLLocationCoordinate2D(latitude: destination.latitude, longitude: destination.longitude)
            let source = "\(sourceLocation.latitude),\(sourceLocation.longitude)"
            let destination = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
            let travelMode = "driving"
            
            let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&mode=\(travelMode)&key=\(Constants.GOOGLE_API_KEY)")!
            Alamofire.request(url).responseJSON{(response) -> Void in
                switch response.result {
                case .success(let data):
                    let json = JSON(data)
                    if let routesJson = json["routes"].arrayObject {
                        let routes = routesJson as! [[String: AnyObject]]
                        if routes.count > 0 {
                            for route in routes {
                                /* get the point */
                                let overviewPolyline = route["overview_polyline"]?["points"] as! String
                                let path = GMSMutablePath(fromEncodedPath: overviewPolyline)
                                /* set up polyline */
                                let polyline = GMSPolyline.init(path: path)
                                polyline.strokeWidth = 4
                                polyline.strokeColor = UIColor(named: "GMS Polyline Color")!
                                polyline.zIndex = 10;
                                polyline.map = self.mapView
                                let borderPolyline = GMSPolyline.init(path: path)
                                borderPolyline.strokeWidth = polyline.strokeWidth * 1.4
                                borderPolyline.strokeColor = UIColor(named: "GMS Polyline Border Color")!
                                borderPolyline.zIndex = polyline.zIndex - 1;
                                borderPolyline.map = self.mapView
                            }
                            
                            // fit map view to show all markers
                            self.mapView.animate(with: GMSCameraUpdate.fit(gmsBounds, withPadding: 50.0))
                        }
                    }

                case .failure(let error):
                    self.dialogManager.showError(error: error.localizedDescription)

                }
            }
        }
    }
    
    private func validate() -> Bool {
        if truckType == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:TruckType", comment: ""))
            return false
        }
        if source == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:SourceAddress", comment: ""))
            return false
        }
        if destination == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:DestinationAddress", comment: ""))
            return false
        }
        if pickUpDate == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:PickUpDate", comment: ""))
            return false
        }
        if dropOffDate == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:DropOffDate", comment: ""))
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? AddressModalViewController {
            if segue.identifier == "SourceAddressSegue" {
                viewController.title = NSLocalizedString("Application:SourceAddress", comment: "")
                if let address = source {
                    viewController.selectedAddress = address
                }
            } else {
                viewController.title = NSLocalizedString("Application:DestinationAddress", comment: "")
                if let address = destination {
                    viewController.selectedAddress = address
                }
            }
        }
        
//        if let viewController = segue.destination as? AdditionalServiceModalViewController {
//            viewController.additionalServices = truckType!.additionalServices
//            viewController.selectedAdditionalServices = additionalServices
//        }
        
        if let viewController = segue.destination as? DateModalViewController {
            if let post = self.post {
                viewController.startDate = post.receivedDate.fromDate()
                viewController.isBackhaulWork = true
            } else {
                viewController.startDate = Date()
                viewController.isBackhaulWork = false
            }
            
            viewController.selectedPickUpDate = pickUpDate
            viewController.selectedPickUpTime = pickUpTime
            viewController.selectedDropOffDate = dropOffDate
            viewController.selectedDropOffTime = dropOffTime
        }
        
        if let viewController = segue.destination as? MapViewController {
            if let source = source {
                work.source = source
            }
            if let destination = destination {
                work.destination = destination
            }
            viewController.work = work
        }
        
        if let viewController = segue.destination as? CreateWorkItemViewController {
            viewController.previousViewController = self
            viewController.work = work
        }
        
        if let viewController = segue.destination as? PostViewController {
            work?.source = source ?? Address()
            work?.destination = destination ?? Address()
            viewController.temporaryWork = work
        }
        
    }
    
    @IBAction func prepare(for unwindsSegue: UIStoryboardSegue) {
        
        if let sourceViewController = unwindsSegue.source as? AddressModalViewController {
            if let address = sourceViewController.selectedAddress {
                if sourceViewController.title == NSLocalizedString("Application:SourceAddress", comment: "") {
                    source = address
                    sourceAddressView.setAddress(source!)
                } else {
                    destination = address
                    destinationAddressView.setAddress(destination!)
                }
                
            } else {
                if sourceViewController.title == NSLocalizedString("Application:SourceAddress", comment: "") {
                    source = nil
                    sourceAddressView.reset()
                } else {
                    destination = nil
                    destinationAddressView.reset()
                }
            }

            calculateTotalFees()
        }
        
//        if let sourceViewController = unwindsSegue.source as? AdditionalServiceModalViewController {
//            additionalServices = sourceViewController.selectedAdditionalServices
//            tableView.reloadData()
//            calculateTotalFees()
//        }
        
        if let sourceViewController = unwindsSegue.source as? DateModalViewController {
            if let pickUpDate = sourceViewController.selectedPickUpDate, let pickUpTime = sourceViewController.selectedPickUpTime,
                let dropOffDate = sourceViewController.selectedDropOffDate, let dropOffTime = sourceViewController.selectedDropOffTime {
                
                self.pickUpDate = pickUpDate
                self.pickUpTime = pickUpTime
                self.dropOffDate = dropOffDate
                self.dropOffTime = dropOffTime
                
                dateView.set(receivedDate: pickUpDate.fromDate().toDateWithFullDayWithoutYearAppFormat(),
                             deliveredDate: dropOffDate.fromDate().toDateWithFullDayWithoutYearAppFormat())
                
                print("\(pickUpDate) \(pickUpTime) / \(dropOffDate) \(dropOffTime)")
            }
        }
        
        if unwindsSegue.source is CreateWorkSummaryViewController || unwindsSegue.source is CreateWorkCreditCardMethodViewController {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = storyBoard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
            tabBarController.selectedIndex = 1 // my work tab
            UIApplication.shared.keyWindow?.rootViewController = tabBarController
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
        }
        
        if let sourceViewController = unwindsSegue.source as? WorkDetailViewController {
            work = sourceViewController.temporaryWork
            source = work!.source
            destination = work!.destination
            pickUpDate = nil
            pickUpTime = nil
            dropOffDate = nil
            dropOffTime = nil
//            additionalServices = work!.additionalServices
            
            if let truckTypes = viewModel.truckTypes {
                for i in 0..<truckTypes.count {
                    let truckType = truckTypes[i]
                    if truckType == work!.truckType {
                        self.truckType = truckType
                        
                        let indexPath = IndexPath(item: i, section: 0)
                        self.truckTypeView.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                            self.collectionViewSelectCenterItem()
                        }
                        break
                    }
                }
            }
            sourceAddressView.setAddress(source!)
            destinationAddressView.setAddress(destination!)
            dateView.reset()
        }
        
    }
    
}

extension CreateWorkViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.truckTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let truckType = viewModel.truckTypes[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TruckTypeCell", for: indexPath) as! TruckTypeCollectionViewCell
        cell.initTruckType(truckType)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionViewSelectItem(at: indexPath)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        collectionViewSelectCenterItem()
    }
    
    func collectionViewSelectItem(at indexPath: IndexPath) {
        let collectionView = truckTypeView.collectionView!
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        calculateTotalFees()
    }
    
    func collectionViewSelectCenterItem() {
        if !viewModel.truckTypes.isEmpty {
            let collectionView = truckTypeView.collectionView!
            let centerPoint = CGPoint(x: collectionView.center.x + collectionView.contentOffset.x,
                                      y: collectionView.center.y + collectionView.contentOffset.y)
            if let indexPath = collectionView.indexPathForItem(at: centerPoint) {
                collectionViewSelectItem(at: indexPath)
            }
        }
    }
    
}
