//
//  CreateWorkSummaryViewController.swift
//  OKTruck
//
//  Created by amberhello on 5/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class CreateWorkSummaryViewController: BaseViewController {
    
    @IBOutlet weak var paymentMethodStackView: UIStackView!
    @IBOutlet weak var promotionView: PromotionView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userTelephoneNumberLabel: UILabel!
    @IBOutlet weak var SourceAddressNameLabel: UILabel!
    @IBOutlet weak var DestinationAddressNameLabel: UILabel!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var pickUpTimeLabel: UILabel!
    @IBOutlet weak var dropOffDateLabel: UILabel!
    @IBOutlet weak var dropOffTimeLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemQuantityLabel: UILabel!
    @IBOutlet weak var itemWeightLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var summaryFeeStackView: SummaryFeeStackView!
    @IBOutlet weak var truckTypeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var footerView: CreateWorkFooterView!
    private var viewModel: CreateWorkSummaryViewModel!
    private var dialogManager: DialogManager!
    private var user: User!
    var work: Work!
    
    var paymentMethodStatus: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = CreateWorkSummaryViewModel(self)
        dialogManager = DialogManager.default()
        user = User.shared
        
        disposable = viewModel.getDistance(sourceLatitude: work.source.latitude,
                                           sourceLongitude: work.source.longitude,
                                           destinationLatitude: work.destination.latitude,
                                           destinationLongitude: work.destination.longitude)
    }
    
    private func initView() {
        paymentMethodStackView.subviews[1].isHidden = true
        
        promotionView.codeTextField.delegate = self
        promotionView.initView()
        
        userNameLabel.text = work.userName
        userTelephoneNumberLabel.text = work.userTelephoneNumber
        SourceAddressNameLabel.text = work.source.name
        DestinationAddressNameLabel.text = work.destination.name
        pickUpDateLabel.text = work.source.date.fromDate().toDateWithFullDayWithoutYearAppFormat()
        if work.source.time == "เวลาใดก็ได้" {
            pickUpTimeLabel.text = NSLocalizedString("Application:Anytime", comment: "")
        } else {
            pickUpTimeLabel.text = work.source.time
        }
        dropOffDateLabel.text = work.destination.date.fromDate().toDateWithFullDayWithoutYearAppFormat()
        if work.destination.time == "เวลาใดก็ได้" {
            dropOffTimeLabel.text = NSLocalizedString("Application:Anytime", comment: "")
        } else {
            dropOffTimeLabel.text = work.destination.time
        }
        itemNameLabel.text = work.item.name
        itemQuantityLabel.text = work.item.quantity
        itemWeightLabel.text = work.item.weight
        if !work.note.isEmpty { noteLabel.text = work.note }
        else { noteLabel.text = "-" }
        truckTypeLabel.text = work.truckType.name
        
        initTotalFeeLabel()
    }
    
    func initTotalFeeLabel() {
        summaryFeeStackView.initView()
        summaryFeeStackView.totalFeeLabel.text = work.getReadablePrice()
        footerView.totalFeeLabel.text = work.getReadablePrice()
    }
    
    @IBAction func checkPromotionCode(_ sender: Any) {
        if !promotionView.codeTextField.isNilOrEmpty {
            disposable = self.viewModel.checkPromotion(code: promotionView.codeTextField.text!)
        }
    }
    
    @IBAction func pay(_ sender: Any) {
        if validate() {
            dialogManager.showConfirm(title: NSLocalizedString("Alert:CreateWork:title", comment: ""), message: NSLocalizedString("Alert:CreateWork:message", comment: "")) {
                switch self.paymentMethodStatus {
                case PaymentMethod.WALLET.status:
                    self.disposable = self.viewModel.createWorkWithWallet(self.work)
                default:
                    self.disposable = self.viewModel.createWorkWithCash(self.work, paymentMethod: self.paymentMethodStatus!)
                }
            }
        }
    }
    
    func clearPromotionCode() {
        work.promotionCode = nil
        promotionView.initView()
        initTotalFeeLabel()
    }
    
    private func validate() -> Bool {
        if paymentMethodStatus == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:PaymentMethod", comment: ""))
            return false
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let viewController = segue.destination as? PaymentMethodModalViewController {
            viewController.work = work
        }
        
    }
    
    @IBAction func prepare(for unwindsSegue: UIStoryboardSegue) {
        
        if let sourceViewController = unwindsSegue.source as? PaymentMethodModalViewController {
            paymentMethodStatus = sourceViewController.paymentMethodStatus
            
            let paymentMethodView = paymentMethodStackView.subviews[1] as! PaymentMethodView
            
            switch sourceViewController.paymentMethodStatus {
            case PaymentMethod.WALLET.status:
                paymentMethodView.icon.image = UIImage(named: "Wallet Icon")
                paymentMethodView.nameLabel.text = "Wallet"
                paymentMethodView.detailLabel.text = NSLocalizedString("PaymentMethodModalViewController:creditLabel:message", comment: "") + user.getCredit()
            case PaymentMethod.CASH_SOURCE.status:
                paymentMethodView.icon.image = UIImage(named: "Dollar Note Icon")
                paymentMethodView.nameLabel.text = PaymentMethod.CASH_SOURCE.pay
                paymentMethodView.detailLabel.text = work.source.name
            default:
                paymentMethodView.icon.image = UIImage(named: "Dollar Note Icon")
                paymentMethodView.nameLabel.text = PaymentMethod.CASH_DESTINATION.pay
                paymentMethodView.detailLabel.text = work.destination.name
            }
            
            paymentMethodStackView.subviews[0].isHidden = true
            paymentMethodStackView.subviews[1].isHidden = false
        }
        
    }

}

extension CreateWorkSummaryViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let promotionCodeTextField = promotionView.codeTextField
        if textField == promotionCodeTextField {
            clearPromotionCode()
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 8
        }
        return true
    }
    
}
