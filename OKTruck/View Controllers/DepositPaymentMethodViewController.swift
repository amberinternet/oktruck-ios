//
//  DepositPaymentMethodViewController.swift
//  OKTruck
//
//  Created by amberhello on 13/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class DepositPaymentMethodViewController: BaseViewController {
    
    @IBOutlet weak var priceLabel: UILabel!
    var price: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        priceLabel.text = "฿\(price.roundDecimalFormat())"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? DepositCreditCardMethodViewController {
            viewController.price = Int(price)
        }
        
        if let viewController = segue.destination as? DepositTransferMethodViewController {
            viewController.price = Int(price)
        }
        
    }
    
}
