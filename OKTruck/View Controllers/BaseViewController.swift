//
//  BaseViewController.swift
//  OKTruck
//
//  Created by amberhello on 14/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import KeychainSwift
import RxSwift

class BaseViewController: UIViewController {
    
    var disposable: Disposable? = nil
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
}
