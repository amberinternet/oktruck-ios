//
//  SecondViewController.swift
//  OKTrucker
//
//  Created by amberhello on 18/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class SettingViewController: BaseViewController {
    
    @IBOutlet weak var buildVersionLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var telephoneLabel: UILabel!
    @IBOutlet weak var joinedLabel: UILabel!
    private var viewModel: SettingViewModel!
    private var dialogManager: DialogManager!
    private var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = SettingViewModel(self)
        user = User.shared
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        buildVersionLabel.text = version()
        profileImageView.af_setImage(withURL: URL(string: user.getFullProfileImageURI())!)
        userIdLabel.text = user.readableUserId
        fullNameLabel.text = user.getFullName()
        emailLabel.text = user.email
        telephoneLabel.text = user.telephoneNumber
        joinedLabel.text = user.createdAt.fromDateTime().toDateAppFormat()
    }

    @IBAction func logout(_ sender: Any) {
        dialogManager.showConfirmLogOut() {
            self.viewModel.logOut()
        }
    }
    
    private func version() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(NSLocalizedString("Application:Version", comment: "")): \(version) build \(build)"
    }
    
}

