//
//  PostDetailViewController.swift
//  OKTruck
//
//  Created by amberhello on 10/1/2563 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON

class PostDetailViewController: BaseViewController {
    
    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var truckTypeLabel: UILabel!
    @IBOutlet weak var licensePlateLabel: UILabel!
    @IBOutlet weak var sourceProvinceLabel: UILabel!
    @IBOutlet weak var destinationProvinceLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var receivedDateLabel: UILabel!
    @IBOutlet weak var receivedTimeLabel: UILabel!
    @IBOutlet weak var deliveredDateLabel: UILabel!
    @IBOutlet weak var deliveredTimeLabel: UILabel!
    @IBOutlet weak var passedProvincesStackView: UIStackView!
    @IBOutlet weak var remarkLabel: UILabel!
    private var dialogManager: DialogManager!
    var post: Post!
    var temporaryWork: Work?

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
    }
        
    private func initView() {
//        truckImageView.af_setImage(withURL: URL(string: post.getTruckType().getImagePath())!)
        truckImageView.image = post.getTruckType().image
        truckTypeLabel.text = post.getTruckType().name
        licensePlateLabel.text = post.driver.truck.licensePlate
        sourceProvinceLabel.text = post.sourceProvince
        destinationProvinceLabel.text = post.destinationProvince
        initMapView()
        receivedDateLabel.text = post.getReceivedShotDate()
        receivedTimeLabel.text = post.getReceivedTime()
        deliveredDateLabel.text = post.getDeliveredShotDate()
        deliveredTimeLabel.text = post.getDeliveredTime()
        for subview in passedProvincesStackView.arrangedSubviews { subview.removeFromSuperview() }
        if !post.passedProvinces.isEmpty {
            for province in post.passedProvinces {
                let label = UILabel()
                label.font = UIFont.systemFont(ofSize: 14)
                label.textColor = UIColor(named: "Text Color")
                label.text = province
                passedProvincesStackView.addArrangedSubview(label)
            }
        } else {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 14)
            label.textColor = UIColor(named: "Text Color")
            label.text = "-"
            passedProvincesStackView.addArrangedSubview(label)
        }
        if !post.remark.isEmpty { remarkLabel.text = post.remark }
        else { remarkLabel.text = "-" }
    }
    
    func initMapView() {
        let thailandLocation = CLLocationCoordinate2D(latitude: 13.7245601, longitude: 100.4930287)
        mapView.camera = GMSCameraPosition.camera(withTarget: thailandLocation, zoom: 8)
        
        var sourceLocation, destinationLocation: CLLocationCoordinate2D!
        
        // google places filter
        let filter: GMSAutocompleteFilter = {
            let filter = GMSAutocompleteFilter()
            filter.country = "TH"
            return filter
        }()
        
        // Search source places
        GMSPlacesClient.shared().autocompleteQuery(post.sourceProvince, bounds: nil, filter: filter, callback: { (result: [GMSAutocompletePrediction]?, error: Error?) in
            if let place = result?.first {
                // Get a place detail by ID
                GMSPlacesClient.shared().fetchPlace(fromPlaceID: place.placeID, placeFields:  GMSPlaceField.all, sessionToken: nil, callback: {
                    (place: GMSPlace?, error: Error?) in
                    if let place = place {
                        sourceLocation = place.coordinate
                        
                        // Search destination places
                        GMSPlacesClient.shared().autocompleteQuery(self.post.destinationProvince, bounds: nil, filter: filter, callback: { (result: [GMSAutocompletePrediction]?, error: Error?) in
                            if let place = result?.first {
                                // Get a place detail by ID
                                GMSPlacesClient.shared().fetchPlace(fromPlaceID: place.placeID, placeFields:  GMSPlaceField.all, sessionToken: nil, callback: {
                                    (place: GMSPlace?, error: Error?) in
                                    if let place = place {
                                        destinationLocation = place.coordinate
                                        
                                        // draw path between markers
                                        let source = "\(sourceLocation.latitude),\(sourceLocation.longitude)"
                                        let destination = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
                                        let travelMode = "driving"
                                        
                                        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&mode=\(travelMode)&key=\(Constants.GOOGLE_API_KEY)")!
                                        Alamofire.request(url).responseJSON{(response) -> Void in
                                            switch response.result {
                                            case .success(let data):
                                                let json = JSON(data)
                                                if let routesJson = json["routes"].arrayObject {
                                                    let routes = routesJson as! [[String: AnyObject]]
                                                    if routes.count > 0 {
                                                        for route in routes {
                                                            /* get the point */
                                                            let overviewPolyline = route["overview_polyline"]?["points"] as! String
                                                            let path = GMSMutablePath(fromEncodedPath: overviewPolyline)
                                                            /* set up polyline */
                                                            let polyline = GMSPolyline.init(path: path)
                                                            polyline.strokeWidth = 4
                                                            polyline.strokeColor = UIColor(named: "GMS Polyline Color")!
                                                            polyline.zIndex = 10;
                                                            polyline.map = self.mapView
                                                            let borderPolyline = GMSPolyline.init(path: path)
                                                            borderPolyline.strokeWidth = polyline.strokeWidth * 1.4
                                                            borderPolyline.strokeColor = UIColor(named: "GMS Polyline Border Color")!
                                                            borderPolyline.zIndex = polyline.zIndex - 1;
                                                            borderPolyline.map = self.mapView
                                                        }
                                                    }
                                                }

                                            case .failure(let error):
                                                self.dialogManager.showError(error: error.localizedDescription)

                                            }
                                        }
                                        
                                        // fit map view to show all markers
                                        var gmsBounds = GMSCoordinateBounds()
                                        gmsBounds = gmsBounds.includingCoordinate(sourceLocation)
                                        gmsBounds = gmsBounds.includingCoordinate(destinationLocation)
                                        self.mapView.animate(with: GMSCameraUpdate.fit(gmsBounds, withPadding: 10.0))
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? MapViewController {
            viewController.post = post
        }
        
        if let viewController = segue.destination as? CreateWorkViewController {
            viewController.post = post
            viewController.temporaryWork = temporaryWork
        }
        
    }

}
