//
//  WalletViewController.swift
//  OKTruck
//
//  Created by amberhello on 27/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class WalletViewController: BaseViewController {
    
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: WalletViewModel!
    let refreshControl = UIRefreshControl()
    private var user: User!
    private var monthQuery: Int!
    private var yearQuery: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshTransactionList()
    }
    
    private func initVariable() {
        viewModel = WalletViewModel(self)
        disposable = viewModel.getProfile()
        user = User.shared
        monthQuery = Date().getMonth()
        yearQuery = Date().getYear()
    }
    
    private func initView() {
        creditLabel.text = user.getReadableCredit()
        
        // Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshTransactionList), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refreshTransactionList() {
        viewModel.isRefresh = true
        disposable = viewModel.getProfile()
        disposable = viewModel.getTransactionList(date: getMonthYearFilter())
        print("TableView is refresh.")
        print(getMonthYearFilter())
    }
    
    func getMonthYearFilter() -> String {
        let year = yearQuery!
        let month = String(format: "%02d", monthQuery)
        return "\(year)-\(month)"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? FilterTransactionViewController{
            viewController.month = monthQuery
            viewController.year = yearQuery
        }
        
        if let viewController = segue.destination as? DepositPaymentMethodViewController, let senderViewController = sender as? DepositModalViewController {
            viewController.price = senderViewController.selectedPrice
        }
        
    }
    
    @IBAction func prepare(for unwindsSegue: UIStoryboardSegue) {
        
        if let sourceViewController = unwindsSegue.source as? FilterTransactionViewController {
            monthQuery = sourceViewController.month
            yearQuery = sourceViewController.year
        } else {
            monthQuery = Date().getMonth()
            yearQuery = Date().getYear()
        }
        
        if let sourceViewController = unwindsSegue.source as? DepositModalViewController {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "DepositPaymentMethodSegue", sender: sourceViewController)
            }
        }
        
    }

}

extension WalletViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.transactionList.isEmpty {
            return 1
        }
        
        return viewModel.transactionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.transactionList.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath)
            cell.frame.size.height = 45
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionViewCell
        cell.initView(transaction: viewModel.transactionList[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.transactionPagination == nil {
            return
        }
        
        if indexPath.item == (viewModel.transactionList.count - 1) {
            disposable = viewModel.getTransactionList(page: viewModel.transactionPagination.currentPage + 1, date: getMonthYearFilter())
        }
    }
    
}
