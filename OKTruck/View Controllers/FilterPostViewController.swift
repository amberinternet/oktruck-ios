//
//  FilterPostViewController.swift
//  OKTruck
//
//  Created by amberhello on 20/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class FilterPostViewController: BaseViewController {
    
    @IBOutlet weak var sourceProvinceTextField: AutocompleteTextField!
    @IBOutlet weak var destinationProvinceTextField: AutocompleteTextField!
    @IBOutlet weak var receivedDateDropDownView: DropDownView!
    @IBOutlet weak var selectTruckButton: TruckTypeButtonView!
    private var viewModel: FilterPostViewModel!
    
    var sourceProvince: String!
    var destinationProvince: String!
    var receivedDate: String!
    var truckType: TruckType?

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = FilterPostViewModel(self)
        disposable = viewModel.getTruckType()
    }
    
    private func initView() {
        sourceProvinceTextField.initDropDown(data: Arrays.provinces)
        sourceProvinceTextField.text = sourceProvince
        destinationProvinceTextField.initDropDown(data: Arrays.provinces)
        destinationProvinceTextField.text = destinationProvince
        
        var dateList = [String]()
        var startDate = Date()
        dateList.append(NSLocalizedString("Application:Everyday", comment: ""))
        dateList.append(startDate.toDateWithFullDayAppFormat())
        for _ in 1...7 {
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
            startDate = tomorrow
            dateList.append(startDate.toDateWithFullDayAppFormat())
        }
        receivedDateDropDownView.initDropDown(data: dateList)
        
        if let truckType = truckType {
            selectTruckButton.selected(truckType: truckType)
        }
    }
    
    @IBAction func filter(_ sender: Any) {
        performSegue(withIdentifier: "FilterPostUnwindSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? TruckTypeModalViewController {
            viewController.truckTypes = viewModel.truckTypes
            viewController.selectedTruckType = truckType
        }
        
    }
    
    @IBAction func prepare(for unwindsSegue: UIStoryboardSegue) {
        
        if let sourceViewController = unwindsSegue.source as? TruckTypeModalViewController {
            if let selectedTruckType = sourceViewController.selectedTruckType {
                truckType = selectedTruckType
                selectTruckButton.selected(truckType: truckType!)
            } else {
                truckType = nil
                selectTruckButton.selectedAllTypes()
            }
        }
        
    }

}

