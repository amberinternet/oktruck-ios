//
//  PostViewController.swift
//  OKTruck
//
//  Created by amberhello on 19/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift

class PostViewController: UICollectionViewController {
    
    private var viewModel: PostViewModel!
    let refreshControl = UIRefreshControl()
    private var sourceProvince: String!
    private var destinationProvince: String!
    private var receivedDate: String!
    private var truckType: TruckType?
    private var disposable: Disposable? = nil
    var temporaryWork: Work?

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshPostList()
    }
        
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
    private func initVariable() {
        viewModel = PostViewModel(self)
        sourceProvince = Constants.INITIAL_STRING
        destinationProvince = Constants.INITIAL_STRING
        receivedDate = Constants.INITIAL_STRING
        truckType = nil
        
        // for search post by infomation from temporary work for recreate backhaul work
        if let temporaryWork = temporaryWork {
            sourceProvince = temporaryWork.source.province
            destinationProvince = temporaryWork.destination.province
        }
    }
    
    private func initView() {
        // Fit cell to screen width
        let collectionViewLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth = UIScreen.main.bounds.width - (collectionViewLayout.minimumInteritemSpacing * 2)
        collectionViewLayout.itemSize = CGSize(width: cellWidth, height: collectionViewLayout.itemSize.height)
        
        // Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshPostList), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc private func refreshPostList() {
        viewModel.isRefresh = true
        loadPostList(page: 1)
        print("CollectionView is refresh.")
    }
    
    private func loadPostList(page: Int) {
        let truckTypeId: String
        if let truckType = truckType { truckTypeId = String(truckType.id)
        } else { truckTypeId = Constants.INITIAL_STRING }
        disposable = viewModel.getPostList(page: page, sourceProvince: sourceProvince, destinationProvince: destinationProvince, receivedDate: receivedDate, truckTypeId: truckTypeId)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? FilterPostViewController {
            viewController.sourceProvince = sourceProvince
            viewController.destinationProvince = destinationProvince
            viewController.receivedDate = receivedDate
            viewController.truckType = truckType
        }
        
        if let viewController = segue.destination as? PostDetailViewController {
            let indexPath = collectionView.indexPathsForSelectedItems!.first!
            let post = viewModel.postList[indexPath.item]

            viewController.post = post
            viewController.temporaryWork = temporaryWork
        }
        
    }
    
    @IBAction func prepare(for unwindsSegue: UIStoryboardSegue) {
        
        if let sourceViewController = unwindsSegue.source as? FilterPostViewController {
            sourceProvince = sourceViewController.sourceProvinceTextField.selectedItem ?? Constants.INITIAL_STRING
            destinationProvince = sourceViewController.destinationProvinceTextField.selectedItem ?? Constants.INITIAL_STRING
            
            if let selectedReceivedDate = sourceViewController.receivedDateDropDownView.selectedItem {
                receivedDate = selectedReceivedDate.fromDateWithFullDayAppFormat().toDateServerFormat()
            } else {
                receivedDate = Constants.INITIAL_STRING
            }
            
            if let truckType = sourceViewController.truckType {
                self.truckType = truckType
            } else {
                truckType = nil
            }
        }
        
    }

    // collectionView dataSource & delegate
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.postList.isEmpty {
            return 1
        }
        
        return viewModel.postList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewModel.postList.isEmpty {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCell", for: indexPath)
            cell.frame.size.height = 45
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCell", for: indexPath) as! PostCollectionViewCell
        cell.initView(post: viewModel.postList[indexPath.item])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.postPagination == nil {
            return
        }
        
        if indexPath.item == (viewModel.postList.count - 1) {
            loadPostList(page: viewModel.postPagination.currentPage + 1)
        }
    }

}
