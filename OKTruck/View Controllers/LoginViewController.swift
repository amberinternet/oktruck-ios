//
//  LoginViewController.swift
//  OKTruck
//
//  Created by amberhello on 13/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var emailTextField: UnderlineTextField!
    @IBOutlet weak var passwordTextField: UnderlineTextField!
    @IBOutlet weak var logInButton: RoundButton!
    @IBOutlet weak var signUpButton: RoundButton!
    @IBOutlet weak var acceptMessageLabel: UILabel!
    @IBOutlet var registerTapGestureRecognizer: UITapGestureRecognizer!
    private var viewModel: LoginViewModel!
    private var dialogManager: DialogManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initVariable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor(named: "Primary Color")
    }
    
    private func initView() {
        let logo = UIImage(named: "OKTruck Logo (Small)")
        let imageView = UIImageView(image: logo)
        self.navigationItem.titleView = imageView
        
        let message = NSLocalizedString("LoginViewController:AcceptMessage", comment: "")
        let subMessage1 = NSLocalizedString("Application:TermsAndConditionsOfService", comment: "")
        let subMessage2 = NSLocalizedString("Application:PrivacyPolicy", comment: "")
        
        let attributedString = NSMutableAttributedString.init(string: message)
        var range = (message as NSString).range(of: subMessage1)
        attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue , range: range)
        range = (message as NSString).range(of: subMessage2)
        attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue , range: range)
        
        acceptMessageLabel.attributedText = attributedString
    }
    
    private func initVariable() {
        viewModel = LoginViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    @IBAction func logIn(_ sender: Any) {
        if (validate()) {
            logInWithTelephone()
        }
    }
    
    @IBAction func showAcceptMessage(_ sender: Any) {
        let acceptMessageDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        acceptMessageDialog.view.tintColor = UIColor(named: "Primary Color")
        
        var title = NSLocalizedString("Application:TermsAndConditionsOfService", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: title, style: .default , handler:{ _ in
            self.performSegue(withIdentifier: "Terms and Conditions Segue", sender: sender)
        }))
        
        title = NSLocalizedString("Application:PrivacyPolicy", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: title, style: .default , handler:{ _ in
            self.performSegue(withIdentifier: "Privacy Policy Segue", sender: sender)
        }))
        
        title = NSLocalizedString("Application:Dismiss", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: title, style: .cancel, handler: nil))
        
        self.present(acceptMessageDialog, animated: true, completion: nil)
    }
    
    private func validate() -> Bool {
        if emailTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:Email", comment: ""))
            return false
        }
//        if !emailTextField.isValidEmail {
//            dialogManager.showAlert(message: NSLocalizedString("Validate:EmailInvalid", comment: ""))
//            return false
//        }
        if passwordTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:Password", comment: ""))
            return false
        }
        return true
    }
    
    func enabledView(isEnabled: Bool) {
        emailTextField.isUserInteractionEnabled = isEnabled
        passwordTextField.isUserInteractionEnabled = isEnabled
        logInButton.isUserInteractionEnabled = isEnabled
        signUpButton.isUserInteractionEnabled = isEnabled
        acceptMessageLabel.isUserInteractionEnabled = isEnabled
    }
    
    private func logInWithTelephone() {
        // TODO:  getFirebaseToken
        disposable = viewModel.logInWithTelephone(email: emailTextField.text!, password: passwordTextField.text!, firebaseToken: "")
    }
    
    func goToMainStoryBoard() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateInitialViewController()!
        self.present(nextViewController, animated:true, completion:nil)
    }
    
}
