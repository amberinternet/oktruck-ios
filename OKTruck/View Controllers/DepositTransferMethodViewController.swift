//
//  DepositTransferMethodViewController.swift
//  OKTrucker
//
//  Created by amberhello on 3/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AVFoundation

class DepositTransferMethodViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var dayDropDownView: DropDownView!
    @IBOutlet weak var monthDropDownView: DropDownView!
    @IBOutlet weak var yearDropDownView: DropDownView!
    @IBOutlet weak var hourDropDownView: DropDownView!
    @IBOutlet weak var minuteDropDownView: DropDownView!
    @IBOutlet weak var imageView: UIImageView!
    private var viewModel: DepositTransferMethodViewModel!
    private var dialogManager: DialogManager!
    private let imagePickerController = UIImagePickerController()
    private var imagePickerDialog: UIAlertController!
    var price: Int!
    private var paySlipData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = DepositTransferMethodViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        dayDropDownView.initDropDown(data: Arrays.day_list)
        dayDropDownView.selectRow(at: Date().getDay() - 1)
        
        monthDropDownView.initDropDown(data: DateFormatter().monthSymbols)
        monthDropDownView.selectRow(at: Date().getMonth() - 1)
        
        let startYear = User.shared.createdAt.fromDateTime().getYear()
        let currentYear = Date().getYear()
        let buddhistYearList = (startYear...currentYear).map { String($0 + 543) }
        yearDropDownView.initDropDown(data: buddhistYearList)
        yearDropDownView.selectRow(at: currentYear - startYear)
        
        hourDropDownView.initDropDown(data: Arrays.hour_list)
        hourDropDownView.selectRow(at: Date().getHour())
        
        minuteDropDownView.initDropDown(data: Arrays.minute_list)
        minuteDropDownView.selectRow(at: Date().getMinute())
        
        initImagePickerDialog()
    }
    
    private func initImagePickerDialog() {
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.mediaTypes = ["public.image"]
        
        imagePickerDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        imagePickerDialog.view.tintColor = UIColor(named: "Primary Color")
        
        var message = NSLocalizedString("Application:TakePhoto", comment: "")
        imagePickerDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ _ in
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized, .notDetermined:
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            default:
                self.showRequestCameraAccessDialog()
            }
        }))
        
        message = NSLocalizedString("Application:PhotoLibrary", comment: "")
        imagePickerDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ _ in
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }))
        
        message = NSLocalizedString("Application:Dismiss", comment: "")
        imagePickerDialog.addAction(UIAlertAction(title: message, style: .cancel))
    }
    
    private func showRequestCameraAccessDialog() {
        let title = NSLocalizedString("Application:CameraAccessDenied:title", comment: "")
        let message = NSLocalizedString("Application:CameraAccessDenied:message", comment: "")
        let settingsUrl = UIApplication.openSettingsURLString
        dialogManager.showRequestAccessDialog(title: title, message: message, settingsUrl: settingsUrl)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        let paySlipImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        imageView.image = paySlipImage
        paySlipData = paySlipImage?.resize(to: 512).jpegData(compressionQuality: 1.0)!
    }
    
    @IBAction func attachSlip(_ sender: Any) {
        self.present(imagePickerDialog, animated: true, completion: nil)
    }
    
    @IBAction func confirm(_ sender: Any) {
        if validate() {
            let year = Int(yearDropDownView.selectedItem!)
            let month = monthDropDownView.selectedId
            let day = dayDropDownView.selectedId
            let hour = hourDropDownView.selectedIndex
            let minute = minuteDropDownView.selectedIndex
            let payTime = DateComponents(calendar: Calendar(identifier: .buddhist),
                                         year: year, month: month, day: day, hour: hour, minute: minute).date!.toDateTimeServerFormat()
            
            disposable = viewModel.requestTransferMethod(depositType: DepositType.TRANSFER.value,
                                                         price: price,
                                                         payTime: payTime,
                                                         paySlipData: paySlipData!)
            print(price!)
            print(payTime)
        }
    }
    
    private func validate() -> Bool {
        if paySlipData == nil {
            dialogManager.showAlert(message: NSLocalizedString("Validate:AttachedTransferSlip", comment: ""))
            return false
        }
        return true
    }
    
}
