//
//  OTPViewController.swift
//  OKTruck
//
//  Created by amberhello on 13/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OTPViewController: BaseViewController {
    
    @IBOutlet weak var sendOTPLabel: UILabel!
    @IBOutlet weak var OTPTextField: UnderlineTextField!
    private var viewModel: OTPViewModel!
    private var dialogManager: DialogManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = OTPViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        let message1 = NSLocalizedString("OTPViewController:sendOTPLabel:message1", comment: "")
        let message2 = NSLocalizedString("OTPViewController:sendOTPLabel:message2", comment: "")
        sendOTPLabel.text = message1 + User.shared.telephoneNumber.toTelephoneNumber() + message2
        
        OTPTextField.set(maximumCharacters: 4)
    }
    
    @IBAction func sendOTP(_ sender: Any) {
        if validate() {
            disposable = viewModel.submit(otp: OTPTextField.text!)
        }
    }
    
    @IBAction func requestOTP(_ sender: Any) {
        disposable = viewModel.requestOTP()
    }
    
    private func validate() -> Bool {
        if OTPTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("Validate:OTP", comment: ""))
            return false
        }
        return true
    }
    
}
