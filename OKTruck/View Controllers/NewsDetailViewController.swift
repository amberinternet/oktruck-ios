//
//  NewsDetailViewController.swift
//  OKTruck
//
//  Created by amberhello on 17/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import WebKit

class NewsDetailViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    private var viewModel: NewsDetailViewModel!
    var temporaryNews: News!

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        disposable = viewModel.getNewsDetail(newsId: viewModel.news.id)
    }
    
    private func initVariable() {
        viewModel = NewsDetailViewModel(self)
        viewModel.news = temporaryNews
    }
    
    func initView() {
        titleLabel.text = viewModel.news.title
        dateLabel.text = viewModel.news.postAt.fromDateTime().toDateTimeAppFormat()
        let htmlString = addHTMLHeader(htmlContent: viewModel.news.fullContent!)
        webView.loadHTMLString(htmlString, baseURL: nil)
    }
    
    private func addHTMLHeader(htmlContent: String) -> String {
        let htmlTagOpen = "<html>\n" +
            "      <head>\n" +
            "            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
            "            <style type=\"text/css\">\n" +
            "                  body {\n" +
            "                        font-family: \"Tahoma\"; \n" +
            "                        margin: auto;\n" +
            "                        background: \"#f0f0f0\"; \n" +
            "                        line-height: \"0.85\"; \n" +
            "                        padding-bottom: 16px; \n" +
            "                  } \n" +
            "                  p {\n" +
            "                        font-size: 16px;\n" +
            "                  }\n" +
            "            </style>\n" +
            "      </head>\n" +
            "      <body>"
        
        let htmlTagClose = "</body></html>"
        
        let styleWidth = "<img"
        let newStyleWidth = "<img style=\"max-width: 100%;\""
        let iframeWidth = "<iframe"
        let newIframeWidth = "<iframe style=\"max-width: 100%; height: auto;\""
        
        let htmlBody = htmlContent.replacingOccurrences(of: styleWidth, with: newStyleWidth).replacingOccurrences(of: iframeWidth, with: newIframeWidth)
        
        return htmlTagOpen + htmlBody + htmlTagClose
    }

}
