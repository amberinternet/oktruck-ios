//
//  FullScreenImageViewController.swift
//  OKTrucker
//
//  Created by amberhello on 13/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class FullScreenImageViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    var imagePath: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        // double tap to zoom in/out
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(zoom(tapGesture:)))
        doubleTapGesture.numberOfTapsRequired = 2
        doubleTapGesture.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(doubleTapGesture)
        
        imageView.af_setImage(withURLRequest: ImageAPIRouter.getItemImage(imagePath: imagePath))
    }
    
    @objc func zoom(tapGesture: UITapGestureRecognizer) {
        if (scrollView.zoomScale == scrollView.minimumZoomScale) {
            let center = tapGesture.location(in: scrollView)
            let size = imageView.image!.size
            let zoomRect = CGRect(x: center.x, y: center.y, width: (size.width / 3), height: (size.height / 3))
            scrollView.zoom(to: zoomRect, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        }
    }

}

extension FullScreenImageViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
