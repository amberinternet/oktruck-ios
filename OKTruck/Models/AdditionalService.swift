//
//  AdditionalService.swift
//  OKTruck
//
//  Created by amberhello on 22/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation
import SwiftyJSON

struct AdditionalService {
    
    let id : Int!
    let name : String!
    var charge : Double!
    
    init(json : JSON){
        id = json["id"].intValue
        name = json["additional_service_name"].stringValue
        charge = json["pivot"]["price"].doubleValue
        
        if let chargeString = json["price"].string {
            charge = Double(chargeString)
        }
    }
    
    func getCharge() -> String { return charge.roundDecimalFormat() }
    
}

extension AdditionalService: Equatable {
    static func == (lhs: AdditionalService, rhs: AdditionalService) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.charge == rhs.charge
    }
}

