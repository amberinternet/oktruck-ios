//
//  Address.swift
//  OKTruck
//
//  Created by amberhello on 2/3/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation
import SwiftyJSON

class Address: NSObject, NSCoding {
    
    var name : String = Constants.INITIAL_STRING
    var landmark : String = Constants.INITIAL_STRING
    var contactName : String = Constants.INITIAL_STRING
    var contactTelephoneNumber : String = Constants.INITIAL_STRING
    
    var fullAddress : String = Constants.INITIAL_STRING
    var subdistrict : String = Constants.INITIAL_STRING
    var district : String = Constants.INITIAL_STRING
    var province : String = Constants.INITIAL_STRING
    
    var latitude : Double = Constants.INITIAL_DOUBLE
    var longitude : Double = Constants.INITIAL_DOUBLE
    
    var id : Int = Constants.INITIAL_INT
    var signatureFileName : String = Constants.INITIAL_STRING
    var date : String = Constants.INITIAL_STRING
    var time : String = Constants.INITIAL_STRING
    
    override init() {}
    
    init(source json : JSON) {
        id = json["id"].intValue
        latitude = json["source_latitude"].doubleValue
        longitude = json["source_longitude"].doubleValue
        fullAddress = json["source_full_address"].stringValue
        subdistrict = json["source_address_subdistrict"].stringValue
        district = json["source_address_district"].stringValue
        province = json["source_address_province"].stringValue
        contactName = json["source_contact_name"].stringValue
        contactTelephoneNumber = json["source_contact_telephone_number"].stringValue
        name = json["source_shop_name"].stringValue
        landmark = json["source_landmark"].stringValue
        signatureFileName = json["autograph"].stringValue
        date = json["received_date"].stringValue
        time = json["received_time"].stringValue
    }
    
    init(destination json : JSON) {
        id = json["id"].intValue
        latitude = json["destination_latitude"].doubleValue
        longitude = json["destination_longitude"].doubleValue
        fullAddress = json["destination_full_address"].stringValue
        subdistrict = json["destination_address_subdistrict"].stringValue
        district = json["destination_address_district"].stringValue
        province = json["destination_address_province"].stringValue
        contactName = json["destination_contact_name"].stringValue
        contactTelephoneNumber = json["destination_contact_telephone_number"].stringValue
        name = json["destination_shop_name"].stringValue
        landmark = json["destination_landmark"].stringValue
        signatureFileName = json["autograph"].stringValue
        date = json["delivered_date"].stringValue
        time = json["delivered_time"].stringValue
    }
    
    func getShortAddress() -> String {
        return district + ", " + province
    }
    
    func getReadableTime() -> String {
        if(time == "เวลาใดก็ได้") { return NSLocalizedString("Application:Anytime", comment: "") }
        else { return time + NSLocalizedString("Application:O'Clock", comment: "") }
    }
    
    func getArrivalShotDate() -> String {
        return date.fromDate().toDateWithFullDayWithoutYearAppFormat()
    }
    
    func getArrivalDateTime() -> String {
        return date.fromDate().toDateWithFullDayAppFormat() + " | " + getReadableTime()
    }
    
    func getReadableLandmark() -> String {
        return NSLocalizedString("Application:Landmark", comment: "") + ": " + landmark
    }
    
    func getReadableContact() -> String {
        return "\(contactName) \(contactTelephoneNumber)"
    }
    
    func getSignatureImagePath() -> String {
        return "image/autograph/" + signatureFileName
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "name") as! String
        landmark = aDecoder.decodeObject(forKey: "landmark") as! String
        contactName = aDecoder.decodeObject(forKey: "contact_name") as! String
        contactTelephoneNumber = aDecoder.decodeObject(forKey: "contact_tel") as! String
        
        fullAddress = aDecoder.decodeObject(forKey: "full_address") as! String
        subdistrict = aDecoder.decodeObject(forKey: "subdistrict") as! String
        district = aDecoder.decodeObject(forKey: "district") as! String
        province = aDecoder.decodeObject(forKey: "province") as! String
        
        latitude = aDecoder.decodeDouble(forKey: "latitude")
        longitude = aDecoder.decodeDouble(forKey: "longitude")
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(landmark, forKey: "landmark")
        aCoder.encode(contactName, forKey: "contact_name")
        aCoder.encode(contactTelephoneNumber, forKey: "contact_tel")
        
        aCoder.encode(fullAddress, forKey: "full_address")
        aCoder.encode(subdistrict, forKey: "subdistrict")
        aCoder.encode(district, forKey: "district")
        aCoder.encode(province, forKey: "province")
        
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
    }
    
}
