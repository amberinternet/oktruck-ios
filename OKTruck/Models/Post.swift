//
//  Post.swift
//  OKTruck
//
//  Created by amberhello on 7/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Post {
    
    let id : Int!
    let driverId : Int!
    let workId : Int!
    let status : Int!
    let sourceProvince : String!
    let receivedDate : String!
    let receivedTime : String!
    let destinationProvince : String!
    let deliveredDate : String!
    let deliveredTime : String!
    let remark : String!
    let formattedPostId : String!
    var passedProvinces : [String] = []
    let driver : Driver!
    
    init(json : JSON){
        id = json["id"].intValue
        driverId = json["driver_id"].intValue
        workId = json["work_id"].intValue
        status = json["status"].intValue
        sourceProvince = json["source_province"].stringValue
        receivedDate = json["received_date"].stringValue
        receivedTime = json["received_time"].stringValue
        destinationProvince = json["destination_province"].stringValue
        deliveredDate = json["delivered_date"].stringValue
        deliveredTime = json["delivered_time"].stringValue
        remark = json["post_remark"].stringValue
        formattedPostId = json["formatted_post_id"].stringValue
        if let jsonArray = json["passed_provinces"].array {
            for jsonObject in jsonArray {
                let province = jsonObject.stringValue
                passedProvinces.append(province)
            }
        }
        driver = Driver(json: json["driver"])
    }
    
    func getTruckType() -> TruckType {
        return driver.truck.truckType
    }
    
    private func getReadable(time: String) -> String {
        if(time == "เวลาใดก็ได้") { return NSLocalizedString("Application:Anytime", comment: "") }
        else { return "\(time) \(NSLocalizedString("Application:O'Clock", comment: ""))" }
    }
    
    func getReceivedTime() -> String {
        return getReadable(time: receivedTime)
    }
    
    func getDeliveredTime() -> String {
        return getReadable(time: deliveredTime)
    }
    
    func getReceivedShotDate() -> String {
        return receivedDate.fromDate().toDateWithFullDayWithoutYearAppFormat()
    }
    
    func getDeliveredShotDate() -> String {
        return deliveredDate.fromDate().toDateWithFullDayWithoutYearAppFormat()
    }
    
    func getReceivedDateTime() -> String {
        return receivedDate.fromDate().toDateAppFormat() + " | " + getReadable(time: receivedTime)
    }
    
    func getDeliveredDateTime() -> String {
        return deliveredDate.fromDate().toDateAppFormat() + " | " + getReadable(time: deliveredTime)
    }
    
}

extension Post: Equatable {
    static func == (lhs: Post, rhs: Post) -> Bool {
        return lhs.id == rhs.id &&
            lhs.driverId == rhs.driverId &&
            lhs.workId == rhs.workId &&
            lhs.status == rhs.status &&
            lhs.sourceProvince == rhs.sourceProvince &&
            lhs.receivedDate == rhs.receivedDate &&
            lhs.receivedTime == rhs.receivedTime &&
            lhs.destinationProvince == rhs.destinationProvince &&
            lhs.deliveredDate == rhs.deliveredDate &&
            lhs.deliveredTime == rhs.deliveredTime &&
            lhs.remark == rhs.remark &&
            lhs.formattedPostId == rhs.formattedPostId &&
            lhs.passedProvinces == rhs.passedProvinces &&
            lhs.driver == rhs.driver
    }
}
