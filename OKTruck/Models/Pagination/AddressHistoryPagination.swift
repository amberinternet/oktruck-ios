//
//  AddressHistoryPagination.swift
//  OKTruck
//
//  Created by amberhello on 19/3/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import SwiftyJSON

class AddressHistoryPagination: Pagination {
    
    var addressHistoryList : [Address]! = []
    
    init(source json : JSON) {
        super.init(json: json)
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let sourceAddress = Address(source: jsonObject)
                addressHistoryList.append(sourceAddress)
            }
        }
    }
    
    init(destination json : JSON) {
        super.init(json: json)
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let destinationAddress = Address(destination: jsonObject)
                addressHistoryList.append(destinationAddress)
            }
        }
    }
    
}

extension AddressHistoryPagination: Equatable {
    static func == (lhs: AddressHistoryPagination, rhs: AddressHistoryPagination) -> Bool {
        return lhs.addressHistoryList! == rhs.addressHistoryList!
    }
}

