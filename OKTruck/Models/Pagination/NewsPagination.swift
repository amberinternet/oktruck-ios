//
//  NewsPagination.swift
//  OKTruck
//
//  Created by amberhello on 2/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

class NewsPagination: Pagination {
    
    var newsList : [News]! = []
    
    override init(json : JSON) {
        super.init(json: json)
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let new = News(json: jsonObject)
                newsList.append(new)
            }
        }
    }
    
}

extension NewsPagination: Equatable {
    static func == (lhs: NewsPagination, rhs: NewsPagination) -> Bool {
        return lhs.newsList! == rhs.newsList!
    }
}
