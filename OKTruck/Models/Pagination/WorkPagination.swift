//
//  WorkPagination.swift
//  OKTruck
//
//  Created by amberhello on 2/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

class WorkPagination: Pagination {
    
    var workList : [Work]! = []
    
    override init(json : JSON) {
        super.init(json: json)
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let work = Work(json: jsonObject)
                workList.append(work)
            }
        }
    }
    
}

extension WorkPagination: Equatable {
    static func == (lhs: WorkPagination, rhs: WorkPagination) -> Bool {
        return lhs.workList! == rhs.workList!
    }
}
