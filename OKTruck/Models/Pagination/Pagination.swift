//
//  Pagination.swift
//  OKTruck
//
//  Created by amberhello on 29/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

class Pagination {
    
    var total : Int!
    var currentPage : Int!
    
    init(json : JSON) {
        total = json["total"].intValue
        currentPage = json["current_page"].intValue
    }
    
}
