//
//  PostPagination.swift
//  OKTrucker
//
//  Created by amberhello on 7/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

class PostPagination: Pagination {
    
    var postList : [Post]! = []
    
    override init(json : JSON) {
        super.init(json: json)
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let post = Post(json: jsonObject)
                postList.append(post)
            }
        }
    }
    
}

extension PostPagination: Equatable {
    static func == (lhs: PostPagination, rhs: PostPagination) -> Bool {
        return lhs.postList! == rhs.postList!
    }
}
