//
//  TransactionPagination.swift
//  OKTruck
//
//  Created by amberhello on 2/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

class TransactionPagination: Pagination {
    
    var transactionList : [Transaction]! = []
    
    override init(json : JSON) {
        super.init(json: json)
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let transaction = Transaction(json: jsonObject)
                transactionList.append(transaction)
            }
        }
    }
    
}

extension TransactionPagination: Equatable {
    static func == (lhs: TransactionPagination, rhs: TransactionPagination) -> Bool {
        return lhs.transactionList! == rhs.transactionList!
    }
}
