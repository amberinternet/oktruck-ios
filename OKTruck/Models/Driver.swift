//
//  Driver.swift
//  OKTruck
//
//  Created by amberhello on 8/1/2563 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Driver {
    
    let id : Int!
    let driverTypeId : Int!
    let companyName : String!
    let citizenId : String!
    let driverLicenseId : String!
    let driverLicenseType : String!
    let createdAt : String!
    let readableDriverId : String!
    let truck : Truck!
    
    init(json : JSON) {
        id = json["id"].intValue
        driverTypeId = json["driver_type_id"].intValue
        companyName = json["company_name"].stringValue
        citizenId = json["citizen_id"].stringValue
        driverLicenseId = json["driver_license_id"].stringValue
        driverLicenseType = json["driver_license_type"].stringValue
        createdAt = json["created_at"].stringValue
        readableDriverId = json["formatted_driver_id"].stringValue
        truck = Truck(json: json["car"])
    }
    
}

extension Driver: Equatable {
    static func == (lhs: Driver, rhs: Driver) -> Bool {
        return lhs.id == rhs.id &&
            lhs.driverTypeId == rhs.driverTypeId &&
            lhs.companyName == rhs.companyName &&
            lhs.citizenId == rhs.citizenId &&
            lhs.driverLicenseId == rhs.driverLicenseId &&
            lhs.driverLicenseType == rhs.driverLicenseType &&
            lhs.createdAt == rhs.createdAt &&
            lhs.readableDriverId == rhs.readableDriverId &&
            lhs.truck == rhs.truck
    }
}
