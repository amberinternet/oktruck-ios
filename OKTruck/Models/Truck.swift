//
//  Truck.swift
//  OKTruck
//
//  Created by amberhello on 8/1/2563 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Truck {
    
    let id : Int!
    let truckTypeId : Int!
    let licensePlate : String!
    let truckProvince : String!
    let truckYear : String!
    let truckModel : String!
    let truckBrand : String!
    let latitude : String!
    let longitude : String!
    let truckType : TruckType!
    
    init(json : JSON){
        id = json["id"].intValue
        truckTypeId = json["car_type_id"].intValue
        licensePlate = json["license_plate"].stringValue
        truckProvince = json["car_province"].stringValue
        truckYear = json["car_year"].stringValue
        truckModel = json["car_model"].stringValue
        truckBrand = json["car_brand"].stringValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        truckType = TruckType(json: json["car_type"])
    }
    
}

extension Truck: Equatable {
    static func == (lhs: Truck, rhs: Truck) -> Bool {
        return lhs.id == rhs.id &&
            lhs.truckTypeId == rhs.truckTypeId &&
            lhs.licensePlate == rhs.licensePlate &&
            lhs.truckProvince == rhs.truckProvince &&
            lhs.truckYear == rhs.truckYear &&
            lhs.truckModel == rhs.truckModel &&
            lhs.truckBrand == rhs.truckBrand &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude &&
            lhs.truckType == rhs.truckType
    }
}
