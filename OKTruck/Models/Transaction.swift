//
//  Transaction.swift
//  OKTruck
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Transaction {
    
    let id : Int!
    let type : Int!
    let description : String!
    let oldCreditBalance : String!
    let newCreditBalance : String!
    let credit : Double!
    let createdAt : String!
    let work : Work!
    let deposit : Deposit!
    let withdraw : Withdraw!
    
    init(json : JSON) {
        id = json["id"].intValue
        type = json["credit_log_type"].intValue
        description = json["description"].stringValue
        oldCreditBalance = json["old_credit_balance"].stringValue
        newCreditBalance = json["new_credit_balance"].stringValue
        credit = json["credit"].doubleValue
        createdAt = json["created_at"].stringValue
        work = Work(json: json["work"])
        deposit = Deposit(json: json["topup"])
        withdraw = Withdraw(json: json["withdraw"])
    }
    
    func getReadableType() -> NSAttributedString {
        var attributedString: NSMutableAttributedString
        
        switch(type) {
            
        case TransactionType.DEPOSIT.value:
            let string = NSLocalizedString("Transaction:DEPOSIT", comment: "")
            let range = (string as NSString).range(of: string)
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttributes([.font : UIFont.boldSystemFont(ofSize: 16)], range: range)
            
        case TransactionType.CREATE_WORK.value:
            let string = "\(NSLocalizedString("Transaction:CREATE_WORK", comment: "")) \(NSLocalizedString("Transaction:number", comment: "")) \(work.getReadableWorkId())"
            let range = (string as NSString).range(of: NSLocalizedString("Transaction:CREATE_WORK", comment: ""))
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.CANCEL_WORK.value, TransactionType.CANCEL_WORK_OMISE.value:
            let string = "\(NSLocalizedString("Transaction:CANCEL_WORK", comment: "")) \(NSLocalizedString("Transaction:number", comment: "")) \(work.getReadableWorkId())"
            let range = (string as NSString).range(of: NSLocalizedString("Transaction:CANCEL_WORK", comment: ""))
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.ADMIN_ADD.value:
            let string = NSLocalizedString("Transaction:ADMIN_ADD", comment: "")
            let range = (string as NSString).range(of: NSLocalizedString("Transaction:AddWallet", comment: ""))
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.ADMIN_DELETE.value:
            let string = NSLocalizedString("Transaction:ADMIN_DELETE", comment: "")
            let range = (string as NSString).range(of: NSLocalizedString("Transaction:DeductWallet", comment: ""))
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.WITHDRAW.value:
            let string = "\(NSLocalizedString("Transaction:WITHDRAW", comment: "")) \(NSLocalizedString("Transaction:number", comment: "")) \(work.getReadableWorkId())"
            let range = (string as NSString).range(of: NSLocalizedString("Transaction:WITHDRAW", comment: ""))
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        default: attributedString = NSMutableAttributedString(string: Constants.UNKNOWN)
        }
        
        return attributedString
    }
    
    func getCredit() -> String { return credit.roundDecimalFormat() }
    
    func getReadableCredit() -> String {
        let credit = getCredit()
        switch(type) {
        case TransactionType.DEPOSIT.value, TransactionType.CANCEL_WORK.value, TransactionType.CANCEL_WORK_OMISE.value, TransactionType.ADMIN_ADD.value: return "+ ฿\(credit)"
        case TransactionType.CREATE_WORK.value, TransactionType.ADMIN_DELETE.value, TransactionType.WITHDRAW.value: return "- ฿\(credit)"
        default: return "฿\(credit)"
        }
    }
    
    func getCreditTextColor() -> UIColor {
        switch(type) {
        case TransactionType.DEPOSIT.value, TransactionType.CANCEL_WORK.value, TransactionType.CANCEL_WORK_OMISE.value, TransactionType.ADMIN_ADD.value: return UIColor(named: "Deposit Color")!
        case TransactionType.CREATE_WORK.value, TransactionType.ADMIN_DELETE.value, TransactionType.WITHDRAW.value: return UIColor(named: "Withdraw Color")!
        default: return UIColor(named: "Text Color")!
        }
    }
}

extension Transaction: Equatable {
    static func == (lhs: Transaction, rhs: Transaction) -> Bool {
        return lhs.id == rhs.id &&
            lhs.type == rhs.type &&
            lhs.description == rhs.description &&
            lhs.oldCreditBalance == rhs.oldCreditBalance &&
            lhs.newCreditBalance == rhs.newCreditBalance &&
            lhs.credit == rhs.credit &&
            lhs.createdAt == rhs.createdAt &&
            lhs.work == rhs.work &&
            lhs.deposit == rhs.deposit &&
            lhs.withdraw == rhs.withdraw
    }
}
