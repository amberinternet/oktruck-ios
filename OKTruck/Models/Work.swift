//
//  Work.swift
//  OKTruck
//
//  Created by amberhello on 5/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Work {
    
    var id: Int = Constants.INITIAL_INT
    var commission: Int = Constants.INITIAL_INT
    var distance: Double = Constants.INITIAL_DOUBLE
    var transportationFee: String = Constants.INITIAL_STRING
    var fullFee: String = Constants.INITIAL_STRING
    var status: Int = Constants.INITIAL_INT
    var paymentMethod: Int = Constants.INITIAL_INT
    var userID: Int = Constants.INITIAL_INT
    var userName: String = Constants.INITIAL_STRING
    var userTelephoneNumber: String = Constants.INITIAL_STRING
    var note: String = Constants.INITIAL_STRING
    var driverPaid: Int = Constants.INITIAL_INT
    var createdAt: String = Constants.INITIAL_STRING
    var formattedWorkId: String = Constants.INITIAL_STRING
    var commissionFee: String = Constants.INITIAL_STRING
    var summaryFee: String = Constants.INITIAL_STRING
    var driverName: String = Constants.INITIAL_STRING
    var driverTelephoneNumber: String = Constants.INITIAL_STRING
    var licensePlate: String = Constants.INITIAL_STRING
    var truckProvince: String = Constants.INITIAL_STRING
    var item: Item = Item()
    var truckType: TruckType = TruckType()
    var additionalServices: [AdditionalService] = []
    var source: Address = Address()
    var destination: Address = Address()
    var postId: Int?
    var backhaulDiscount: Double = Constants.INITIAL_DOUBLE
    var promotionCode: String?
    
    init() {}
    
    init(json : JSON){
        id = json["id"].intValue
        commission = json["commission"].intValue
        distance = json["distance"].doubleValue
        transportationFee = json["distance_price"].stringValue
        fullFee = json["full_price"].stringValue
        status = json["status"].intValue
        paymentMethod = json["payment_method"].intValue
        userName = json["user_name"].stringValue
        userTelephoneNumber = json["user_telephone_number"].stringValue
        note = json["user_note"].stringValue
        driverPaid = json["is_driver_paid"].intValue
        formattedWorkId = json["formatted_work_id"].stringValue
        commissionFee = json["commission_price"].stringValue
        summaryFee = json["summary_price"].stringValue
        driverName = "\(json["driver"]["user"]["first_name"].stringValue) \(json["driver"]["user"]["last_name"].stringValue)"
        driverTelephoneNumber = json["driver"]["user"]["telephone_number"].stringValue
        licensePlate = json["driver"]["car"]["license_plate"].stringValue
        truckProvince = json["driver"]["car"]["car_province"].stringValue
        item = Item(json: json["item"])
        truckType = TruckType(json: json["car_type"])
        if let jsonArray = json["additional_services"].array {
            for jsonObject in jsonArray {
                let additionalService = AdditionalService(json: jsonObject)
                additionalServices.append(additionalService)
            }
        }
        source = Address(source: json["source"])
        destination = Address(destination: json["destination"])
    }
    
    init(omiseResponse json : JSON) {
        userID = json["user_id"].intValue
        truckType.id = json["car_type_id"].intValue
        distance = json["distance"].doubleValue
        transportationFee = json["distance_price"].stringValue
        fullFee = json["full_price"].stringValue
        paymentMethod = json["payment_method"].intValue
        status = json["status"].intValue
        userName = json["user_name"].stringValue
        userTelephoneNumber = json["user_telephone_number"].stringValue
        note = json["user_note"].stringValue
        createdAt = json["created_at"].stringValue
        id = json["id"].intValue
        formattedWorkId = json["formatted_work_id"].stringValue
        commissionFee = json["commission_price"].stringValue
        summaryFee = json["summary_price"].stringValue
    }
    
    func getReadableWorkId() -> String {
//        return "#" + formattedWorkId
        return formattedWorkId
    }
    
    func getReadableDistance() -> String {
        let km = distance / 1000.0
        return "\(km.roundDecimalFormat()) \(NSLocalizedString("Application:KM", comment: ""))"
    }
    
    func getReadablePaymentMethod() -> String {
        switch(paymentMethod) {
        case PaymentMethod.CASH_SOURCE.status: return PaymentMethod.CASH_SOURCE.pay
        case PaymentMethod.CASH_DESTINATION.status: return PaymentMethod.CASH_DESTINATION.pay
        case PaymentMethod.WALLET.status: return PaymentMethod.WALLET.pay
        case PaymentMethod.OMISE.status: return PaymentMethod.OMISE.pay
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadableReceiveMethod() -> String {
        switch(paymentMethod) {
        case PaymentMethod.CASH_SOURCE.status: return PaymentMethod.CASH_SOURCE.receive
        case PaymentMethod.CASH_DESTINATION.status: return PaymentMethod.CASH_DESTINATION.receive
        case PaymentMethod.WALLET.status, PaymentMethod.OMISE.status: return PaymentMethod.WALLET.receive
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadableWorkStatus() -> String {
        switch(status) {
        case WorkStatus.PAYMENT_FAIL.status: return WorkStatus.PAYMENT_FAIL.readableStatus
        case WorkStatus.FIND_TRUCK.status:
            if postId == nil { return WorkStatus.FIND_TRUCK.readableStatus }
            else { return WorkStatus.BOOK_BACKHAUL.readableStatus }
        case WorkStatus.FIND_TRUCK.status: return WorkStatus.FIND_TRUCK.readableStatus
        case WorkStatus.WAITING_CONFIRM_WORK.status: return WorkStatus.WAITING_CONFIRM_WORK.readableStatus
        case WorkStatus.WAITING_RECEIVE_ITEM.status: return WorkStatus.WAITING_RECEIVE_ITEM.readableStatus
        case WorkStatus.ARRIVED_SOURCE.status: return WorkStatus.ARRIVED_SOURCE.readableStatus
        case WorkStatus.WAITING_SENT_ITEM.status: return WorkStatus.WAITING_SENT_ITEM.readableStatus
        case WorkStatus.ARRIVED_DESTINATION.status: return WorkStatus.ARRIVED_DESTINATION.readableStatus
        case WorkStatus.COMPLETE.status: return WorkStatus.COMPLETE.readableStatus
        case WorkStatus.CANCEL.status: return WorkStatus.CANCEL.readableStatus
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadablePrice() -> String {
        return "฿" + Double(summaryFee)!.roundDecimalFormat()
    }
    
}

extension Work: Equatable {
    static func == (lhs: Work, rhs: Work) -> Bool {
        return lhs.id == rhs.id &&
            lhs.distance == rhs.distance &&
            lhs.commission == rhs.commission &&
            lhs.transportationFee == rhs.transportationFee &&
            lhs.summaryFee == rhs.summaryFee &&
            lhs.status == rhs.status &&
            lhs.paymentMethod == rhs.paymentMethod &&
            lhs.userName == rhs.userName &&
            lhs.userTelephoneNumber == rhs.userTelephoneNumber &&
            lhs.note == rhs.note &&
            lhs.driverPaid == rhs.driverPaid &&
            lhs.formattedWorkId == rhs.formattedWorkId &&
            lhs.commissionFee == rhs.commissionFee &&
            lhs.item == rhs.item &&
            lhs.truckType == rhs.truckType &&
            lhs.additionalServices == rhs.additionalServices &&
            lhs.source == rhs.source &&
            lhs.destination == rhs.destination
    }
}
