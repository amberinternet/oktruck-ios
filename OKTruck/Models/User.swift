//
//  User.swift
//  OKTrucker
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct User {
    
    var id : Int = Constants.INITIAL_INT
    var firstName : String = Constants.INITIAL_STRING
    var lastName : String = Constants.INITIAL_STRING
    var email : String = Constants.INITIAL_STRING
    var profileImageURI : String = Constants.INITIAL_STRING
    var address : String = Constants.INITIAL_STRING
    var telephoneNumber : String = Constants.INITIAL_STRING
    var creditBalance : Double = Constants.INITIAL_DOUBLE
    var isVerify : Int = Constants.INITIAL_INT
    var createdAt : String = Constants.INITIAL_STRING
    var readableUserId : String = Constants.INITIAL_STRING
    var company = Company()
    
    var password : String = Constants.INITIAL_STRING
    var confirmPassword : String = Constants.INITIAL_STRING
    
    private init() {}
    
    init(json : JSON) {
        id = json["id"].intValue
        firstName = json["first_name"].stringValue
        lastName = json["last_name"].stringValue
        email = json["email"].stringValue
        profileImageURI = json["profile_image"].stringValue
        address = json["current_address"].stringValue
        telephoneNumber = json["telephone_number"].stringValue
        creditBalance = json["credit_balance"].doubleValue
        isVerify = json["is_verified"].intValue
        createdAt = json["created_at"].stringValue
        readableUserId = json["formatted_user_id"].stringValue
        company = Company(json: json["company"])
    }
    
    static var shared: User = User()
    
    mutating func set(user: User) {
        id = user.id
        firstName = user.firstName
        lastName = user.lastName
        email = user.email
        profileImageURI = user.profileImageURI
        address = user.address
        telephoneNumber = user.telephoneNumber
        creditBalance = user.creditBalance
        isVerify = user.isVerify
        createdAt = user.createdAt
        readableUserId = user.readableUserId
        company = user.company
    }
    
    mutating func clear() {
        id = Constants.INITIAL_INT
        firstName = Constants.INITIAL_STRING
        lastName = Constants.INITIAL_STRING
        email = Constants.INITIAL_STRING
        profileImageURI = Constants.INITIAL_STRING
        address = Constants.INITIAL_STRING
        telephoneNumber = Constants.INITIAL_STRING
        creditBalance = Constants.INITIAL_DOUBLE
        isVerify = Constants.INITIAL_INT
        createdAt = Constants.INITIAL_STRING
        readableUserId = Constants.INITIAL_STRING
        company = Company()
        
        password = Constants.INITIAL_STRING
        confirmPassword = Constants.INITIAL_STRING
    }
    
    func getFullName() -> String {
        return firstName + " " + lastName
    }
    
    func getCredit() -> String {
        return creditBalance.roundDecimalFormat()
    }
    
    func getReadableCredit() -> String {
        let credit = getCredit()
        return "฿\(credit)"
    }
    
    func getFullProfileImageURI() -> String {
        return Constants.BASE_URL + profileImageURI
    }
    
}
