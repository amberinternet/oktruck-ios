//
//  Company.swift
//  OKTruck
//
//  Created by amberhello on 19/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Company {
    
    var id : Int = Constants.INITIAL_INT
    var name : String = Constants.INITIAL_STRING
    var telephoneNumber : String = Constants.INITIAL_STRING
    var taxId : String = Constants.INITIAL_STRING
    var address : String = Constants.INITIAL_STRING
    
    init() {}
    
    init(json : JSON) {
        id = json["id"].intValue
        name = json["company_name"].stringValue
        telephoneNumber = json["company_telephone_number"].stringValue
        taxId = json["company_tax_id"].stringValue
        address = json["company_address"].stringValue
    }
    
}
