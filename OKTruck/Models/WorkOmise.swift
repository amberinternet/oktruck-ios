//
//  WorkOmise.swift
//  OKTruck
//
//  Created by amberhello on 7/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct WorkOmise {
    
    let work : Work!
    let authorizeURI : String!
    
    init(json : JSON) {
        work = Work(omiseResponse: json["work"])
        authorizeURI = json["authorize_uri"].stringValue
    }
    
}
