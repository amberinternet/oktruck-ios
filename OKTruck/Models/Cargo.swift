//
//  Item.swift
//  OKTruck
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Item {
    
    var id : Int = Constants.INITIAL_INT
    var name : String = Constants.INITIAL_STRING
    var quantity : String = Constants.INITIAL_STRING
    var weight : String = Constants.INITIAL_STRING
    var imageFileName : String = Constants.INITIAL_STRING
    
    var image: UIImage = UIImage()
    
    init() {}
    
    init(json : JSON) {
        id = json["id"].intValue
        name = json["item_name"].stringValue
        quantity = json["item_quantity"].stringValue
        weight = json["item_weight"].stringValue
        imageFileName = json["item_image"].stringValue
    }
    
    func getCargoImagePath() -> String {
        return "image/item/" + imageFileName
    }
    
}

extension Item: Equatable {
    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.quantity == rhs.quantity &&
            lhs.weight == rhs.weight &&
            lhs.image == rhs.image
    }
}
