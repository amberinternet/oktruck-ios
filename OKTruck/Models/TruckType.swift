//
//  TruckType.swift
//  OKTruck
//
//  Created by amberhello on 22/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation
import SwiftyJSON

struct TruckType {
    
    var id : Int = Constants.INITIAL_INT
    var name : String = Constants.INITIAL_STRING
    var imageURI : String = Constants.INITIAL_STRING
    var maximumLoad : String = Constants.INITIAL_STRING
    var priceFirst3km : Double = Constants.INITIAL_DOUBLE
    var price3To15km : Double = Constants.INITIAL_DOUBLE
    var price15To100km : Double = Constants.INITIAL_DOUBLE
    var price100UpKm : Double = Constants.INITIAL_DOUBLE
    var additionalServices : [AdditionalService]! = []
    
    var image: UIImage { return UIImage(named: "Truck Type \(id) Image")! }   // TODO: get truck image
    
    init() {}
    
    init(json : JSON){
        id = json["id"].intValue
        name = json["car_type_name"].stringValue
        imageURI = json["car_type_image"].stringValue
        maximumLoad = json["car_type_description"].stringValue
        priceFirst3km = json["price_for_first_3_km"].doubleValue
        price3To15km = json["price_per_km_at_3_15"].doubleValue
        price15To100km = json["price_per_km_at_15_100"].doubleValue
        price100UpKm = json["price_per_km_at_100up"].doubleValue
        if let jsonArray = json["additional_services"].array {
            for jsonObject in jsonArray {
                let additionalService = AdditionalService(json: jsonObject)
                additionalServices.append(additionalService)
            }
        }
    }
    
    func getImagePath() -> String {
        return Constants.PRODUCTION_URL + "img/truck/" + imageURI
    }
    
}

extension TruckType: Equatable {
    static func == (lhs: TruckType, rhs: TruckType) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.imageURI == rhs.imageURI &&
            lhs.maximumLoad == rhs.maximumLoad &&
            lhs.priceFirst3km == rhs.priceFirst3km &&
            lhs.price3To15km == rhs.price3To15km &&
            lhs.price15To100km == rhs.price15To100km &&
            lhs.price100UpKm == rhs.price100UpKm
    }
}
