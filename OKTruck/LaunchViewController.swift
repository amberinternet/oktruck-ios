//
//  SplashViewController.swift
//  OK Truck
//
//  Created by amberhello on 18/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SplashViewController: BaseViewController {
    
    @IBOutlet weak var loadingIndicator: IndicatorImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // refresh token
        
        let token = keychain.get("token")
        
        if token != nil {
            loadingIndicator.startAnimating()
            
            let headers = [
                "Authorization": "Bearer " + token!
                ] as HTTPHeaders
            
            Alamofire.request(APIService().REFRESH_TOKEN, method: .post, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { response in
                    if let value = response.result.value {
                        let json = JSON(value)
                        let token = json["data"]["token"].stringValue
                        
                        if !token.isEmpty {
                            self.keychain.set(token, forKey: "token")
                            
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let nextViewController = storyBoard.instantiateInitialViewController()!
                            self.present(nextViewController, animated:true, completion:nil)
                            
                        } else {
                            self.keychain.delete("token")
                            
                            self.performSegue(withIdentifier: "Log In Segue", sender: self)
                            
                            let error = json["error"].stringValue
                            print(error)
                        }
                        
                        self.loadingIndicator.stopAnimating()
                        
                    } else if let error = response.result.error {
                        // show alert dialog
                        let massage = NSLocalizedString("เข้าสู่ระบบไม่สำเร็จ กรุณาตรวจสอบการเชื่อมต่ออินเตอร์เน็ต", comment: "")
                        let actionTitle = NSLocalizedString("ตกลง", comment: "")
                        
                        let alertController = UIAlertController(title: nil, message: massage, preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: actionTitle, style: .default, handler: { action in
                            exit(0)
                        })
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true)
                        
                        print(error)
                    }
            }
        } else {
            performSegue(withIdentifier: "Log In Segue", sender: self)
        }
    }
}
