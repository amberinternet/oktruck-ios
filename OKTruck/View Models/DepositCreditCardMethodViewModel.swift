//
//  DepositCreditCardMethodViewModel.swift
//  OKTruck
//
//  Created by amberhello on 13/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class DepositCreditCardMethodViewModel {
    
    private var viewController: DepositCreditCardMethodViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    var depositOmise: DepositOmise! {
        didSet {
            viewController.showAuthorizingPaymentForm(authorizeURI: depositOmise.authorizeURI)
        }
    }
    
    init(_ viewControllor: DepositCreditCardMethodViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func requestDepositWithOmise(price: Int, token: String) -> Disposable? {
        return RxNetwork<DepositOmiseResponse>().request(userService.depositWithOmise(depositType: DepositType.OMISE.value, price: price, omiseToken: token, fromApp: 1), onSuccess: { (response) in
            self.depositOmise = response.depositOmise
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:SendingRequest", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func completePaymentOmise() -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.completePaymentOmise(depositId: depositOmise.deposit.id), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("Successful:Payment", comment: ""), completion: {
                self.viewController.performSegue(withIdentifier: "CreateWorkCreditCardMethodUnwindSegue", sender: nil)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("CreditCardMethodViewModel:LoadingMessage", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}

