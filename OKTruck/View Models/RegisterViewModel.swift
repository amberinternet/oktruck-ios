//
//  RegisterViewModel.swift
//  OKTruck
//
//  Created by amberhello on 19/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class RegisterViewModel {
    
    private var viewController: RegisterViewController!
    private var dialogManager: DialogManager!
    private var authService: AuthService!
    private var user: User!
    
    init(_ viewControllor: RegisterViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        authService = AuthService.default
    }
    
    func signUp() -> Disposable? {
        user = User.shared
        return RxNetwork<SuccessResponse>().request(authService.signUp(
            firstName: user.firstName,
            lastName: user.lastName,
            telephoneNumber: user.telephoneNumber,
            email: user.email,
            password: user.password,
            confirmPassword: user.confirmPassword,
            companyName: user.company.name
            ), onSuccess: { (_) in
                self.viewController.disposable = self.requestOTP()
                self.viewController.performSegue(withIdentifier: "OTPSegue", sender: nil)
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("RegisterViewModel:LoadingMessage", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func requestOTP() -> Disposable? {
        user = User.shared
        return RxNetwork<SuccessResponse>().request(authService.requestOTP(email: user.email, telephoneNumber: user.telephoneNumber))
    }
    
}
