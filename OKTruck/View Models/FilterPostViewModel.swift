//
//  FilterPostViewModel.swift
//  OKTruck
//
//  Created by amberhello on 20/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class FilterPostViewModel {
    
    private var viewController: FilterPostViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var truckTypes: [TruckType]! = []
    
    init(_ viewControllor: FilterPostViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getTruckType() -> Disposable? {
        return RxNetwork<TruckTypesResponse>().request(userService.getTruckTypes(), onSuccess: { (response) in
            self.truckTypes = Array(response.truckTypes[2...])
            self.truckTypes.reverse()
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            self.viewController.dismiss(animated: true, completion: nil)
        })
    }
    
}
