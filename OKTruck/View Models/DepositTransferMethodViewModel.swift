//
//  DepositTransferMethodViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 3/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class DepositTransferMethodViewModel {
    
    private var viewController: DepositTransferMethodViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    init(_ viewControllor: DepositTransferMethodViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func requestTransferMethod(depositType: Int, price: Int, payTime: String, paySlipData: Data) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.depositWithTransferSlip(depositType: depositType, price: price, payTime: payTime, paySlipData: paySlipData), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("Successful:PaymentRequest", comment: ""), completion: {
                self.viewController.performSegue(withIdentifier: "TransferMethodUnwindSegue", sender: nil)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:SendingRequest", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
