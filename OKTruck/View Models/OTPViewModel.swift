//
//  OTPViewModel.swift
//  OKTruck
//
//  Created by amberhello on 20/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class OTPViewModel {
    
    private var viewController: OTPViewController!
    private var dialogManager: DialogManager!
    private var authService: AuthService!
    private var user: User!
    
    init(_ viewControllor: OTPViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        authService = AuthService.default
    }
    
    func submit(otp: String) -> Disposable? {
        user = User.shared
        return RxNetwork<SuccessResponse>().request(authService.submitOTP(telephoneNumber: user.telephoneNumber, otp: otp), onSuccess: { (_) in
            self.viewController.performSegue(withIdentifier: "Registered Segue", sender: nil)
            User.shared.clear()
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:CheckingData", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func requestOTP() -> Disposable? {
        user = User.shared
        return RxNetwork<SuccessResponse>().request(authService.requestOTP(email: user.email, telephoneNumber: user.telephoneNumber), onSuccess: { (_) in
            self.dialogManager.showToast(message: NSLocalizedString("OTPViewModel:SentOTP", comment: ""))
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        })
    }
    
}
