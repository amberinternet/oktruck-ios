//
//  PostViewModel.swift
//  OKTruck
//
//  Created by amberhello on 19/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class PostViewModel {
    
    private var viewController: PostViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var postList: [Post]! = []
    
    var postPagination: PostPagination! {
        didSet {
            if postPagination != oldValue && postPagination != nil {
                viewController.collectionView.reloadData()
                print("Loaded page: \(postPagination.currentPage!)")
            }
        }
    }
    
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true {
                viewController.refreshControl.beginRefreshing()
                postPagination = nil
            } else {
                viewController.refreshControl.endRefreshing()
            }
        }
    }
    
    init(_ viewControllor: PostViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getPostList(page: Int = 1 , sourceProvince: String? = "", destinationProvince: String? = "", receivedDate: String? = "", truckTypeId: String? = "") -> Disposable? {
        return RxNetwork<PostListResponse>().request(userService.getPost(page: page, sourceProvince!, destinationProvince!, receivedDate!, truckTypeId!), onSuccess: { (response) in
            if (self.isRefresh) {
                self.postList.removeAll()
                self.isRefresh = false
            }
            
            self.postList.append(contentsOf: response.postPagination.postList)
            self.postPagination = response.postPagination
            
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
}
