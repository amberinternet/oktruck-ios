//
//  AddressModalViewModel.swift
//  OKTruck
//
//  Created by Thanathip Suppapantita on 20/3/20.
//  Copyright © 2020 Amber Internet. All rights reserved.
//

import RxSwift

class AddressModalViewModel {
    
    private var viewController: AddressModalViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var addressHistoryList: [Address]! = []
        
    var addressHistoryPagination: AddressHistoryPagination! {
        didSet {
            if addressHistoryPagination != oldValue && addressHistoryPagination != nil {
                viewController.tableView.reloadData()
                print("Loaded page: \(addressHistoryPagination.currentPage!)")
            }
        }
    }
        
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true {
                viewController.refreshControl.beginRefreshing()
                addressHistoryPagination = nil
            } else {
                viewController.refreshControl.endRefreshing()
            }
        }
    }
        
    init(_ viewControllor: AddressModalViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
        
    func getSourceHistoryList(page: Int = 1) -> Disposable? {
        return RxNetwork<AddressHistoryListResponse>().request(userService.getSourceHistory(page: page), onSuccess: { (response) in
            if (self.isRefresh) {
                self.addressHistoryList.removeAll()
                self.isRefresh = false
            }
            
            self.addressHistoryList.append(contentsOf: response.addressHistoryPagination.addressHistoryList)
            self.addressHistoryPagination = response.addressHistoryPagination
            
        }, onFailure: { (error) in
//            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
    func getDestinationHistoryList(page: Int = 1) -> Disposable? {
        return RxNetwork<AddressHistoryListResponse>().request(userService.getDestinationHistory(page: page), onSuccess: { (response) in
            if (self.isRefresh) {
                self.addressHistoryList.removeAll()
                self.isRefresh = false
            }
            
            self.addressHistoryList.append(contentsOf: response.addressHistoryPagination.addressHistoryList)
            self.addressHistoryPagination = response.addressHistoryPagination
            
        }, onFailure: { (error) in
//            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
        
}
