//
//  CreateWorkSummaryViewModel.swift
//  OKTruck
//
//  Created by amberhello on 6/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class CreateWorkSummaryViewModel {
    
    private var viewController: CreateWorkSummaryViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    init(_ viewControllor: CreateWorkSummaryViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getProfile() -> Disposable? {
        return RxNetwork<UserResponse>().request(userService.getProfile(), onSuccess: { (response) in
            User.shared.set(user: response.user)
            print(User.shared.getCredit())
//            self.viewController.walletMethodView.creditLabel.text = User.shared.getCredit()
        }, onFailure: { (_) in })
    }
    
    func getDistance(sourceLatitude: Double, sourceLongitude: Double, destinationLatitude: Double, destinationLongitude: Double) -> Disposable? {
        return RxNetwork<DistanceResponse>().request(userService.getDistance(sourceLatitude: sourceLatitude, sourceLongitude: sourceLongitude, destinationLatitude: destinationLatitude, destinationLongitude: destinationLongitude), onSuccess: { (response) in
            self.viewController.work.distance = response.distance
            self.viewController.distanceLabel.text = self.viewController.work.getReadableDistance()
        }, onFailure: { (_) in })
    }
    
    func checkPromotion(code: String) -> Disposable? {
        let animateDuration: TimeInterval = 0.3
        
        return RxNetwork<PromotionResponse>().request(userService.checkPromotion(code: code), onSuccess: { (response) in
            self.viewController.work.promotionCode = code
            
            self.viewController.summaryFeeStackView.fullFeeLabel.text = self.viewController.work.getReadablePrice()
            let readableTotalFee = "฿ \((Double(self.viewController.work.summaryFee)! - response.discount).roundDecimalFormat())"
            self.viewController.summaryFeeStackView.totalFeeLabel.text = readableTotalFee
            self.viewController.summaryFeeStackView.showDiscount()
            
            self.viewController.footerView.totalFeeLabel.text = readableTotalFee
            
            self.viewController.promotionView.usableStackView.isHidden = false
            UIView.animate(withDuration: animateDuration, animations: {
                self.viewController.promotionView.usableStackView.subviews[0].isHidden = false
            })
        }, onFailure: { (_) in
            self.viewController.clearPromotionCode()
            
            self.viewController.promotionView.usableStackView.isHidden = false
            UIView.animate(withDuration: animateDuration, animations: {
                self.viewController.promotionView.usableStackView.subviews[1].isHidden = false
            })
        })
    }
    
    func createWorkWithWallet(_ work: Work) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.createWorkWithWallet(work: work), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("Successful:CreateWork", comment: "")) {
                self.viewController.performSegue(withIdentifier: "CreateWorkUnwindSegue", sender: nil)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:SendingData", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func createWorkWithCash(_ work: Work, paymentMethod: Int) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.createWorkWithCash(work: work, paymentMethod: paymentMethod), onSuccess: { (_) in
                self.dialogManager.showSuccess(message: NSLocalizedString("Successful:CreateWork", comment: "")) {
                    self.viewController.performSegue(withIdentifier: "CreateWorkUnwindSegue", sender: nil)
                }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:SendingData", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
