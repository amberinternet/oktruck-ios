//
//  CreateWorkCreditCardMethodViewModel.swift
//  OKTruck
//
//  Created by amberhello on 4/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class CreateWorkCreditCardMethodViewModel {
    
    private var viewController: CreateWorkCreditCardMethodViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    var workOmise: WorkOmise! {
        didSet {
            viewController.showAuthorizingPaymentForm(authorizeURI: workOmise.authorizeURI)
        }
    }
    
    init(_ viewControllor: CreateWorkCreditCardMethodViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func createWorkWithOmise(_ work: Work, token: String) -> Disposable? {
        return RxNetwork<WorkOmiseResponse>().request(userService.createWorkWithOmise(work: work, omiseToken: token), onSuccess: { (response) in
            self.workOmise = response.workOmise
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:SendingRequest", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func completePaymentOmise() -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.completePaymentOmise(workId: workOmise.work.id), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("Successful:Payment", comment: ""), completion: {
                self.viewController.performSegue(withIdentifier: "CreateWorkCreditCardMethodUnwindSegue", sender: nil)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("CreditCardMethodViewModel:LoadingMessage", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
