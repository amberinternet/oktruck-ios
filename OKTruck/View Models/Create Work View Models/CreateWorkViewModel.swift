//
//  CreateWorkViewModel.swift
//  OKTruck
//
//  Created by amberhello on 23/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class CreateWorkViewModel {
    
    private var viewController: CreateWorkViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var truckTypes: [TruckType]! = []
    var totalFee: Double!
    
    
    init(_ viewControllor: CreateWorkViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getTruckType() -> Disposable? {
        return RxNetwork<TruckTypesResponse>().request(userService.getTruckTypes(), onSuccess: { (response) in
            self.truckTypes = Array(response.truckTypes[2...])
            self.truckTypes.reverse()
            self.viewController.truckTypeView.collectionView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.viewController.collectionViewSelectCenterItem()
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            self.viewController.dismiss(animated: true, completion: nil)
        })
    }
    
    private let animateDuration: TimeInterval = 0.5
    
    func calculateFee(truckTypeId: Int, sourceLatitude: Double, sourceLongitude: Double, destinationLatitude: Double, destinationLongitude: Double, additionalServices: [AdditionalService], postId: Int? = nil) -> Disposable? {
        return RxNetwork<CalculateFeeResponse>().request(userService.calculateFee(
            truckTypeId: truckTypeId,
            sourceLatitude: sourceLatitude,
            sourceLongitude: sourceLongitude,
            destinationLatitude: destinationLatitude,
            destinationLongitude: destinationLongitude,
            additionalServices: additionalServices,
            postId: postId
        ), onSuccess: { (response) in
            self.totalFee = response.summaryFee
            self.viewController.footerView.totalFeeLabel.text = "฿" + Double(self.totalFee).roundDecimalFormat()
            
            if self.viewController.footerViewBottomConstraint.constant == -self.viewController.footerView.frame.height {
                self.viewController.footerViewBottomConstraint.constant = 0
                UIView.animate(withDuration: self.animateDuration, animations: {
                    self.viewController.footerView.frame.origin.y = self.viewController.footerView.frame.origin.y - self.viewController.footerView.frame.height
                }, completion: { (_) in
                    self.viewController.contentStackViewBottomConstraint.constant = self.viewController.footerView.frame.height
                })
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            
            if self.viewController.footerViewBottomConstraint.constant == 0 {
                self.viewController.contentStackViewBottomConstraint.constant = 0
                self.viewController.footerViewBottomConstraint.constant = -self.viewController.footerView.frame.height
                UIView.animate(withDuration: self.animateDuration, animations: {
                    self.viewController.footerView.frame.origin.y = self.viewController.footerView.frame.origin.y + self.viewController.footerView.frame.height
                })
            }
        })
    }
    
}
