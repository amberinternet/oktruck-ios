//
//  MyCompleteWorkViewModel.swift
//  OKTruck
//
//  Created by amberhello on 19/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class MyCompleteWorkViewModel {
    
    private var viewController: MyCompleteWorkViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var workList: [Work]! = []

    var workPagination: WorkPagination! {
        didSet {
            if workPagination != oldValue && workPagination != nil {
                viewController.collectionView.reloadData()
                print("Loaded page: \(workPagination.currentPage!)")
            }
        }
    }

    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true {
                viewController.refreshControl.beginRefreshing()
                workPagination = nil
            } else {
                viewController.refreshControl.endRefreshing()
            }
        }
    }

    init(_ viewControllor: MyCompleteWorkViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }

    func getMyCompleteWorkList(page: Int = 1) -> Disposable? {
        return RxNetwork<WorkListResponse>().request(userService.getMyWork(page: page, status: WorkQuery.STATUS_COMPLETED.value, search: "", sort: WorkQuery.SORT_DESC.value), onSuccess: { (response) in
            if (self.isRefresh) {
                self.workList.removeAll()
                self.isRefresh = false
            }

            self.workList.append(contentsOf: response.workPagination.workList)
            self.workPagination = response.workPagination

        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
}
