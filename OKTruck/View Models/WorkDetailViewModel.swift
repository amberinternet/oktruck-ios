//
//  WorkDetailViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 12/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift
import Alamofire

class WorkDetailViewModel {
    
    private var viewController: WorkDetailViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var work: Work!
    
    var status: Int! {
        didSet {
            if status != oldValue {
                viewController.initView()
            }
        }
    }
    
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true { viewController.refreshControl.beginRefreshing() }
            else { viewController.refreshControl.endRefreshing() }
            
            if (work != viewController.temporaryWork) {
                viewController.temporaryWork = work
                viewController.initView()
                print("The work has changed.")
            }
        }
    }
    
    init(_ viewControllor: WorkDetailViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getWorkDetail() -> Disposable? {
        return RxNetwork<WorkResponse>().request(userService.getWorkDetail(workId: work.id), onSuccess: { (response) in
            print("Loaded work: \(self.work.getReadableWorkId())")
            if (self.isRefresh) {
                self.work = response.work
                self.isRefresh = false
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
    func cancelWork() -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.cancelWork(workId: work.id), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("Successful:CancelWork", comment: ""), completion: {
                self.viewController.navigationController?.popViewController(animated: true)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:SendingData", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }

}
