//
//  CreateWorkPaymantMethodViewModel.swift
//  OKTruck
//
//  Created by amberhello on 6/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class CreateWorkPaymantMethodViewModel {
    
    private var viewController: CreateWorkPaymentMethodViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    init(_ viewControllor: CreateWorkPaymentMethodViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getProfile() -> Disposable? {
        return RxNetwork<UserResponse>().request(userService.getProfile(), onSuccess: { (response) in
            User.shared.set(user: response.user)
            print(User.shared.getCredit())
            self.viewController.walletMethodView.creditLabel.text = User.shared.getCredit()
        }, onFailure: { (_) in })
    }
    
    func createWorkWithWallet(_ work: Work) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.createWorkWithWallet(work: work), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("Successful:CreateWork", comment: "")) {
                self.viewController.performSegue(withIdentifier: "CreateWorkUnwindSegue", sender: nil)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("Network:SendingData", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
