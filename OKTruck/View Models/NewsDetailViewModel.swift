//
//  NewsDetailViewModel.swift
//  OKTruck
//
//  Created by amberhello on 17/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class NewsDetailViewModel {
    
    private var viewController: NewsDetailViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    var news: News! {
        didSet {
            viewController.initView()
        }
    }
    
    init(_ viewControllor: NewsDetailViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getNewsDetail(newsId: Int) -> Disposable? {
        return RxNetwork<NewsResponse>().request(userService.getNewsDetail(newsId: newsId), onSuccess: { (response) in
            self.news = response.news
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            self.viewController.navigationController?.popViewController(animated: true)
        }, onLoading: {
            self.dialogManager.showLoading()
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
