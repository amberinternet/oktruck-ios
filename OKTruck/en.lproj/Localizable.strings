/* 
  Localizable.strings
  OKTruck

  Created by amberhello on 14/2/2562 BE.
  Copyright © 2562 Amber Internet. All rights reserved.
*/

"Application:Version" = "Version";
"Application:Back" = "Back";
"Application:OK" = "OK";
"Application:Dismiss" = "Dismiss";
"Application:Close" = "Close";
"Application:Alert" = "Alert";
"Application:Success" = "Success";
"Application:Settings" = "Settings";
"Application:PhotoLibrary" = "Photo Library";
"Application:PhotoLibraryAccessDenied:title" = "Photo library Access is Denied";
"Application:PhotoLibraryAccessDenied:message" = "Please enable photo library access in settings";
"Application:TakePhoto" = "Take Photo";
"Application:CameraAccessDenied:title" = "Camera Access is Denied";
"Application:CameraAccessDenied:message" = "Please enable camera access in settings";
"Application:LocationServicesDisabled:title" = "Location Services are Disabled";
"Application:LocationServicesDisabled:message" = "Please enable enabled location services in settings";
"Application:LocationAccessDenied:title" = "Location Access is Denied";
"Application:LocationAccessDenied:message" = "Please enable location access in settings";
"Application:ContactsAccessDenied:title" = "Contacts Access is Denied";
"Application:ContactsAccessDenied:message" = "Please enable contacts access in settings";
"Application:TermsAndConditionsOfService" = "Terms and Conditions of Service";
"Application:PrivacyPolicy" = "Privacy Policy";
"Application:Work" = "Work";
"Application:Source" = "Source";
"Application:Destination" = "Destination";
"Application:Contact" = "Contact";
"Application:Cargo" = "Cargo";
"Application:Quantity" = "Quantity";
"Application:Weight" = "Weight";
"Application:PickUp" = "Pick Up";
"Application:DropOff" = "Drop Off";
"Application:SourceAddress" = "Source Address";
"Application:DestinationAddress" = "Destination Address";
"Application:CargoName" = "Cargo name";
"Application:CargoQuantity" = "Quantity of Cargo";
"Application:CargoWeight" = "Gross Weight of Cargo";
"Application:PickUpDateTime" = "Pick Up Date and Time";
"Application:DropOffDateTime" = "Drop Off Date and Time";
"Application:PickUpDate" = "Pick Up Date";
"Application:DropOffDate" = "Drop Off Date";
"Application:Today" = "Today";
"Application:Tomorrow" = "Tomorrow";
"Application:Everyday" = "Everyday";
"Application:Anytime" = "Anytime";
"Application:InProcess" = "In Process";
"Application:Completed" = "Completed";
"Application:O'Clock" = "";
"Application:Landmark" = "Landmark";
"Application:KM" = "kilometers";
"Application:AllTypes" = "All Types";
"Application:BackhaulDiscount" = "Backhaul discount";
"Application:CreateWork" = "Create Work";
"Application:CreateBackhaulWork" = "Create Backhaul Work";
"Application:Note" = "Note";
"Application:Name" = "Name";
"Application:TelephoneNumber" = "Telephone Number";
"Application:PlaceName" = "Company / Store Name";
"Application:Landmark" = "Landmark";
"Application:Contact" = "Contact";

// Network

"Network:NoInternetConnection" = "No internet connection";
"Network:ServerFailure" = "Server failure";
"Network:SendingData" = "Sending data...";
"Network:CheckingData" = "Checking data...";
"Network:SendingRequest" = "Sending request...";

// alert

"Alert:UserNotVerify" = "You can access the application after the admin confirms the information.";
"Alert:LogOut:title" = "Confirm log out";
"Alert:LogOut:message" = "Do you want to log out?";
"Alert:CreateWork:title" = "Confirm create work";
"Alert:CreateWork:message" = "Do you want to create work?";
"Alert:CancelWork:title" = "Confirm cancel work";
"Alert:CancelWork:message" = "Do you want to cancel work?";
"Alert:ContactPicker:NoTelephoneNumber:message" = "The selected contact does not have a telephone number.";
"Alert:ContactPicker:SelectTelephoneNumber:title" = "Select telephone number";

// error

"Error:FoundError" = "Found an error";
"Error:EmailPasswordIncorrect" = "Email or password is incorrect";
"Error:PleaseLoginAgain" = "Please login again.";
"Error:AccountNotVerify" = "Your account has not been verified.\nPlease wait for the admin to contact you to confirm your identity.";
"Error:AccountSuspended" = "Your account has been suspended.";
"Error:CannotSendOTP" = "Cannot send OTP to this phone number.";
"Error:InvalidOTP" = "Invalid OTP";
"Error:OTPExpired" = "The OTP code has expired. Please request the code again.";
"Error:PaymentFailed" = "Payment failed. Please contact the admin.";
"Error:CreditNotEnough" = "Your credit is not enough.\nPlease top up.";
"Error:NewsNotFound" = "News not found";
"Error:WorkCannotCancel" = "This work cannot cancel";
"Error:DuplicateLocation" = "The pick up and drop off locations are duplicated.";

// Successful

"Successful:Payment" = "Successful payment";
"Successful:CreateWork" = "Create a work successfully";
"Successful:CancelWork" = "Cancel a work successfully";
"Successful:PaymentRequest" = "Payment request successfully sent";

// work status

"WorkStatus:PAYMENT_FAIL" = "Card Payment Fail";
"WorkStatus:FIND_TRUCK" = "Find Truck";
"WorkStatus:BOOK_BACKHAUL" = "Book a Backhaul";
"WorkStatus:WAITING_CONFIRM_WORK" = "Waiting for Confirmation";
"WorkStatus:WAITING_RECEIVE_ITEM" = "Waiting for Pick Up";
"WorkStatus:ARRIVED_SOURCE" = "Arrived at Source";
"WorkStatus:WAITING_SENT_ITEM" = "Shipping Now";
"WorkStatus:ARRIVED_DESTINATION" = "Arrived at Destination";
"WorkStatus:COMPLETE" = "Completed";
"WorkStatus:CANCEL" = "Canceled";

// Payment Method

"PaymentMethod:CASH_SOURCE:pay" = "Pay by cash at source";
"PaymentMethod:CASH_DESTINATION:pay" = "Pay by cash at destination";
"PaymentMethod:WALLET:pay" = "Pay by Wallet";
"PaymentMethod:OMISE:pay" = "Pay by card";
"PaymentMethod:CASH_SOURCE:receive" = "Cash work (pay at source)";
"PaymentMethod:CASH_DESTINATION:receive" = "Cash work (pay at destination)";
"PaymentMethod:WALLET:receive" = "Credit work";
"PaymentMethod:OMISE:receive" = "Credit work";

// Transaction

"Transaction:DEPOSIT" = "Top up";
"Transaction:CREATE_WORK" = "Create work";
"Transaction:CANCEL_WORK" = "Cancel work";
"Transaction:ADMIN_ADD" = "Add Wallet by addmin";
"Transaction:ADMIN_DELETE" = "Deduct Wallet by admin";
"Transaction:Withdraw" = "Withdraw";
"Transaction:number" = "no.";
"Transaction:AddWallet" = "Add Wallet";
"Transaction:DeductWallet" = "Deduct Wallet";


// Transaction Request Status

"TransactionRequestStatus:WAITING:deposit" = "Waiting";
"TransactionRequestStatus:APPROVED:deposit" = "Approved";
"TransactionRequestStatus:REJECT:deposit" = "Reject";
"TransactionRequestStatus:WAITING:withdraw" = "Waiting";
"TransactionRequestStatus:APPROVED:withdraw" = "Approved";
"TransactionRequestStatus:REJECT:withdraw" = "Reject";

// Deposit Type

"DepositType:OMISE" = "Credit/Debit Card";
"DepositType:TRANSFER" = "Transfer Money";

// login

"LoginViewController:AcceptMessage" = "By clicking on the log in button or the user registration button means you agree to Terms and Conditions of Service and Privacy Policy of the OKTruck Application";
"LoginViewController:LogingInMessage" = "Loging in...";

// Validate

"Validate:FirstName" = "Please enter first name";
"Validate:LastName" = "Please enter last name";
"Validate:Email" = "Please enter email";
"Validate:EmailInvalid" = "Invalid email";
"Validate:TelephoneNumber" = "Please enter telephone number";
"Validate:TelephoneNumberInvalid" = "Please enter the 10-digit phone number";
"Validate:Password" = "Please enter password";
"Validate:PasswordConfirmation" = "Please confirm password";
"Validate:PasswordNotMatch" = "The password confirmation does not match";
"Validate:OTP" = "Please enter the 4-digit OTP you received";
"Validate:Location" = "This location is outside the service area.";
"Validate:PlaceName" = "Please enter company/store name";
"Validate:ContactName" = "Please enter contact name";
"Validate:ContactTelephoneNumber" = "Please enter contact telephone number";
"Validate:TruckType" = "Please select truck type";
"Validate:SourceAddress" = "Please select source address";
"Validate:DestinationAddress" = "Please select destination address";
"Validate:CargoName" = "Please enter cargo name";
"Validate:Quantity" = "Please enter quantity";
"Validate:Weight" = "Please enter weight";
"Validate:CargoImage" = "Please select cargo image";
"Validate:PickUpDate" = "Please select pick up date";
"Validate:DropOffDate" = "Please select drop off date";
"Validate:UserName" = "Please enter your name";
"Validate:UserTelephoneNumber" = "Please enter your telephone number";
"Validate:PaymentMethod" = "Please select payment method";
"Validate:CardNumber" = "Please enter card number";
"Validate:CardNumberInvalid" = "Card number is invalid";
"Validate:ExpirationDate" = "Please enter card expiration date";
"Validate:ExpirationDateInvalid" = "Card expiration date is invalid";
"Validate:CVV" = "Please enter CVV";
"Validate:CVVInvalid" = "CVV is invalid";
"Validate:CardholderName" = "Please enter cardholder name";
"Validate:AttachedTransferSlip" = "Please find attached proof of payment.";

// register

"RegisterViewModel:FirstName" = "First Name*";
"RegisterViewModel:LastName" = "Last Name*";
"RegisterViewModel:Email" = "Email*";
"RegisterViewModel:TelephoneNumber" = "Telephone Number*";
"RegisterViewModel:Company" = "Company / Store Name";
"RegisterViewModel:Password" = "Password*";
"RegisterViewModel:PasswordConfirmation" = "Confirm Password*";
"RegisterViewModel:LoadingMessage" = "Applying for membership...";

"OTPViewController:sendOTPLabel:message1" = "The system has sent a message to the number ";
"OTPViewController:sendOTPLabel:message2" = ". Please enter the four-digit OTP code you received.";
"OTPViewModel:SentOTP" = "Sent OTP";

// Payment Method

"PaymentMethodModalViewController:creditLabel:message" = "Balance: ";

// Credit Card Method

"CreditCardMethodViewModel:LoadingMessage" = "Checking payment...";
