//
//  CustomAnimatedTextInputStyle.swift
//  OKTruck
//
//  Created by amberhello on 17/2/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import AnimatedTextInput

struct CustomAnimatedTextInputStyle: AnimatedTextInputStyle {
    var activeColor: UIColor = UIColor(named: "Primary Color")!
    var placeholderInactiveColor: UIColor = UIColor(named: "Primary Color")!
    var inactiveColor: UIColor {
        if #available(iOS 13.0, *) { return UIColor.placeholderText
        } else { return UIColor.placeholder }
    }
    var lineInactiveColor: UIColor = UIColor.clear
    var lineActiveColor: UIColor = UIColor.clear
    var lineHeight: CGFloat = 0
    var errorColor: UIColor = UIColor.red
    var textInputFont: UIFont = UIFont.systemFont(ofSize: 16)
    var textInputFontColor: UIColor = UIColor(named: "Text Color")!
    var placeholderMinFontSize: CGFloat = 12
    var counterLabelFont: UIFont?
    var topMargin: CGFloat = 0
    var bottomMargin: CGFloat = -15
    var leftMargin: CGFloat = 0
    var rightMargin: CGFloat = 0
    var yHintPositionOffset: CGFloat = 0
    var yPlaceholderPositionOffset: CGFloat = 15
    var textAttributes: [String : Any]?
}
