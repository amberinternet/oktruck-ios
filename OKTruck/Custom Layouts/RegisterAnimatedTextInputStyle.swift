//
//  RegisterAnimatedTextInputStyle.swift
//  OKTruck
//
//  Created by amberhello on 13/3/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import AnimatedTextInput

struct RegisterAnimatedTextInputStyle: AnimatedTextInputStyle {
    var activeColor: UIColor = UIColor.white
    var placeholderInactiveColor: UIColor = UIColor.white
    var inactiveColor: UIColor = UIColor.lightGray
    var lineInactiveColor: UIColor = UIColor.clear
    var lineActiveColor: UIColor = UIColor.clear
    var lineHeight: CGFloat = 0
    var errorColor: UIColor = UIColor.red
    var textInputFont: UIFont = UIFont.systemFont(ofSize: 16)
    var textInputFontColor: UIColor = UIColor.white
    var placeholderMinFontSize: CGFloat = 12
    var counterLabelFont: UIFont?
    var topMargin: CGFloat = 0
    var bottomMargin: CGFloat = -15
    var leftMargin: CGFloat = 0
    var rightMargin: CGFloat = 0
    var yHintPositionOffset: CGFloat = 0
    var yPlaceholderPositionOffset: CGFloat = 15
    var textAttributes: [String : Any]?
}
