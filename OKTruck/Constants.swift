//
//  Constant.swift
//  OKTruck
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

struct Constants {
    /* init value */
    static let INITIAL_STRING = ""
    static let INITIAL_INT = 0
    static let INITIAL_DOUBLE = 0.0
    static let UNKNOWN = "ไม่ระบุ"
    
    static let BATH_SIGN = "฿"
    static let INITIAL_YEAR = 2018
    
    /* network */
    static let TOKEN_TYPE = "Bearer "
    static let TOKEN_KEY = "token_key"
    
    enum HttpHeaderField: String {
        case authentication = "Authorization"
    }
    
    enum ContentType: String {
        case json = "application/json"
    }
    
    /* api */
    static let API_PATH = "api/v1/"
    
    static let TEST_URL = "http://192.168.1.5:8000/"
    static let TEST_API_URL = TEST_URL + API_PATH
    
    static let PRODUCTION_URL = "https://backoffice.oktruck.com/"
    static let PRODUCTION_API_URL = PRODUCTION_URL + API_PATH
    
    static let BASE_URL = PRODUCTION_URL
    static let BASE_API_URL = PRODUCTION_API_URL
    
    static let GOOGLE_API_KEY = "AIzaSyBsU3T_6lZnm0fU8mHaeZpLNj8HyfKudi4"

    /* user manager */
    static let IS_ENABLE_NOTIFICATION_KEY = "is_enable_notification_key"
    static let ADDRESS_HISTORY_KEY = "address_history_key"
    
    /* contact */
    static let ADDMIN_PHONE_NUMBER = "02-077-7498"
    
}
