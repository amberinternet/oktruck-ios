//
//  UserType.swift
//  OKTruck
//
//  Created by amberhello on 8/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

enum UserType {
    
    case USER
    
    var type: String {
        switch self {
        case .USER:
            return "user"
        }
    }
    
}
