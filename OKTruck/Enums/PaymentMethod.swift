//
//  PaymentMethod.swift
//  OKTruck
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation

enum PaymentMethod {
    
    case CASH_SOURCE
    case CASH_DESTINATION
    case WALLET
    case OMISE
    
    var status: Int {
        switch self {
        case .CASH_SOURCE:
            return 1
        case .CASH_DESTINATION:
            return 2
        case .WALLET:
            return 3
        case .OMISE:
            return 4
        }
    }
    
    var pay: String {
        switch self {
        case .CASH_SOURCE:
            return NSLocalizedString("PaymentMethod:CASH_SOURCE:pay", comment: "")
        case .CASH_DESTINATION:
            return NSLocalizedString("PaymentMethod:CASH_DESTINATION:pay", comment: "")
        case .WALLET:
            return NSLocalizedString("PaymentMethod:WALLET:pay", comment: "")
        case .OMISE:
            return NSLocalizedString("PaymentMethod:OMISE:pay", comment: "")
        }
    }
    
    var receive: String {
        switch self {
        case .CASH_SOURCE:
            return NSLocalizedString("PaymentMethod:CASH_SOURCE:receive", comment: "")
        case .CASH_DESTINATION:
            return NSLocalizedString("PaymentMethod:CASH_DESTINATION:receive", comment: "")
        case .WALLET:
            return NSLocalizedString("PaymentMethod:WALLET:receive", comment: "")
        case .OMISE:
            return NSLocalizedString("PaymentMethod:OMISE:receive", comment: "")
        }
    }
    
}

