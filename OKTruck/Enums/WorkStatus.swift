//
//  WorkStatus.swift
//  OKTrucker
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

enum WorkStatus {
    
    case PAYMENT_FAIL
    case FIND_TRUCK
    case BOOK_BACKHAUL
    case WAITING_CONFIRM_WORK
    case WAITING_RECEIVE_ITEM
    case ARRIVED_SOURCE
    case WAITING_SENT_ITEM
    case ARRIVED_DESTINATION
    case COMPLETE
    case CANCEL
    
    var status: Int {
        switch self {
        case .PAYMENT_FAIL:
            return 0
        case .FIND_TRUCK, .BOOK_BACKHAUL:
            return 1
        case .WAITING_CONFIRM_WORK:
            return 2
        case .WAITING_RECEIVE_ITEM:
            return 3
        case .ARRIVED_SOURCE:
            return 4
        case .WAITING_SENT_ITEM:
            return 5
        case .ARRIVED_DESTINATION:
            return 6
        case .COMPLETE:
            return 7
        case .CANCEL:
            return 8
        }
    }
    
    var readableStatus: String {
        switch self {
        case .PAYMENT_FAIL:
            return NSLocalizedString("WorkStatus:PAYMENT_FAIL", comment: "")
        case .FIND_TRUCK:
            return NSLocalizedString("WorkStatus:FIND_TRUCK", comment: "")
        case .BOOK_BACKHAUL:
            return NSLocalizedString("WorkStatus:BOOK_BACKHAUL", comment: "")
        case .WAITING_CONFIRM_WORK:
            return NSLocalizedString("WorkStatus:WAITING_CONFIRM_WORK", comment: "")
        case .WAITING_RECEIVE_ITEM:
            return NSLocalizedString("WorkStatus:WAITING_RECEIVE_ITEM", comment: "")
        case .ARRIVED_SOURCE:
            return NSLocalizedString("WorkStatus:ARRIVED_SOURCE", comment: "")
        case .WAITING_SENT_ITEM:
            return NSLocalizedString("WorkStatus:WAITING_SENT_ITEM", comment: "")
        case .ARRIVED_DESTINATION:
            return NSLocalizedString("WorkStatus:ARRIVED_DESTINATION", comment: "")
        case .COMPLETE:
            return NSLocalizedString("WorkStatus:COMPLETE", comment: "")
        case .CANCEL:
            return NSLocalizedString("WorkStatus:CANCEL", comment: "")
        }
    }
    
    var StatusColor: UIColor {
        switch self {
        case .FIND_TRUCK:
            return UIColor(named: "Find Truck Color")!
        case .BOOK_BACKHAUL:
            return UIColor(named: "Book Backhaul Color")!
        case .WAITING_CONFIRM_WORK:
            return UIColor(named: "Waiting Confirm Color")!
        case .WAITING_RECEIVE_ITEM:
            return UIColor(named: "Waiting Receive Item Color")!
        case .ARRIVED_SOURCE:
            return UIColor(named: "Arrived Source Color")!
        case .WAITING_SENT_ITEM:
            return UIColor(named: "Waiting Send Item Color")!
        case .ARRIVED_DESTINATION:
            return UIColor(named: "Arrived Destination Color")!
        case .COMPLETE:
            return UIColor(named: "Finish Color")!
        case .CANCEL:
            return UIColor(named: "Cancel Color")!
        default:
            return UIColor.clear
        }
    }
    
}

