//
//  DepositType.swift
//  OKTruck
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation

enum DepositType {
    
    case OMISE
    case TRANSFER
    
    var value: Int {
        switch self {
        case .OMISE:
            return 1
        case .TRANSFER:
            return 2
        }
    }
    
    var method: String {
        switch self {
        case .OMISE:
            return NSLocalizedString("DepositType:OMISE", comment: "")
        case .TRANSFER:
            return NSLocalizedString("DepositType:TRANSFER", comment: "")
        }
    }
    
}
