//
//  TransactionType.swift
//  OKTruck
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

enum TransactionType {
    
    case DEPOSIT
    case CREATE_WORK
    case CANCEL_WORK
    case ADMIN_ADD
    case ADMIN_DELETE
    case WITHDRAW
    case FINISH
    case ACCEPT_WORK
    case CANCEL_WORK_OMISE
    case ADD_FROM_CANCEL_WORK
    case DEDUCT_FROM_CANCEL_WORK
    
    var value: Int {
        switch self {
        case .DEPOSIT:
            return 1
        case .CREATE_WORK:
            return 2
        case .CANCEL_WORK:
            return 3
        case .ADMIN_ADD:
            return 4
        case .ADMIN_DELETE:
            return 5
        case .WITHDRAW:
            return 6
        case .FINISH:
            return 7
        case .ACCEPT_WORK:
            return 8
        case .CANCEL_WORK_OMISE:
            return 9
        case .ADD_FROM_CANCEL_WORK:
            return 10
        case .DEDUCT_FROM_CANCEL_WORK:
            return 11
        }
    }
    
}
