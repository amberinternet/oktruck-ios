//
//  WorkQuery.swift
//  OKTrucker
//
//  Created by amberhello on 10/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

enum WorkQuery {
    
    case STATUS_IN_PROGRESS
    case STATUS_COMPLETED
    case SORT_ASC
    case SORT_DESC
    
    var value: String {
        switch self {
        case .STATUS_IN_PROGRESS:
            return "in-progress"
        case .STATUS_COMPLETED:
            return "completed"
        case .SORT_ASC:
            return "asc"
        case .SORT_DESC:
            return "desc"
        }
        
    }
    
}
