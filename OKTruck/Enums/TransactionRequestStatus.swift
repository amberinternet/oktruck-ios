//
//  TransactionRequestStatus.swift
//  OKTrucker
//
//  Created by amberhello on 25/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation

enum TransactionRequestStatus {
    
    case WAITING
    case APPROVED
    case REJECT
    
    var status: Int {
        switch self {
        case .WAITING:
            return 0
        case .APPROVED:
            return 1
        case .REJECT:
            return 2
        }
    }
    
    var depositMessage: String {
        switch self {
        case .WAITING:
            return NSLocalizedString("TransactionRequestStatus:WAITING:deposit", comment: "")
        case .APPROVED:
            return NSLocalizedString("TransactionRequestStatus:APPROVED:deposit", comment: "")
        case .REJECT:
            return NSLocalizedString("TransactionRequestStatus:REJECT:deposit", comment: "")
        }
    }
    
    var withdrawMessage: String {
        switch self {
        case .WAITING:
            return NSLocalizedString("TransactionRequestStatus:WAITING:withdraw", comment: "")
        case .APPROVED:
            return NSLocalizedString("TransactionRequestStatus:APPROVED:withdraw", comment: "")
        case .REJECT:
            return NSLocalizedString("TransactionRequestStatus:REJECT:withdraw", comment: "")
        }
    }
}
