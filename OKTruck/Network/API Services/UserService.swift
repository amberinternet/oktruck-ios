//
//  UserService.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire
import SwiftyJSON
import RxSwift

class UserService {
    
    private let networkManager = NetworkManager.default()
    
    private init() {}
    
    static let `default` = UserService()
    
    func refreshToken() -> Observable<AccessTokenResponse> {
        return Observable<AccessTokenResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.refreshToken).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = AccessTokenResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func destroyToken(firebaseToken: String) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.destroyToken(firebaseToken)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getProfile() -> Observable<UserResponse> {
        return Observable<UserResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getProfile).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = UserResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getTruckTypes() -> Observable<TruckTypesResponse> {
        return Observable<TruckTypesResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getTruckTypes).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = TruckTypesResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getDistance(sourceLatitude: Double, sourceLongitude: Double, destinationLatitude: Double, destinationLongitude: Double) -> Observable<DistanceResponse> {
        return Observable<DistanceResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getDistance(sourceLatitude, sourceLongitude, destinationLatitude, destinationLongitude)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = DistanceResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func calculateFee(truckTypeId: Int, sourceLatitude: Double, sourceLongitude: Double, destinationLatitude: Double, destinationLongitude: Double, additionalServices: [AdditionalService], postId: Int?) -> Observable<CalculateFeeResponse> {
        return Observable<CalculateFeeResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("\(truckTypeId)".data(using: String.Encoding.utf8)!, withName: "car_type_id")
                multipartFormData.append("\(sourceLatitude)".data(using: String.Encoding.utf8)!, withName: "source_latitude")
                multipartFormData.append("\(sourceLongitude)".data(using: String.Encoding.utf8)!, withName: "source_longitude")
                multipartFormData.append("\(destinationLatitude)".data(using: String.Encoding.utf8)!, withName: "destination_latitude")
                multipartFormData.append("\(destinationLongitude)".data(using: String.Encoding.utf8)!, withName: "destination_longitude")
                for additionalService in additionalServices {
                    multipartFormData.append("1".data(using: String.Encoding.utf8)!, withName: "has_service_\(additionalService.id!)")
                }
                if let postId = postId {
                    multipartFormData.append("\(postId)".data(using: String.Encoding.utf8)!, withName: "post_id")
                }
            }, with: UserAPIRouter.calculateFee) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = CalculateFeeResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func createWorkWithWallet(work: Work) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("\(work.truckType.id)".data(using: String.Encoding.utf8)!, withName: "car_type_id")
                multipartFormData.append("\(PaymentMethod.WALLET.status)".data(using: String.Encoding.utf8)!, withName: "payment_method")
                multipartFormData.append("\(work.userName)".data(using: String.Encoding.utf8)!, withName: "user_name")
                multipartFormData.append("\(work.userTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "user_telephone_number")
                multipartFormData.append("\(work.note)".data(using: String.Encoding.utf8)!, withName: "user_note")
                multipartFormData.append("\(work.item.name)".data(using: String.Encoding.utf8)!, withName: "item_name")
                multipartFormData.append("\(work.item.quantity)".data(using: String.Encoding.utf8)!, withName: "item_quantity")
                multipartFormData.append("\(work.item.weight)".data(using: String.Encoding.utf8)!, withName: "item_weight")
                multipartFormData.append(work.item.getImageData()!, withName: "item_image", fileName: "item_image.jpeg", mimeType: "image/jpeg")
                multipartFormData.append("\(work.source.latitude)".data(using: String.Encoding.utf8)!, withName: "source_latitude")
                multipartFormData.append("\(work.source.longitude)".data(using: String.Encoding.utf8)!, withName: "source_longitude")
                multipartFormData.append("\(work.source.fullAddress)".data(using: String.Encoding.utf8)!, withName: "source_full_address")
                multipartFormData.append("\(work.source.subdistrict)".data(using: String.Encoding.utf8)!, withName: "source_address_subdistrict")
                multipartFormData.append("\(work.source.district)".data(using: String.Encoding.utf8)!, withName: "source_address_district")
                multipartFormData.append("\(work.source.province)".data(using: String.Encoding.utf8)!, withName: "source_address_province")
                multipartFormData.append("\(work.source.contactName)".data(using: String.Encoding.utf8)!, withName: "source_contact_name")
                multipartFormData.append("\(work.source.contactTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "source_contact_telephone_number")
                multipartFormData.append("\(work.source.name)".data(using: String.Encoding.utf8)!, withName: "source_shop_name")
                multipartFormData.append("\(work.source.landmark)".data(using: String.Encoding.utf8)!, withName: "source_landmark")
                multipartFormData.append("\(work.source.date)".data(using: String.Encoding.utf8)!, withName: "received_date")
                multipartFormData.append("\(work.source.time)".data(using: String.Encoding.utf8)!, withName: "received_time")
                multipartFormData.append("\(work.destination.latitude)".data(using: String.Encoding.utf8)!, withName: "destination_latitude")
                multipartFormData.append("\(work.destination.longitude)".data(using: String.Encoding.utf8)!, withName: "destination_longitude")
                multipartFormData.append("\(work.destination.fullAddress)".data(using: String.Encoding.utf8)!, withName: "destination_full_address")
                multipartFormData.append("\(work.destination.subdistrict)".data(using: String.Encoding.utf8)!, withName: "destination_address_subdistrict")
                multipartFormData.append("\(work.destination.district)".data(using: String.Encoding.utf8)!, withName: "destination_address_district")
                multipartFormData.append("\(work.destination.province)".data(using: String.Encoding.utf8)!, withName: "destination_address_province")
                multipartFormData.append("\(work.destination.contactName)".data(using: String.Encoding.utf8)!, withName: "destination_contact_name")
                multipartFormData.append("\(work.destination.contactTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "destination_contact_telephone_number")
                multipartFormData.append("\(work.destination.name)".data(using: String.Encoding.utf8)!, withName: "destination_shop_name")
                multipartFormData.append("\(work.destination.landmark)".data(using: String.Encoding.utf8)!, withName: "destination_landmark")
                multipartFormData.append("\(work.destination.date)".data(using: String.Encoding.utf8)!, withName: "delivered_date")
                multipartFormData.append("\(work.destination.time)".data(using: String.Encoding.utf8)!, withName: "delivered_time")
                for additionalService in work.additionalServices {
                    multipartFormData.append("1".data(using: String.Encoding.utf8)!, withName: "has_service_\(additionalService.id!)")
                }
                if let postId = work.postId {
                    multipartFormData.append("\(postId)".data(using: String.Encoding.utf8)!, withName: "post_id")
                }
                if let promotionCode = work.promotionCode {
                    multipartFormData.append("\(promotionCode)".data(using: String.Encoding.utf8)!, withName: "promotion_code")
                }
            }, with: UserAPIRouter.createWork) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = SuccessResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                            print(JSON(value))
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func createWorkWithOmise(work: Work, omiseToken: String) -> Observable<WorkOmiseResponse> {
        return Observable<WorkOmiseResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("\(work.truckType.id)".data(using: String.Encoding.utf8)!, withName: "car_type_id")
                multipartFormData.append("\(PaymentMethod.OMISE.status)".data(using: String.Encoding.utf8)!, withName: "payment_method")
                multipartFormData.append("\(omiseToken)".data(using: String.Encoding.utf8)!, withName: "omise_token")
                multipartFormData.append("\(Constants.BASE_URL)".data(using: String.Encoding.utf8)!, withName: "redirect_uri")
                multipartFormData.append("\(work.userName)".data(using: String.Encoding.utf8)!, withName: "user_name")
                multipartFormData.append("\(work.userTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "user_telephone_number")
                multipartFormData.append("\(work.note)".data(using: String.Encoding.utf8)!, withName: "user_note")
                multipartFormData.append("\(work.item.name)".data(using: String.Encoding.utf8)!, withName: "item_name")
                multipartFormData.append("\(work.item.quantity)".data(using: String.Encoding.utf8)!, withName: "item_quantity")
                multipartFormData.append("\(work.item.weight)".data(using: String.Encoding.utf8)!, withName: "item_weight")
                multipartFormData.append(work.item.getImageData()!, withName: "item_image", fileName: "item_image.jpeg", mimeType: "image/jpeg")
                multipartFormData.append("\(work.source.latitude)".data(using: String.Encoding.utf8)!, withName: "source_latitude")
                multipartFormData.append("\(work.source.longitude)".data(using: String.Encoding.utf8)!, withName: "source_longitude")
                multipartFormData.append("\(work.source.fullAddress)".data(using: String.Encoding.utf8)!, withName: "source_full_address")
                multipartFormData.append("\(work.source.subdistrict)".data(using: String.Encoding.utf8)!, withName: "source_address_subdistrict")
                multipartFormData.append("\(work.source.district)".data(using: String.Encoding.utf8)!, withName: "source_address_district")
                multipartFormData.append("\(work.source.province)".data(using: String.Encoding.utf8)!, withName: "source_address_province")
                multipartFormData.append("\(work.source.contactName)".data(using: String.Encoding.utf8)!, withName: "source_contact_name")
                multipartFormData.append("\(work.source.contactTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "source_contact_telephone_number")
                multipartFormData.append("\(work.source.name)".data(using: String.Encoding.utf8)!, withName: "source_shop_name")
                multipartFormData.append("\(work.source.landmark)".data(using: String.Encoding.utf8)!, withName: "source_landmark")
                multipartFormData.append("\(work.source.date)".data(using: String.Encoding.utf8)!, withName: "received_date")
                multipartFormData.append("\(work.source.time)".data(using: String.Encoding.utf8)!, withName: "received_time")
                multipartFormData.append("\(work.destination.latitude)".data(using: String.Encoding.utf8)!, withName: "destination_latitude")
                multipartFormData.append("\(work.destination.longitude)".data(using: String.Encoding.utf8)!, withName: "destination_longitude")
                multipartFormData.append("\(work.destination.fullAddress)".data(using: String.Encoding.utf8)!, withName: "destination_full_address")
                multipartFormData.append("\(work.destination.subdistrict)".data(using: String.Encoding.utf8)!, withName: "destination_address_subdistrict")
                multipartFormData.append("\(work.destination.district)".data(using: String.Encoding.utf8)!, withName: "destination_address_district")
                multipartFormData.append("\(work.destination.province)".data(using: String.Encoding.utf8)!, withName: "destination_address_province")
                multipartFormData.append("\(work.destination.contactName)".data(using: String.Encoding.utf8)!, withName: "destination_contact_name")
                multipartFormData.append("\(work.destination.contactTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "destination_contact_telephone_number")
                multipartFormData.append("\(work.destination.name)".data(using: String.Encoding.utf8)!, withName: "destination_shop_name")
                multipartFormData.append("\(work.destination.landmark)".data(using: String.Encoding.utf8)!, withName: "destination_landmark")
                multipartFormData.append("\(work.destination.date)".data(using: String.Encoding.utf8)!, withName: "delivered_date")
                multipartFormData.append("\(work.destination.time)".data(using: String.Encoding.utf8)!, withName: "delivered_time")
                for additionalService in work.additionalServices {
                    multipartFormData.append("1".data(using: String.Encoding.utf8)!, withName: "has_service_\(additionalService.id!)")
                }
                if let postId = work.postId {
                    multipartFormData.append("\(postId)".data(using: String.Encoding.utf8)!, withName: "post_id")
                }
                if let promotionCode = work.promotionCode {
                    multipartFormData.append("\(promotionCode)".data(using: String.Encoding.utf8)!, withName: "promotion_code")
                }
            }, with: UserAPIRouter.createWork) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = WorkOmiseResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                            print(JSON(value))
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func completePaymentOmise(workId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.createWorkCompletePaymentOmise(workId: workId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func createWorkWithCash(work: Work, paymentMethod: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("\(work.truckType.id)".data(using: String.Encoding.utf8)!, withName: "car_type_id")
                multipartFormData.append("\(paymentMethod)".data(using: String.Encoding.utf8)!, withName: "payment_method")
                multipartFormData.append("\(work.userName)".data(using: String.Encoding.utf8)!, withName: "user_name")
                multipartFormData.append("\(work.userTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "user_telephone_number")
                multipartFormData.append("\(work.note)".data(using: String.Encoding.utf8)!, withName: "user_note")
                multipartFormData.append("\(work.item.name)".data(using: String.Encoding.utf8)!, withName: "item_name")
                multipartFormData.append("\(work.item.quantity)".data(using: String.Encoding.utf8)!, withName: "item_quantity")
                multipartFormData.append("\(work.item.weight)".data(using: String.Encoding.utf8)!, withName: "item_weight")
                multipartFormData.append(work.item.getImageData()!, withName: "item_image", fileName: "item_image.jpeg", mimeType: "image/jpeg")
                multipartFormData.append("\(work.source.latitude)".data(using: String.Encoding.utf8)!, withName: "source_latitude")
                multipartFormData.append("\(work.source.longitude)".data(using: String.Encoding.utf8)!, withName: "source_longitude")
                multipartFormData.append("\(work.source.fullAddress)".data(using: String.Encoding.utf8)!, withName: "source_full_address")
                multipartFormData.append("\(work.source.subdistrict)".data(using: String.Encoding.utf8)!, withName: "source_address_subdistrict")
                multipartFormData.append("\(work.source.district)".data(using: String.Encoding.utf8)!, withName: "source_address_district")
                multipartFormData.append("\(work.source.province)".data(using: String.Encoding.utf8)!, withName: "source_address_province")
                multipartFormData.append("\(work.source.contactName)".data(using: String.Encoding.utf8)!, withName: "source_contact_name")
                multipartFormData.append("\(work.source.contactTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "source_contact_telephone_number")
                multipartFormData.append("\(work.source.name)".data(using: String.Encoding.utf8)!, withName: "source_shop_name")
                multipartFormData.append("\(work.source.landmark)".data(using: String.Encoding.utf8)!, withName: "source_landmark")
                multipartFormData.append("\(work.source.date)".data(using: String.Encoding.utf8)!, withName: "received_date")
                multipartFormData.append("\(work.source.time)".data(using: String.Encoding.utf8)!, withName: "received_time")
                multipartFormData.append("\(work.destination.latitude)".data(using: String.Encoding.utf8)!, withName: "destination_latitude")
                multipartFormData.append("\(work.destination.longitude)".data(using: String.Encoding.utf8)!, withName: "destination_longitude")
                multipartFormData.append("\(work.destination.fullAddress)".data(using: String.Encoding.utf8)!, withName: "destination_full_address")
                multipartFormData.append("\(work.destination.subdistrict)".data(using: String.Encoding.utf8)!, withName: "destination_address_subdistrict")
                multipartFormData.append("\(work.destination.district)".data(using: String.Encoding.utf8)!, withName: "destination_address_district")
                multipartFormData.append("\(work.destination.province)".data(using: String.Encoding.utf8)!, withName: "destination_address_province")
                multipartFormData.append("\(work.destination.contactName)".data(using: String.Encoding.utf8)!, withName: "destination_contact_name")
                multipartFormData.append("\(work.destination.contactTelephoneNumber)".data(using: String.Encoding.utf8)!, withName: "destination_contact_telephone_number")
                multipartFormData.append("\(work.destination.name)".data(using: String.Encoding.utf8)!, withName: "destination_shop_name")
                multipartFormData.append("\(work.destination.landmark)".data(using: String.Encoding.utf8)!, withName: "destination_landmark")
                multipartFormData.append("\(work.destination.date)".data(using: String.Encoding.utf8)!, withName: "delivered_date")
                multipartFormData.append("\(work.destination.time)".data(using: String.Encoding.utf8)!, withName: "delivered_time")
                for additionalService in work.additionalServices {
                    multipartFormData.append("1".data(using: String.Encoding.utf8)!, withName: "has_service_\(additionalService.id!)")
                }
                if let postId = work.postId {
                    multipartFormData.append("\(postId)".data(using: String.Encoding.utf8)!, withName: "post_id")
                }
                if let promotionCode = work.promotionCode {
                    multipartFormData.append("\(promotionCode)".data(using: String.Encoding.utf8)!, withName: "promotion_code")
                }
            }, with: UserAPIRouter.createWork) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = SuccessResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                            print(JSON(value))
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func getNews(page: Int) -> Observable<NewsListResponse> {
        return Observable<NewsListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getNews(page)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = NewsListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getNewsDetail(newsId: Int) -> Observable<NewsResponse> {
        return Observable<NewsResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getNewsDetail(newsId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = NewsResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getMyWork(page: Int, status: String, search: String, sort: String) -> Observable<WorkListResponse> {
        return Observable<WorkListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getMyWork(page, status, search, sort)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = WorkListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getWorkDetail(workId: Int) -> Observable<WorkResponse> {
        return Observable<WorkResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getWorkDetail(workId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = WorkResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func cancelWork(workId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.cancelWork(workId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getTransaction(page: Int, date: String) -> Observable<TransactionsListResponse> {
        return Observable<TransactionsListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getTransaction(page, date)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = TransactionsListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func depositWithTransferSlip(depositType: Int, price: Int, payTime: String, paySlipData: Data) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("\(depositType)".data(using: String.Encoding.utf8)!, withName: "topup_type")
                multipartFormData.append("\(price)".data(using: String.Encoding.utf8)!, withName: "price")
                multipartFormData.append("\(payTime)".data(using: String.Encoding.utf8)!, withName: "pay_time")
                multipartFormData.append(paySlipData, withName: "pay_slip_image", fileName: "pay_slip.jpeg", mimeType: "image/jpeg")
            }, with: UserAPIRouter.depositWithTransferSlip) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = SuccessResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func depositWithOmise(depositType: Int, price: Int, omiseToken: String, fromApp: Int) -> Observable<DepositOmiseResponse> {
        return Observable<DepositOmiseResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.depositWithOmise(depositType, price, omiseToken, fromApp)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = DepositOmiseResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func completePaymentOmise(depositId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.depositCompletePaymentOmise(depositId: depositId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getPost(page: Int, _ sourceProvince: String, _ destinationProvince: String, _ receivedDate: String, _ truckTypeId: String) -> Observable<PostListResponse> {
        return Observable<PostListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getPost(page, sourceProvince, destinationProvince, receivedDate, truckTypeId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = PostListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getSourceHistory(page: Int) -> Observable<AddressHistoryListResponse> {
        return Observable<AddressHistoryListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getSourceHistory(page)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = AddressHistoryListResponse(source: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getDestinationHistory(page: Int) -> Observable<AddressHistoryListResponse> {
        return Observable<AddressHistoryListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getDestinationHistory(page)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = AddressHistoryListResponse(destination: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func checkPromotion(code: String) -> Observable<PromotionResponse> {
        return Observable<PromotionResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.checkPromotion(code)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = PromotionResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
}

extension UserService {
    
    func logOut() {
        User.shared.clear()
        UserManager.default.destroyAllData()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNavigationView")
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(nextViewController, animated:true, completion:nil)
        } else {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = nextViewController
        }
    }
    
}
