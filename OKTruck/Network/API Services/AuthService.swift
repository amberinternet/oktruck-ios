//
//  AuthService.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire
import SwiftyJSON
import RxSwift

class AuthService {
    
    private let networkManager = NetworkManager.default()
    
    private init() {}
    
    static let `default` = AuthService()
    
    func logIn(email: String, password: String, firebaseToken: String, type: String) -> Observable<AccessTokenResponse> {
        return Observable<AccessTokenResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(AuthAPIRouter.logIn(email, password, firebaseToken, type)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = AccessTokenResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func signUp(
        firstName: String,
        lastName: String,
        telephoneNumber: String,
        email: String,
        password: String,
        confirmPassword: String,
        companyName: String) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(AuthAPIRouter.signUp(firstName, lastName, telephoneNumber, email, password, confirmPassword, companyName)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func requestOTP(email: String, telephoneNumber: String) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(AuthAPIRouter.requestOTP(email, telephoneNumber)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func submitOTP(telephoneNumber: String, otp: String) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(AuthAPIRouter.submitOTP(telephoneNumber, otp)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
}
