//
//  AuthAPIRouter.swift
//  OKTrucker
//
//  Created by amberhello on 6/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire

enum AuthAPIRouter: URLRequestConvertible {
    
    //The endpoint name we'll call later
    case logIn(_ email: String, _ password: String, _ firebaseToken: String, _ type: String)
    case signUp(
        _ firstName: String,
        _ lastName: String,
        _ telephoneNumber: String,
        _ email: String,
        _ password: String,
        _ confirmPassword: String,
        _ companyName: String)
    case requestOTP(_ email: String, _ telephoneNumber: String)
    case submitOTP(_ telephoneNumber: String, _ otp: String)
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .logIn:
            return .post
        case .signUp:
            return .post
        case .requestOTP:
            return .post
        case .submitOTP:
            return .post
        }
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
        case .logIn:
            return "token"
        case .signUp:
            return "signup/user"
        case .requestOTP:
            return "otp"
        case .submitOTP:
            return "otp/submit"
        }
    }
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
            
        case .logIn(let email, let password, let firebaseToken, let type):
            return ["email" : email, "password" : password, "token" : firebaseToken, "type" : type]
            
        case .signUp(let firstName, let lastName, let telephoneNumber, let email, let password, let confirmPassword, let companyName):
            return [
                "first_name" : firstName,
                "last_name" : lastName,
                "username" : telephoneNumber,
                "email" : email,
                "password" : password,
                "password_confirmation" : confirmPassword,
                "company_name" : companyName
            ]
        case .requestOTP(let email, let telephoneNumber):
            return [
                "email" : email,
                "telephone_number" : telephoneNumber
            ]
        case .submitOTP(let telephoneNumber, let otp):
            return [
                "telephone_number" : telephoneNumber,
                "otp" : otp
            ]
        }
    }
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.BASE_API_URL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
}
