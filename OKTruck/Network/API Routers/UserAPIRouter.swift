//
//  UserAPIRouter.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire

enum UserAPIRouter: URLRequestConvertible {
    //The endpoint name we'll call later
    case refreshToken
    case destroyToken(_ firebaseToken: String)
    case getProfile
    case getTruckTypes
    case getDistance(_ sourceLatitude: Double, _ sourceLongitude: Double, _ destinationLatitude: Double, _ destinationLongitude: Double)
    case calculateFee
    case createWork
    case createWorkCompletePaymentOmise(workId: Int)
    case getNews(_ page: Int)
    case getNewsDetail(_ newsId: Int)
    case getMyWork(_ page: Int, _ status: String, _ search: String, _ sort: String)
    case getWorkDetail(_ workId: Int)
    case cancelWork(_ workId: Int)
    case getTransaction(_ page: Int, _ date: String)
    case depositWithTransferSlip
    case depositWithOmise(_ depositType: Int, _ price: Int, _ omiseToken: String, _ fromApp: Int)
    case depositCompletePaymentOmise(depositId: Int)
    case getPost(_ page: Int, _ sourceProvince: String, _ destinationProvince: String, _ receivedDate: String, _ truckTypeId: String)
    case getSourceHistory(_ page: Int)
    case getDestinationHistory(_ page: Int)
    case checkPromotion(_ code: String)
    
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .refreshToken:
            return .post
        case .destroyToken:
            return .post
        case .getProfile:
            return .get
        case .getTruckTypes:
            return .get
        case .getDistance:
            return .get
        case .calculateFee:
            return .post
        case .createWork:
            return .post
        case .createWorkCompletePaymentOmise:
            return .post
        case .getNews:
            return .get
        case .getNewsDetail:
            return .get
        case .getMyWork:
            return .get
        case .getWorkDetail:
            return .get
        case .cancelWork:
            return .post
        case .getTransaction:
            return .get
        case .depositWithTransferSlip:
            return .post
        case .depositWithOmise:
            return .post
        case .depositCompletePaymentOmise:
            return .post
        case .getPost:
            return .get
        case .getSourceHistory:
            return .get
        case .getDestinationHistory:
            return .get
        case .checkPromotion:
            return .post
        }
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
        case .refreshToken:
            return "token/refresh"
        case .destroyToken:
            return "token/destroy"
        case .getProfile:
            return "me"
        case .getTruckTypes:
            return "car/type"
        case .getDistance:
            return "distance"
        case .calculateFee:
            return "work/calprice"
        case .createWork:
            return "work"
        case .createWorkCompletePaymentOmise(let workId):
            return "work/\(workId)/payment/success"
        case .getNews:
            return "news"
        case .getNewsDetail(let newsId):
            return "news/\(newsId)"
        case .getMyWork:
            return "work"
        case .getWorkDetail(let workId):
            return "work/\(workId)"
        case .cancelWork(let workId):
            return "work/\(workId)/cancel"
        case .getTransaction:
            return "me/credit-log"
        case .depositWithTransferSlip:
            return "me/topup"
        case .depositWithOmise:
            return "me/topup"
        case .depositCompletePaymentOmise(let depositId):
            return "topup/\(depositId)/success"
        case .getPost:
            return "post"
        case .getSourceHistory:
            return "historySources"
        case .getDestinationHistory:
            return "historyDestinations"
        case .checkPromotion:
            return "promotion/check"
        }
    }
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
        case .refreshToken:
            return nil
        case .destroyToken(let firebaseToken):
            return ["token" : firebaseToken]
        case .getProfile:
            return nil
        case .getTruckTypes:
            return nil
        case .getDistance(let sourceLatitude, let sourceLongitude, let destinationLatitude, let destinationLongitude):
            return [
                "source_latitude" : sourceLatitude,
                "source_longitude" : sourceLongitude,
                "destination_latitude" : destinationLatitude,
                "destination_longitude" : destinationLongitude
            ]
        case .createWork:
            return nil
        case .calculateFee:
            return nil
        case .createWorkCompletePaymentOmise:
            return nil
        case .getNews(let page):
            return ["page" : page]
        case .getNewsDetail:
            return nil
        case .getMyWork(let page, let status, let search, let sort):
            return [
                "page" : page,
                "status" : status,
                "q" : search,
                "order_by" : sort
            ]
        case .getWorkDetail:
            return nil
        case .cancelWork:
            return nil
        case .getTransaction(let page, let date):
            return [
                "page" : page,
                "month" : date
            ]
        case .depositWithTransferSlip:
            return nil
        case .depositWithOmise(let depositType, let price, let omiseToken, let fromApp):
            return [
                "topup_type" : depositType,
                "price" : price,
                "omise_token" : omiseToken,
                "from_app" : fromApp
            ]
        case .depositCompletePaymentOmise:
            return nil
        case .getPost(let page, let sourceProvince, let destinationProvince, let receivedDate, let truckTypeId):
            return [
                "page" : page,
                "province" : sourceProvince,
                "province_2" : destinationProvince,
                "date" : receivedDate,
                "car_type" : truckTypeId
            ]
        case .getSourceHistory(let page):
            return [
                "page" : page
            ]
        case .getDestinationHistory(let page):
            return [
                "page" : page
            ]
        case .checkPromotion(let code):
            return [
                "promotion_code" : code
            ]
        }
    }
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.BASE_API_URL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        // Authentication Headers
//        urlRequest.setValue(Constants.TOKEN_TYPE + UserManager.default.token, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    
}
