//
//  PromotionResponse.swift
//  OKTruck
//
//  Created by amberhello on 30/3/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct PromotionResponse {
    
    let code : String!
    let discount : Double!
    
    init(json : JSON) {
        code = json["code"].stringValue
        discount = json["discount"].doubleValue
    }
    
}
