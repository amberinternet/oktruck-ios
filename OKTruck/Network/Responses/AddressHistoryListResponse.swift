//
//  AddressHistoryListResponse.swift
//  OKTruck
//
//  Created by Thanathip Suppapantita on 20/3/20.
//  Copyright © 2020 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct AddressHistoryListResponse {
    
    let addressHistoryPagination : AddressHistoryPagination!
    
    init(source json : JSON) {
        addressHistoryPagination = AddressHistoryPagination(source: json["data"])
    }
    
    init(destination json : JSON) {
        addressHistoryPagination = AddressHistoryPagination(destination: json["data"])
    }
    
}
