//
//  CalculateFeeResponse.swift
//  OKTruck
//
//  Created by amberhello on 21/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct CalculateFeeResponse {
    
    let fullFee : Double!
    let backhaulDiscount : Double!
    let summaryFee : Double!

    init(json : JSON) {
        fullFee = json["full_price"].doubleValue
        backhaulDiscount = json["backhaul_discount"].doubleValue
        summaryFee = json["summary_price"].doubleValue
    }
    
}
