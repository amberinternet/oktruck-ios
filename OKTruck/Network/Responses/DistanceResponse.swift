//
//  DistanceResponse.swift
//  OKTruck
//
//  Created by amberhello on 3/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct DistanceResponse {
    
    let distance : Double!
    
    init(json : JSON) {
        distance = json["data"]["distance"].doubleValue
    }
    
}
