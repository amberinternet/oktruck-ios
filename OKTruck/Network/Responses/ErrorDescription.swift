//
//  ErrorDescription.swift
//  OKTrucker
//
//  Created by amberhello on 24/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

class ErrorDescription {
    
    var firstNameList: [String] = []
    var lastNameList: [String] = []
    var usernameList: [String] = []
    var emailList: [String] = []
    var telephoneNumberList: [String] = []
    var passwordList: [String] = []
    var payTimeList: [String] = []
    var priceList: [String] = []
    var slipImageList: [String] = []
    var signatureList: [String] = []
    
    private var error: String = Constants.INITIAL_STRING
    
    init(json : JSON) {
        firstNameList = json["first_name"].arrayValue.map { $0.stringValue }
        lastNameList = json["last_name"].arrayValue.map { $0.stringValue }
        usernameList = json["username"].arrayValue.map { $0.stringValue }
        emailList = json["email"].arrayValue.map { $0.stringValue }
        telephoneNumberList = json["telephone_number"].arrayValue.map { $0.stringValue }
        passwordList = json["password"].arrayValue.map { $0.stringValue }
        payTimeList = json["pay_time"].arrayValue.map { $0.stringValue }
        priceList = json["price"].arrayValue.map { $0.stringValue }
        slipImageList = json["pay_slip_image"].arrayValue.map { $0.stringValue }
        signatureList = json["autograph"].arrayValue.map { $0.stringValue }
    }
    
    func getError() -> String {
        error = Constants.INITIAL_STRING
        error += firstNameList.getError()
        error += lastNameList.getError()
        error += usernameList.getError()
        error += emailList.getError()
        error += telephoneNumberList.getError()
        error += passwordList.getError()
        error += payTimeList.getError()
        error += priceList.getError()
        error += slipImageList.getError()
        error += signatureList.getError()
        if !error.isEmpty { return error.trimmingCharacters(in: .whitespacesAndNewlines) } else { return error }
    }
    
}
