//
//  WorkResponse.swift
//  OKTruck
//
//  Created by amberhello on 14/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct WorkResponse {
    
    let work : Work!
    
    init(json : JSON) {
        work = Work(json: json["data"])
    }
    
}
