//
//  ErrorResponse.swift
//  OKTrucker
//
//  Created by amberhello on 10/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct ErrorResponse: LocalizedError {
    
    var message: String
    var description: ErrorDescription
    
    init(json : JSON) {
        message = json["error"].stringValue
        description = ErrorDescription(json: json["description"])
        
        if let data = json["data"].string {
            message = data
        }
    }
    
    public var errorDescription: String?
    {
        if !(description.getError().isEmpty) {
            return description.getError()
        } else {
            switch message {
            case "validation_failed", "could_not_create_token": return NSLocalizedString("Error:FoundError", comment: "")
            case "invalid_credentials": return NSLocalizedString("Error:EmailPasswordIncorrect", comment: "")
            case "token_invalid", "token_not_provided", "token_expired", "ERROR": return NSLocalizedString("Error:PleaseLoginAgain", comment: "")
            case "user_telephone_number_is_not_verified", "user_is_not_verified": return NSLocalizedString("Error:AccountNotVerify", comment: "")
            case "user_status_is_deactive": return NSLocalizedString("Error:AccountSuspended", comment: "")
            case "unable_to_send_sms": return NSLocalizedString("Error:CannotSendOTP", comment: "")
            case "invalid_otp": return NSLocalizedString("Error:InvalidOTP", comment: "")
            case "otp_is_already_expired": return NSLocalizedString("Error:OTPExpired", comment: "")
            case "topup_not_found": return NSLocalizedString("Error:PaymentFailed", comment: "")
            case "credit_is_not_enough": return NSLocalizedString("Error:CreditNotEnough", comment: "")
            case "news_not_found": return NSLocalizedString("Error:NewsNotFound", comment: "")
            case "work_cannot_cancel": return NSLocalizedString("Error:WorkCannotCancel", comment: "")
            case "distance_must_greater_than_0": return NSLocalizedString("Error:DuplicateLocation", comment: "")
            default: return message.replacingOccurrences(of: "_", with: " ")
            }
        }
    }
    
}

