//
//  WorkOmiseResponse.swift
//  OKTruck
//
//  Created by amberhello on 7/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct WorkOmiseResponse {
    
    let workOmise : WorkOmise!
    
    init(json : JSON) {
        workOmise = WorkOmise(json: json["data"])
    }
    
}
