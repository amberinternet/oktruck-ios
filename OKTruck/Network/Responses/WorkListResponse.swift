//
//  WorkListResponse.swift
//  OKTruck
//
//  Created by amberhello on 10/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct WorkListResponse {
    
    let workPagination : WorkPagination!
    
    init(json : JSON) {
        workPagination = WorkPagination(json: json["data"])
    }
    
}
