//
//  TransactionsListResponse.swift
//  OKTruck
//
//  Created by amberhello on 1/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct TransactionsListResponse {
    
    let transactionPagination : TransactionPagination!
    
    init(json : JSON) {
        transactionPagination = TransactionPagination(json: json["data"])
    }
    
}
