//
//  TruckTypesResponse.swift
//  OKTruck
//
//  Created by amberhello on 23/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct TruckTypesResponse {
    
    var truckTypes : [TruckType]! = []
    
    init(json : JSON) {
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let truckType = TruckType(json: jsonObject)
                truckTypes.append(truckType)
            }
        }
    }
    
}
