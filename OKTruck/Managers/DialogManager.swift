//
//  DialogManager.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class DialogManager {
    
    static private var viewControllor: UIViewController! {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    
    private init() {}
    
    static private let instance = DialogManager()
    
    static func `default`() -> DialogManager {
        return instance
    }
    
    func showContactAdmin() {
        let dialogController = ContactAdminDialogController(nibName: "ContactAdminDialog", bundle: nil)
        present(dialogController)
    }
    
    func showConfirm(title: String, message: String, completion: @escaping () -> Void) {
//        let dialogController = ConfirmDialogController(nibName: "ConfirmDialog", bundle: nil)
//        dialogController.title = title
//        dialogController.message = message
//        dialogController.completion = completion
//        present(dialogController)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:Dismiss", comment: ""),
                                                style: UIAlertAction.Style.cancel))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:OK", comment: ""),
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
                                                    completion()
        }))
        present(alertController)
    }
    
    func showConfirmLogOut(completion: @escaping () -> Void) {
    //        let dialogController = ConfirmDialogController(nibName: "ConfirmDialog", bundle: nil)
    //        dialogController.title = title
    //        dialogController.message = message
    //        dialogController.completion = completion
    //        present(dialogController)
            
            let alertController = UIAlertController(title: NSLocalizedString("Alert:LogOut:title", comment: ""),
                                                    message: NSLocalizedString("Alert:LogOut:message", comment: ""),
                                                    preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:Dismiss", comment: ""),
                                                    style: UIAlertAction.Style.cancel))
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:OK", comment: ""),
                                                    style: UIAlertAction.Style.destructive,
                                                    handler: {(_: UIAlertAction!) in
                                                        completion()
            }))
            present(alertController)
        }
    
    func showAlert(message: String) {
//        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
//        dialogController.message = message
//        present(dialogController)
        
        let title = NSLocalizedString("Application:Alert", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:OK", comment: ""),
                                                style: UIAlertAction.Style.default))
        present(alertController)
    }
    
    func showAlert(title: String, message: String) {
//        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
//        dialogController.title = title
//        dialogController.message = message
//        present(dialogController)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:OK", comment: ""),
                                                style: UIAlertAction.Style.default))
        present(alertController)
    }
    
    func showAlert(message: String, completion: @escaping () -> Void) {
//        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
//        dialogController.message = message
//        dialogController.completion = completion
//        present(dialogController)
        
        let title = NSLocalizedString("Application:Alert", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:OK", comment: ""),
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
                                                    completion()
        }))
        present(alertController)
    }
    
    func showAlert(title: String, message: String, completion: @escaping () -> Void) {
//        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
//        dialogController.title = title
//        dialogController.message = message
//        dialogController.completion = completion
//        present(dialogController)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:OK", comment: ""),
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
                                                    completion()
        }))
        present(alertController)
    }
    
    func showAlert(title: String, message: String, action: String, completion: @escaping () -> Void) {
//        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
//        dialogController.title = title
//        dialogController.message = message
//        dialogController.action = action
//        dialogController.completion = completion
//        present(dialogController)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: action,
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
                                                    completion()
        }))
        present(alertController)
    }
    
    func showError(error: String) {
//        let dialogController = ErrorDialogController(nibName: "ErrorDialog", bundle: nil)
//        dialogController.message = error
//        present(dialogController)
        
        let title = NSLocalizedString("Error:FoundError", comment: "")
        let alertController = UIAlertController(title: title, message: error, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:Close", comment: ""),
                                                style: UIAlertAction.Style.destructive))
        present(alertController)
    }
    
    func showSuccess(message: String, completion: @escaping () -> Void) {
//        let dialogController = SuccessDialogController(nibName: "SuccessDialog", bundle: nil)
//        dialogController.message = message
//        dialogController.completion = completion
//        present(dialogController)
        
        let title = NSLocalizedString("Application:Success", comment: "")
        let alertController = ImageAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.setTitleImage(UIImage(named: "Checked Icon"))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:Close", comment: ""),
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
                                                    completion()
        }))
        present(alertController)
    }
    
    func showSuccess(title: String, message: String, completion: @escaping () -> Void) {
//        let dialogController = SuccessDialogController(nibName: "SuccessDialog", bundle: nil)
//        dialogController.title = title
//        dialogController.message = message
//        dialogController.completion = completion
//        present(dialogController)
        
        let alertController = ImageAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.setTitleImage(UIImage(named: "Checked Icon"))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Application:Close", comment: ""),
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
                                                    completion()
        }))
        present(alertController)
    }
    
    func showRequestAccessDialog(title: String, message: String, settingsUrl: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Application:Settings", comment: ""), style: .default) { (_) -> Void in
            if let settingsUrl = URL(string: settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Application:Dismiss", comment: ""), style: .cancel)
        
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        present(alertController)
    }
    
    func showToast(message : String, duration: Double = 2.0) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        DialogManager.viewControllor.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    
    private var activityIndicatorDialogController: ActivityIndicatorDialogController? = nil
    
    func showLoading() {
        if activityIndicatorDialogController == nil {
            activityIndicatorDialogController = ActivityIndicatorDialogController(nibName: "ActivityIndicatorDialog", bundle: nil)
            present(activityIndicatorDialogController)
        }
    }
    
    func showLoading(message: String) {
        if activityIndicatorDialogController == nil {
            activityIndicatorDialogController = ActivityIndicatorDialogController(nibName: "ActivityIndicatorDialog", bundle: nil)
            activityIndicatorDialogController!.message = message
            present(activityIndicatorDialogController)
        } else {
            activityIndicatorDialogController!.loadingLabel.text = message
        }
    }
    
    func hideLoading() {
        if activityIndicatorDialogController != nil {
            activityIndicatorDialogController!.dismiss(animated: false, completion: {
                self.activityIndicatorDialogController = nil
            })
        }
    }
    
    func present(_ dialogController: UIViewController!) {
        dialogController.providesPresentationContextTransitionStyle = true
        dialogController.definesPresentationContext = true
        dialogController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        dialogController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        DialogManager.viewControllor.present(dialogController, animated: false)
    }
    
    func present(_ alertController: UIAlertController!) {
        DialogManager.viewControllor.present(alertController, animated: true)
    }
    
}
