//
//  PostCollectionViewCell.swift
//  OKTrucker
//
//  Created by amberhello on 7/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class PostCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var truckTypeLabel: UILabel!
    @IBOutlet weak var licensePlateLabel: UILabel!
    @IBOutlet weak var sourceDateLabel: UILabel!
    @IBOutlet weak var sourceProvinceLabel: UILabel!
//    @IBOutlet weak var destinationDateLabel: UILabel!
    @IBOutlet weak var destinationProvinceLabel: UILabel!
    @IBOutlet weak var passedProvincesLabel: UILabel!
    
    func initView(post: Post) {
//        truckImageView.af_setImage(withURL: URL(string: post.getTruckType().getImagePath())!)
        truckImageView.image = post.getTruckType().image
        truckTypeLabel.text = post.getTruckType().name
        licensePlateLabel.text = post.driver.truck.licensePlate
        if Calendar.current.isDateInToday(post.receivedDate.fromDate()) {
            sourceDateLabel.text = "(\(NSLocalizedString("Application:Today", comment: ""))) \(post.receivedDate.fromDate().toDateWithFullDayAppFormat())"
        } else {
            sourceDateLabel.text = post.receivedDate.fromDate().toDateWithFullDayAppFormat()
        }
        sourceProvinceLabel.text = post.sourceProvince
//        destinationDateLabel.text = post.getDeliveredDateTime()
        destinationProvinceLabel.text = post.destinationProvince
        passedProvincesLabel.text = post.passedProvinces.joined(separator:", ")
    }
    
}
