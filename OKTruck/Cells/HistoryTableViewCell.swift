//
//  HistoryTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 27/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var contactTelLabel: UILabel!
    
}
