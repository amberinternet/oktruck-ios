//
//  DateTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 27/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class DateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    func setDate(_ date: String, time: String) {
        if time == "เวลาใดก็ได้" {
            placeholderLabel.text = "\(date.fromDate().toDateWithFullDayAppFormat()) | \(NSLocalizedString("Application:Anytime", comment: ""))"
        } else {
            placeholderLabel.text = "\(date.fromDate().toDateWithFullDayAppFormat()) | \(time)"
        }
        placeholderLabel.textColor = UIColor(named: "Text Color")
    }
    
}
