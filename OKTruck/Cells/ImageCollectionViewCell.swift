//
//  ImageCollectionViewCell.swift
//  OKTruck
//
//  Created by amberhello on 27/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
