//
//  TruckTypeCollectionViewCell.swift
//  OKTruck
//
//  Created by amberhello on 14/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class TruckTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var typeNameLabel: UILabel!
    @IBOutlet weak var maximumLoadLabel: UILabel!
    @IBOutlet weak var truckImageViewCenterYConstraint: NSLayoutConstraint!
    private let animateDuration: TimeInterval = 0.1
    var truckType: TruckType?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }
    
    func initTruckType(_ truckType: TruckType) {
        self.truckType = truckType
//        cell.truckImageView.af_setImage(withURL: URL(string: truckType.getImagePath())!)
        truckImageView.image = truckType.image.mono
        typeNameLabel.text = truckType.name
        maximumLoadLabel.text = truckType.maximumLoad
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                truckImageView.image = truckType?.image
                UIView.animate(withDuration: animateDuration, animations: {
                    self.reset()
                })
            } else {
                truckImageView.image = truckType?.image.mono
                UIView.animate(withDuration: animateDuration, animations: {
                    self.setUp()
                }, completion: { _ in
                    self.contentView.frame.origin.y = -self.truckImageViewCenterYConstraint.constant
                })
            }
        }
    }
    
    private func setUp() {
        self.typeNameLabel.alpha = 0
        self.maximumLoadLabel.alpha = 0
        self.contentView.frame.origin.y = -self.truckImageViewCenterYConstraint.constant
    }
    
    private func reset() {
        self.typeNameLabel.alpha = 1
        self.maximumLoadLabel.alpha = 1
        self.contentView.frame.origin.y = 0
    }
    
}
