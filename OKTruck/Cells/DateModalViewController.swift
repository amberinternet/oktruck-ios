//
//  DateModalViewController.swift
//  OKTruck
//
//  Created by amberhello on 1/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class DateModalViewController: BaseModalViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickUpDatePickerView: UIPickerView!
    @IBOutlet weak var dropOffDatePickerView: UIPickerView!
    private var pickUpDateTimeList: [[String]]!
    private var dropOffDateTimeList: [[String]]!
    private var pickUpDateList: [String]!
    private var dropOffDateList: [String]!
    private var timeList: [String]!
    private var durationTimeList: [String]!
    var startDate: Date!
    var isBackhaulWork: Bool!
    
    var selectedPickUpDate: String?
    var selectedPickUpTime: String?
    var selectedDropOffDate: String?
    var selectedDropOffTime: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        // Pick Up Date
        pickUpDateList = [String]()
        pickUpDateList.append(startDate.toDateServerFormat())
        timeList = [NSLocalizedString("Application:Anytime", comment: "")]
        timeList.append(contentsOf: Arrays.hour_time_list)
        var showDateList = [String]()
        
        if !isBackhaulWork {
            if Date().getHour() <= 19 {
                showDateList.append("\(NSLocalizedString("Application:Today", comment: "")) (\(startDate.toDateWithFullDayAppFormat()))")
            } else {
                startDate = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
                showDateList.append("\(NSLocalizedString("Application:Tomorrow", comment: "")) (\(startDate.toDateWithFullDayAppFormat()))")
            }
            for _ in 1...7 {
                let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
                startDate = tomorrow
                pickUpDateList.append(startDate.toDateServerFormat())
                showDateList.append(startDate.toDateWithFullDayAppFormat())
            }
        } else {
            showDateList.append(startDate.toDateWithFullDayAppFormat())
        }
        pickUpDateTimeList = [showDateList, timeList]
        
        // Drop Off Date
        dropOffDateList = [String]()
        durationTimeList = [NSLocalizedString("Application:Anytime", comment: "")]
        durationTimeList.append(contentsOf: Arrays.duration_time_list)
        initDeliveredDateVariable(startDate: startDate)
    }
    
    private func initDeliveredDateVariable(startDate: Date) {
        var startDate = startDate
        dropOffDateList.removeAll()
        dropOffDateList.append(startDate.toDateServerFormat())
        var showDateList = [String]()
        
        if startDate.getDay() == Date().getDay() {
            showDateList.append("\(NSLocalizedString("Application:Today", comment: "")) (\(startDate.toDateWithFullDayAppFormat()))")
        } else {
            showDateList.append(startDate.toDateWithFullDayAppFormat())
        }
        for _ in 1...2 {
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
            startDate = tomorrow
            dropOffDateList.append(startDate.toDateServerFormat())
            showDateList.append(startDate.toDateWithFullDayAppFormat())
        }
        
        dropOffDateTimeList = [showDateList, durationTimeList]
    }
    
    private func initView() {
        if let selectedPickUpDate = selectedPickUpDate,
            let selectedPickUpTime = selectedPickUpTime,
            let selectedDropOffDate = selectedDropOffDate,
            let selectedDropOffTime = selectedDropOffTime
        {
            pickUpDatePickerView.selectRow(pickUpDateList.firstIndex(of: selectedPickUpDate) ?? 0, inComponent: 0, animated: false)
            pickerView(pickUpDatePickerView, didSelectRow: pickUpDatePickerView.selectedRow(inComponent: 0), inComponent: 0)
            pickUpDatePickerView.selectRow(timeList.firstIndex(of: selectedPickUpTime) ?? 0, inComponent: 1, animated: false)
            pickerView(pickUpDatePickerView, didSelectRow: pickUpDatePickerView.selectedRow(inComponent: 1), inComponent: 1)
            
            dropOffDatePickerView.selectRow(dropOffDateList.firstIndex(of: selectedDropOffDate) ?? 0, inComponent: 0, animated: false)
            pickerView(dropOffDatePickerView, didSelectRow: dropOffDatePickerView.selectedRow(inComponent: 0), inComponent: 0)
            dropOffDatePickerView.selectRow(durationTimeList.firstIndex(of: selectedDropOffTime) ?? 0, inComponent: 1, animated: false)
            pickerView(dropOffDatePickerView, didSelectRow: dropOffDatePickerView.selectedRow(inComponent: 1), inComponent: 1)
        } else {
            pickerView(pickUpDatePickerView, didSelectRow: 0, inComponent: 0)
            pickerView(pickUpDatePickerView, didSelectRow: 0, inComponent: 1)
            pickerView(dropOffDatePickerView, didSelectRow: 0, inComponent: 0)
            pickerView(dropOffDatePickerView, didSelectRow: 0, inComponent: 1)
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func selectDate(_ sender: Any) {
        selectedPickUpDate = pickUpDateList[pickUpDatePickerView.selectedRow(inComponent: 0)]
        if pickUpDatePickerView.selectedRow(inComponent: 1) == 0 {
            selectedPickUpTime = "เวลาใดก็ได้"
        } else {
            selectedPickUpTime = timeList[pickUpDatePickerView.selectedRow(inComponent: 1)]
        }
        
        selectedDropOffDate = dropOffDateList[dropOffDatePickerView.selectedRow(inComponent: 0)]
        if dropOffDatePickerView.selectedRow(inComponent: 1) == 0 {
            selectedDropOffTime = "เวลาใดก็ได้"
        } else {
            selectedDropOffTime = durationTimeList[dropOffDatePickerView.selectedRow(inComponent: 1)]
        }
        
        performSegue(withIdentifier: "DateUnwindSegue", sender: sender)
    }
    
}

extension DateModalViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickUpDatePickerView {
            return pickUpDateTimeList[component].count
        } else {
            return dropOffDateTimeList[component].count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickUpDatePickerView {
            return pickUpDateTimeList[component][row]
        } else {
            return dropOffDateTimeList[component][row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel = view as? UILabel ?? UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = UIColor(named: "Text Color")
        label.textAlignment = .center
        
        if pickerView == pickUpDatePickerView {
            label.text = pickUpDateTimeList[component][row]
        } else {
            label.text = dropOffDateTimeList[component][row]
        }
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            if pickerView == pickUpDatePickerView {
                if row == 0 {
                    let startHour: Int
                    if Date().getHour() <= 19 {
                        startHour = Date().getHour() + 4
                    } else {
                        startHour = Date().getHour() - 20
                    }
                    pickUpDateTimeList[1] = Array(timeList[(startHour+1)...])
                    pickUpDateTimeList[1].insert(timeList[0], at: 0)
                    pickerView.reloadComponent(1)
                } else {
                    pickUpDateTimeList[1] = timeList
                    pickerView.reloadComponent(1)
                }
                
                let selectedPickUpTime = timeList[pickUpDatePickerView.selectedRow(inComponent: 1)]
                if pickerView.selectedRow(inComponent: 1) > 0 && selectedPickUpTime != pickUpDateTimeList[1][pickerView.selectedRow(inComponent: 1)] {
                    pickerView.selectRow(0, inComponent: 1, animated: true)
                    self.pickerView(pickerView, didSelectRow: 0, inComponent: 1)
                }
                
                // reset dropOffDatePickerView when select in pickUpDatePickerView
                let selectedPickUpDate = pickUpDateList[row]
                initDeliveredDateVariable(startDate: selectedPickUpDate.fromDate())
                dropOffDatePickerView.reloadComponent(0)
                self.pickerView(dropOffDatePickerView, didSelectRow: 0, inComponent: 0)
            }
        }
    }
    
}
