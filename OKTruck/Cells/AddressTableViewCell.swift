//
//  AddressTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 27/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    
    func setAddress(_ address: Address) {
        addressLabel.text = address.name
        contactLabel.text = NSLocalizedString("Application:Contact", comment: "") + ": " + address.contactName + " " + address.contactTelephoneNumber
        
        addressStackView.isHidden = false
        placeholderLabel.isHidden = true
    }
    
}
