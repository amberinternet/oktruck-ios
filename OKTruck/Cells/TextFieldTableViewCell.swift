//
//  TextFieldTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 27/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
}
