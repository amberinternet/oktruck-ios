//
//  TruckTypeTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 22/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class TruckTypeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var typeNameLabel: UILabel!
    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var maximumLoadLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp() {
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor(named: "Background Tint Color")
        selectedView.layer.borderColor = UIColor(named: "Tint Color")!.cgColor
        selectedView.layer.borderWidth = 1
        
        selectedBackgroundView = selectedView
    }
    
}
