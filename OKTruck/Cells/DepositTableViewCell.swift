//
//  DepositTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 13/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class DepositTableViewCell: UITableViewCell {
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var bonusCreditLabel: UILabel!
    @IBOutlet weak var totalCreditLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp() {
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor(named: "Background Tint Color")
        selectedView.layer.borderColor = UIColor(named: "Tint Color")!.cgColor
        selectedView.layer.borderWidth = 1
        
        selectedBackgroundView = selectedView
    }
    
}
