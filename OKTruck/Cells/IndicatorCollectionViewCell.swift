//
//  IndicatorCollectionViewCell.swift
//  OKTruck
//
//  Created by amberhello on 30/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class IndicatorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
}
