//
//  FeeTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 3/8/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class FeeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var chargeLabel: UILabel!

}
