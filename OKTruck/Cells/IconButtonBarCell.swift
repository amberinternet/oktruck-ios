//
//  IconButtonBarCell.swift
//  OKTruck
//
//  Created by amberhello on 6/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class IconButtonBarCell: UICollectionViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
