//
//  TransactionViewCell.swift
//  OKTrucker
//
//  Created by amberhello on 29/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class TransactionViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    
    func initView(transaction: Transaction) {
        DispatchQueue.main.async {
            self.nameLabel.attributedText = transaction.getReadableType()
            self.dateLabel.text = transaction.createdAt.fromDateTime().toDateTimeAppFormat()
            self.creditLabel.text = transaction.getReadableCredit()
            self.creditLabel.textColor = UIColor(named: "Text Color")!
        }
    }
    
}
