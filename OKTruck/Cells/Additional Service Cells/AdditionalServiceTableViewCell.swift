//
//  AdditionalServiceTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 30/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class AdditionalServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var checkbox: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

}
