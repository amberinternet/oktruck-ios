//
//  AdditionalServiceWithPriceLabelTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 31/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

import UIKit

class AdditionalServiceWithChargeLabelTableViewCell: AdditionalServiceTableViewCell {
    
    @IBOutlet weak var chargeLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected { checkbox.image = UIImage(named: "Checkbox Image (Checked)") }
        else { checkbox.image = UIImage(named: "Checkbox Image (Unchecked)") }
        selectedBackgroundView?.backgroundColor = UIColor.clear
    }
    
}
