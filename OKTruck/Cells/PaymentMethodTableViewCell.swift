//
//  PaymentMethodTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 23/1/2563 BE.
//  Copyright © 2563 Amber Internet. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var checkbox: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected { checkbox.isHidden = false }
        else { checkbox.isHidden = true }
    }
    
}
