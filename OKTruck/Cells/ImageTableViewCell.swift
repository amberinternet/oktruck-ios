//
//  ImageTableViewCell.swift
//  OKTruck
//
//  Created by amberhello on 27/2/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    
    func initView() {
        placeholderLabel.isHidden = false
        itemImageView.image = nil
        itemImageView.isHidden = true
    }
    
    func setImage(_ image: UIImage) {
        placeholderLabel.isHidden = true
        itemImageView.image = image
        itemImageView.isHidden = false
    }
    
}
