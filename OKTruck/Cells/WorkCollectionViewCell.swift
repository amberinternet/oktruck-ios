//
//  WorkCollectionViewCell.swift
//  OKTruck
//
//  Created by amberhello on 24/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class WorkCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var sourceNameLabel: UILabel!
    @IBOutlet weak var destinationNameLabel: UILabel!
    @IBOutlet weak var sourceTimeLabel: UILabel!
    @IBOutlet weak var sourceDateLabel: UILabel!
    @IBOutlet weak var destinationTimeLabel: UILabel!
    @IBOutlet weak var destinationDateLabel: UILabel!
    @IBOutlet weak var statusLabel: WorkStatusLabel!
    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var workIdLabel: UILabel!
    
    func initView(work: Work) {
        sourceNameLabel.text = work.source.name
        destinationNameLabel.text = work.destination.name
        sourceTimeLabel.text = work.source.getReadableTime()
        sourceDateLabel.text = work.source.getArrivalShotDate()
        destinationTimeLabel.text = work.destination.getReadableTime()
        destinationDateLabel.text = work.destination.getArrivalShotDate()
//        truckImageView.af_setImage(withURL: URL(string: post.getTruckType().getImagePath())!)
        truckImageView.image = work.truckType.image
        workIdLabel.text = work.getReadableWorkId()
        statusLabel.initView(work: work)
    }
    
}
